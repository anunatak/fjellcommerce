<?php
/*
Plugin Name: FjellCommerce
Description: Integrasjon mot WooCommerce for Norsk Fjellfestival
Plugin URI: http://norskfjellfestival.no
Author: Tor Morten Jensen v/ Anunatak
Author URI: http://anunatak.no
Version: 0.0.1
License: GPL2
Text Domain: fjellcommerce
*/

final class FjellCommerce {

	/**
	 * WooCommerce version.
	 *
	 * @var string
	 */
	public $version = '1.0.0';

	/**
	 * The single instance of the class.
	 *
	 * @var FjellCommerce
	 * @since 1.0
	 */
	protected static $_instance = null;

	/**
	 * Instance of the WooCommerce API
	 *
	 * @var FC_WooCommerce_API
	 * @since 1.0
	 */
	public $WC = null;

	/**
	 * Main FjellCommerce Instance.
	 *
	 * Ensures only one instance of FjellCommerce is loaded or can be loaded.
	 *
	 * @since 1.0
	 * @static
	 * @see FjellCommerce()
	 * @return FjellCommerce - Main instance.
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'fjellcommerce' ), '2.1' );
	}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'fjellcommerce' ), '2.1' );
	}

	/**
	 * Auto-load in-accessible properties on demand.
	 *
	 * @param mixed   $key
	 * @return mixed
	 */
	public function __get( $key ) {
	}

	/**
	 * WooCommerce Constructor.
	 */
	public function __construct() {
		if ( $this->dependencies_met() ) {
			$this->define_constants();
			$this->includes();
			$this->init_hooks();

			do_action( 'fjellcommerce_loaded' );
		} else {
			add_action( 'admin_notices', function() { ?>
				<div class="notice notice-error">
					<p><?php _e( '<strong>FjellCommerce</strong> krever at du har <strong>WooCommerce</strong> og <strong>Advanced Custom Fields</strong> installert.', 'fjellcommerce' ); ?></p>
				</div>
			<?php } );
		}
	}

	/**
	 * Check if dependencies are loaded
	 *
	 * @return boolean
	 */
	private function dependencies_met() {
		return function_exists( 'WC' ) && function_exists( 'get_field' );
	}

	/**
	 * Hook into actions and filters.
	 *
	 * @since  1.0
	 */
	private function init_hooks() {
	}

	/**
	 * Define WC Constants.
	 */
	private function define_constants() {
		$this->define( 'FC_PLUGIN_FILE', __FILE__ );
		$this->define( 'FC_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
		$this->define( 'FC_VERSION', $this->version );
		$this->define( 'FJELLCOMMERCE_VERSION', $this->version );
	}

	/**
	 * Define constant if not already set.
	 *
	 * @param string  $name
	 * @param string|bool $value
	 */
	private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}

	/**
	 * What type of request is this?
	 *
	 * @param string  $type admin, ajax, cron or frontend.
	 * @return bool
	 */
	private function is_request( $type ) {
		switch ( $type ) {
		case 'admin' :
			return is_admin();
		case 'ajax' :
			return defined( 'DOING_AJAX' );
		case 'cron' :
			return defined( 'DOING_CRON' );
		case 'frontend' :
			return ( ! is_admin() || defined( 'DOING_AJAX' ) ) && ! defined( 'DOING_CRON' );
		}
	}

	/**
	 * Include required core files used in admin and on the frontend.
	 */
	public function includes() {

		include_once 'includes/woocommerce/class-fc-woocommerce-product.php'; // WooCommerce Product Model
		include_once 'includes/woocommerce/class-fc-woocommerce-api.php'; // Holds an API wrapper for WooCommerce
		include_once 'includes/woocommerce/class-fc-woocommerce-mods.php'; // WooCommerce Modifications
		include_once 'includes/woocommerce/pages/class-fc-woocommerce-pages-ticket.php'; // WooCommerce Account
		include_once 'includes/woocommerce/pages/class-fc-woocommerce-pages-favorites.php'; // WooCommerce Account
		include_once 'includes/class-fc-post-types.php'; // Registers post types
		include_once 'includes/class-fc-events-filter.php'; // Event filters
		include_once 'includes/class-fc-events.php'; // Events
		include_once 'includes/class-fc-ajax.php'; // Ajax handlers
		include_once 'includes/class-fc-duplicate.php'; // Registers post types

		// Set the WooCommerce API instance
		$this->WC = new FC_WooCommerce_API();

		include_once 'includes/class-fc-emails.php'; // ticket email

		if ( $this->is_request( 'admin' ) ) {
			include_once 'includes/admin/class-fc-admin-sync.php'; // Syncs events to products
			include_once 'includes/admin/class-fc-admin-metabox.php'; // Add Metaboxes
			include_once 'includes/admin/class-fc-admin-switching.php'; // Add Metaboxes
			include_once 'includes/admin/class-fc-admin-dashboard.php'; // Add Metaboxes
			include_once 'includes/admin/class-fc-admin-menu.php'; // Add Metaboxes
		}

		include_once 'includes/stripe/class-fc-stripe-api.php'; // Stripe API
		include_once 'includes/stripe/class-fc-stripe-report.php'; // Stripe Report
		include_once 'includes/stripe/class-fc-stripe-report-legacy.php'; // Stripe Report Legacy (NFF Program)

		include_once 'includes/class-fc-tickets.php'; // Ticket
		include_once 'includes/class-fc-participants.php'; // Participants
		include_once 'includes/class-fc-revenue.php'; // Revenue

		include_once 'includes/class-fc-cron.php'; // Cron

	}

	/**
	 * Gets the current festival
	 * @return WP_Term
	 */
	public function get_active_festival() {
		$festival = get_field( 'active_festival', 'option' );

		$term = get_term_by( 'term_id', $festival, 'nff_program_festival' );

		if ( $term )
			return $term;

		$term = get_term_by( 'slug', 'norsk-fjellfestival', 'nff_program_festival' );

		return $term;
	}

	/**
	 * Gets the current active admin festival
	 * @return WP_Term
	 */
	public function get_current_festival() {
		if(isset($_COOKIE['current_festival'])) {
			$festival = get_term_by( 'slug', $_COOKIE['current_festival'], 'nff_program_festival' );
			return $festival;
		}
		return FjellCommerce()->get_active_festival();
	}

	/**
	 * Gets the active year
	 * @return string
	 */
	public function get_active_year() {
		$year = get_field( 'active_year', 'option' );
		if ( $year )
			return $year;

		return date( 'Y' );
	}

	/**
	 * Checks if a products sales period has started
	 * @param  mixed  $product
	 * @return boolean
	 */
	public function has_sale_not_started($product) {
		$product = wc_get_product($product);

		$sales_start = get_post_meta( $product->id, 'sales_start', true );
		if($sales_start) {
			$sales_start = date_create_from_format('d.m.Y', $sales_start);
			if($sales_start->format('Ymd') > date('Ymd')) {
				return $sales_start->format('d.m.Y');
			}
		}
		return false;
	}

	/**
	 * Checks if a product has reached the end of its sales period
	 * @param  mixed  $product
	 * @return boolean
	 */
	public function has_sale_stopped($product) {
		$product = wc_get_product($product);

		$sales_stop = get_post_meta( $product->id, 'sales_stop', true );

		if($sales_stop) {
			$sales_stop = date_create_from_format('d.m.Y', $sales_stop);
			if(date('Ymd') > $sales_stop->format('Ymd')) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the sales status of a product
	 * @param  mixed $product
	 * @return mixed
	 */
	public function get_sale_status($product) {
		$product = wc_get_product($product);
		$started = $this->has_sale_not_started($product);
		if($this->has_sale_stopped($product)) {
			return 'Salget er ferdig';
		} elseif($started) {
			return 'Salget starter '. $started;
		} elseif(!$product->is_purchasable()) {
			return 'Utsolgt';
		}
		return false;
	}



}

/**
 * Main instance of FjellCommerce.
 *
 * Returns the main instance of WC to prevent the need to use globals.
 *
 * @since  2.1
 * @return FjellCommerce
 */
function FjellCommerce() {
	return FjellCommerce::instance();
}

add_action( 'plugins_loaded', function() {
	// Global for backwards compatibility.
	$GLOBALS['fjellcommerce'] = FjellCommerce();
} );
