<?php
/**
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php printf( __( "Hei. Dine billetter til %s er klare og ligger vedlagt denne eposten.", 'woocommerce' ), FjellCommerce()->get_active_festival()->name ); ?></p>

<p>Billetten fremvises på arrangementet – printet eller digitalt. Har du konto på norskfjellfestival.no, finner du billettene dine der også – til en hver tid.</p>

<p>Har du ikke konto? <a href="<?php echo home_url('/min-konto') ?>">Lag deg en her</a>, og få alle fremtidige billettkjøp samlet på et og samme sted.</p>

<p>Følg oss gjerne på <a href="<?php echo home_url('/') ?>">nett</a>, <a href="http://facebook.com/norskfjellfestival">facebook</a> og <a href="http://instagram.com/norskfjellfestival">instagram</a> for oppdateringer og inspirasjon.</p>

<?php

/**
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
