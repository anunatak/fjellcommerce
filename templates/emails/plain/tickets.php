<?php
/**
 * Customer completed order email (plain text)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/plain/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author		WooThemes
 * @package 	WooCommerce/Templates/Emails/Plain
 * @version		2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

echo "= " . $email_heading . " =\n\n";

echo sprintf( __( "Hei. Dine billetter til %s er klare og ligger vedlagt denne eposten.", 'woocommerce' ), FjellCommerce()->get_active_festival()->name ) . "\n\n";

echo "Billetten fremvises på arrangementet – printet eller digitalt. Har du konto på norskfjellfestival.no, finner du billettene dine der også – til en hver tid.\n\n";

echo "Har du ikke konto? På ".home_url('/min-konto')." kan du lage en og få alle fremtidige billettkjøp samlet på et og samme sted.\n\n";

echo "Følg oss gjerne på nett (".home_url('/').") nett, Facebook (http://facebook.com/norskfjellfestival) og Instagram (http://instagram.com/norskfjellfestival) for oppdateringer og inspirasjon.\n\n";

echo apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) );
