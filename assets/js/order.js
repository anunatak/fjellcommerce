jQuery(document).ready(function($) {
	$('.edit_ticket').on('click', function(e) {
		e.preventDefault();
		$(this).parent().parent().toggleClass('selected');
		var id = $(this).data('ticket');
		$('tr.ticket_row_edit[data-ticket="'+id+'"]').toggle();
	})

	$('.set-active').on('click', function(e) {
		e.preventDefault();
		var active = $(this).parent().find('.active-input').val()

		var toggled = active === 'yes' ? 'no' : 'yes'
		var toggled_text = toggled === 'yes' ? 'Sett inaktiv' : 'Sett aktiv'
		$(this).parent().find('.active-input').val(toggled)
		$(this).text(toggled_text);
	})

	$('.set-checked').on('click', function(e) {
		e.preventDefault();
		var checked = $(this).parent().find('.checked-input').val()

		var toggled = checked === 'yes' ? 'no' : 'yes'
		var toggled_text = toggled === 'yes' ? 'Sett ikke kontrollert' : 'Sett kontrollert'
		$(this).parent().find('.checked-input').val(toggled)
		$(this).text(toggled_text);
	})
});
