<?php
/**
 * Enables switching between different festivals
 */
class FC_Admin_Switching {

	/**
	 * Hooks everything up.
	 * @return void
	 */
	public static function init() {
        add_action( 'admin_bar_menu', array(__CLASS__, 'add_switcher'), 999 );
        add_action( 'admin_init', array(__CLASS__, 'set_festival') );
        add_action( 'pre_get_posts', array( __CLASS__, 'query_festival' ) );
	}

	/**
	 * Modifies the query to only show items from the selected festival
	 * @param  WP_Query $query
	 * @return void
	 */
	public static function query_festival($query) {

		if(!is_admin()) // we dont want to run this on the front-end
			return;

		if(!$query->is_main_query()) // only apply these rules to the main query
			return;

		if(isset($query->query_vars['nff_program_festival']))
			return;


		if($post_type = get_current_screen()->post_type) { // is it a post type?
			$taxonomies = get_object_taxonomies( get_current_screen()->post_type ); // get the taxonomies for this post type

			if(in_array('nff_program_festival', $taxonomies)) { // is the festival taxonomy attached?

				if(!$tax_query = $query->get('tax_query')) {
					$tax_query = array();
				}
				$tax_query[] = array(
					'relation' => 'OR',
					array( // we'll assume that items that do not have a festival attached are global, thus they will be shown to all festivals
				        'taxonomy' => 'nff_program_festival',
				        'operator' => 'NOT EXISTS'
			        ),
					array( // also add the festival-specific items
				        'taxonomy' => 'nff_program_festival',
				        'terms' => array(self::current_festival()->slug),
				        'field' => 'slug'
			        )
				);
				$query->set('tax_query', $tax_query); // set the taxonomy query
			}
		}
	}

	/**
	 * Adds a neat switcher to the admin bar
	 */
	public static function add_switcher() {
        global $wp_admin_bar;

        $wp_admin_bar->add_node(array(
            'id'    => 'fjellcommerce-festival-switcher',
            'title' => self::get_current_festival_name(),
            'href'  => '#',
        ));

        $festivals = get_terms('taxonomy=nff_program_festival&hide_empty=0');
        foreach($festivals as $festival) {
        	$name = $festival->name;
	        $wp_admin_bar->add_node(array(
	        	'parent' => 'fjellcommerce-festival-switcher',
	            'id'    => 'fjellcommerce-festival-switcher-'. $festival->slug,
	            'title' => $name,
	            'href'  => add_query_arg('set_festival', $festival->slug),
	        ));
        }

	}

	/**
	 * Gets the name of the current festival
	 * @return string
	 */
	public static function get_current_festival_name() {
		return self::current_festival()->name;
	}

	/**
	 * Gets the current festival objext
	 * @return WP_Term
	 */
	public static function current_festival() {
		if(isset($_COOKIE['current_festival'])) {
			$festival = get_term_by( 'slug', $_COOKIE['current_festival'], 'nff_program_festival' );
			return $festival;
		}
		return FjellCommerce()->get_active_festival();
	}

	/**
	 * Sets a cookie with the selected festival
	 */
	public static function set_festival() {
		if ( ! headers_sent() && did_action( 'wp_loaded' ) && isset($_GET['set_festival'])) {
			$expire = ( 12 * HOUR_IN_SECONDS );
			wc_setcookie('current_festival', $_GET['set_festival']);
			wp_redirect( wp_get_referer() );
		}

	}

}

FC_Admin_Switching::init();
