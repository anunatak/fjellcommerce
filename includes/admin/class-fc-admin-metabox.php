<?php
/**
 * Meta box
 *
 * Registers post types and taxonomies.
 *
 * @class     FC_Admin_Metabox
 * @version   2.5.0
 * @package   WooCommerce/Classes/Products
 * @category  Class
 * @author    WooThemes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * FC_Post_types Class.
 */
class FC_Admin_Metabox {


	/**
	 * Hook in methods.
	 */
	public static function init() {
		add_action( 'add_meta_boxes', array( __CLASS__, 'add_meta_box' ), 40 );
		add_action( 'admin_enqueue_scripts', array(__CLASS__, 'scripts') );
		add_action( 'save_post', array( __CLASS__, 'save_meta_box' ) );
	}

	/**
	 * Enqueue scripts and styles for the order page
	 * @return void
	 */
	public static function scripts() {
		if(get_current_screen()->id === 'shop_order') {
			wp_enqueue_script( 'fjellcommerce/admin/order', plugins_url( 'assets/js/order.js', FC_PLUGIN_FILE ), array('jquery'), FjellCommerce()->version );
			wp_enqueue_style( 'fjellcommerce/admin/order', plugins_url( 'assets/css/order.css', FC_PLUGIN_FILE ), array(), FjellCommerce()->version );
		}
	}

	/**
	 * Adds a metabox to the edit order screen if the order has tickets
	 */
	public static function add_meta_box() {
		global $post, $current_screen;

		$order_id = $post->ID;
		$order = wc_get_order( $order_id );
		if ( $order ) {
			$order_key = $order->order_key;
			if ( $tickets = FC_Tickets::create_tickets( $order_key ) ) {
				add_meta_box( 'fjellcommerce-tickets', 'Billetter', array( __CLASS__, 'render_metabox' ), 'shop_order', 'normal', 'high', compact( 'tickets' ) );
			}
		}
	}

	/**
	 * Renders the metabox
	 * @param  WP_Post $post
	 * @param  array $options
	 * @return void
	 */
	public static function render_metabox( $post, $options ) {
		$tickets = $options['args']['tickets'];
		$active = get_post_meta( $post->ID, '_active_ticket', true );
		$checked = get_post_meta( $post->ID, '_checked_tickets', true );
		$order = wc_get_order( $post->ID );
		?>
		<div class="panel-wrap woocommerce">
			<div id="ticket_data" class="panel">
				<h2>Ordren inneholder billetter</h2>
				<p class="ticket_info">
					Du kan endre disse billettene her. Husk å trykke "Lagre ordre" etter du har redigert en billett. <br><a href="<?php echo home_url('/ticket/'. $order->order_key) ?>">Se billettene i PDF</a>
				</p>
			</div>
			<table class="ticket_table">
				<thead>
					<tr class="ticket_header">
						<th class="ticket_info">
							Billett
						</th>
						<th class="ticket_active">
							Aktiv?
						</th>
						<th class="ticket_checked">
							Kontrollert?
						</th>
					</tr>
				</thead>
				<tbody>
				<?php
				foreach ( $tickets['tickets'] as $ticket_order => $ticket ) {
					$active = get_post_meta( $post->ID, '_active_ticket_'. $ticket_order, true );
					$checked = get_post_meta( $post->ID, '_checked_ticket_'. $ticket_order, true );
					?>
					<tr class="ticket_row">
						<td class="ticket_info">
							Billett #<?php echo $ticket_order + 1 ?> til <?php echo $ticket['title'] ?> (<?php echo ucfirst( date_i18n( 'j. F Y', $ticket['date']->format( 'U' ) ) ) ?>)<br />
							<a href="#" class="edit_ticket" data-ticket="<?php echo $ticket_order + 1 ?>">Rediger</a>
						</td>
						<td class="ticket_active">
							<?php echo $active === 'no' ? 'Nei' : 'Ja' ?>
						</td>
						<td class="ticket_checked">
							<?php echo $checked === 'yes' ? 'Ja' : 'Nei' ?>
						</td>
					</tr>
					<tr class="ticket_row_edit" data-ticket="<?php echo $ticket_order + 1 ?>" style="display:none">
						<td class="ticket_edit_col" colspan="3">
							<p>
								<button class="button-secondary set-active"><?php echo $active === 'no' ? 'Sett aktiv' : 'Sett inaktiv' ?></button>
								<button class="button-secondary set-checked"><?php echo $checked === 'yes' ? 'Sett ikke kontrollert' : 'Sett kontrollert' ?></button>
								<input class="active-input" type="hidden" name="active_tickets[<?php echo $ticket_order + 1 ?>]" value="<?php echo $active === 'no' ? 'no' : 'yes' ?>">
								<input class="checked-input" type="hidden" name="checked_tickets[<?php echo $ticket_order + 1 ?>]" value="<?php echo $checked === 'yes' ? 'yes' : 'no' ?>">
							</p>
							<?php do_action( 'fjellcommerce_edit_ticket_form', $ticket, $ticket_order + 1, $order ); // hook into this to add fields to edit screen. See 'fjellcommerce-information' for implementation example ?>
						</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>

		</div>
		<?php
	}

	/**
	 * Saves the content of the metabox
	 * @param  integer $order_id
	 * @return void
	 */
	public static function save_meta_box( $order_id ) {
		if ( get_post_type( $order_id ) === 'shop_order' && ( isset( $_POST['ticket'] ) || isset( $_POST['active_tickets'] ) ) ) {
			$order = wc_get_order( $order_id );
			if ( $order ) {
				$order_key = $order->order_key;
				if ( $tickets = FC_Tickets::create_tickets( $order_key ) ) {
					$active_tickets = array();
					$checked_tickets = array();
					foreach ( $tickets['tickets'] as $ticket_id => $ticket ) {
						$active_tickets[$ticket_id] = $_POST['active_tickets'][$ticket_id];
						$checked_tickets[$ticket_id] = $_POST['checked_tickets'][$ticket_id];
						do_action( 'fjellcommerce_edit_ticket_save', $ticket, $ticket_id + 1, $order_id, $_POST );// hook into this to save field values. See 'fjellcommerce-information' for implementation example
					}

					foreach($active_tickets as $ticket_id => $value) {
						update_post_meta( $order_id, '_active_ticket_'. $ticket_id + 1, $value ); // set the ticket status
					}
					foreach($checked_tickets as $ticket_id => $value) {
						update_post_meta( $order_id, '_checked_ticket_'. $ticket_id + 1, $value ); // set if the ticket has been checked
					}
					// update_post_meta( $order_id, '_active_tickets', $active_tickets );
				}
			}
		}
	}



}

FC_Admin_Metabox::init();
