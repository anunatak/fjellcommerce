<?php
/**
 * Hides elements in the menu based on which festival is being edited
 */
class FC_Admin_Menu {

	/**
	 * Hooks everything up
	 * @return void
	 */
	public static function init() {
		add_action( 'admin_menu', array(__CLASS__, 'hide_items') );
	}

	/**
	 * Hides elements
	 * @return void
	 */
	public static function hide_items() {
		global $menu;
		if(!current_user_can( 'manage_options' )) {
			$current = FC_Admin_Switching::current_festival();
			$festival_settings = array(
				'trollveggen-triathlon' => array( // the slug to the festival in the taxonomy
					'hidden' => array(
						'toplevel_page_woocommerce', // <- the id of the top-level page from WP
						'toplevel_page_woocommerce-pos',
						'menu-posts-nff_mountain_kings',
						'menu-posts-product'
					)
				),
				'camp-romsdal' => array(
					'hidden' => array(
						'toplevel_page_woocommerce',
						'toplevel_page_woocommerce-pos',
						'menu-posts-nff_mountain_kings',
						'menu-posts-product'
					)
				),
				'romsdalseggenlopet' => array(
					'hidden' => array(
						'toplevel_page_woocommerce',
						'toplevel_page_woocommerce-pos',
						'menu-posts-nff_mountain_kings',
						'menu-posts-product'
					)
				)
			);
			$settings = isset($festival_settings[$current->slug]) ? $festival_settings[$current->slug] : array('hidden' => array());
			foreach($menu as $index => $item) {
				foreach($item as $el) {
					if(in_array($el, $settings['hidden'])) {
						unset($menu[$index]);
					}
				}
			}
		}
	}

}

FC_Admin_Menu::init();
