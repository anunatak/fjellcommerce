<?php
/**
 * Sets up the dashboard
 */
class FC_Admin_Dashboard {

	/**
	 * Hooks everything up
	 * @return void
	 */
	public static function init() {
        add_action('wp_dashboard_setup', array(__CLASS__, 'dashboard_setup'));
        add_action( 'admin_enqueue_scripts', array(__CLASS__, 'scripts') );
        add_action( 'wp_ajax_fjellcommerce_save_active_festival', array(__CLASS__, 'save_active_festival') );
        add_action( 'wp_ajax_fjellcommerce_save_active_year', array(__CLASS__, 'save_active_year') );
        add_action( 'wp_ajax_fjellcommerce_get_new_festival_modal', array(__CLASS__, 'new_festival_modal') );
        add_action( 'wp_ajax_fjellcommerce_get_new_festival_process', array(__CLASS__, 'new_festival_process') );
        add_action( 'wp_ajax_fjellcommerce_get_new_festival_batch', array(__CLASS__, 'new_festival_batch') );
	}

	/**
	 * Adds a dashboard widget so the client can manage the festivals
	 * @return void
	 */
	public static function dashboard_setup() {
		wp_add_dashboard_widget('fjellcommerce_manage_festivals', 'Håndter festivaler', array(__CLASS__, 'dashboard_widget'));
	}

	/**
	 * Renders the manage-widget
	 * @return void
	 */
	public static function dashboard_widget() {
		// $active = FjellCommerce()->get_active_festival();
		if(current_user_can( 'manage_woocommerce' ) || current_user_can( 'manage_options' )) : // only for shop managers or admins
		?>
		<p><strong>Opprett nytt år</strong></p>
		<p class="description">Du kan opprette ett nytt år på en festival kjapt og enkelt.</p>
		<p><button class="button-primary" id="create-festival">Opprett år</button></p>

		<p><strong>Velg aktiv festival</strong></p>
		<p class="description">Valget ditt lagres automatisk.</p>
		<p id="progress-festival" style="color:green;display:none"></p>
		<form id="active_festival_forms">
			<?php foreach(get_terms('taxonomy=nff_program_festival&hide_empty=0') as $festival) : ?>
				<p><label><input type="radio" class="festival-select" data-festival="<?php echo $festival->term_id ?>" name="festival_select" <?php checked( $festival->term_id, FjellCommerce()->get_active_festival()->term_id ) ?>> <?php echo $festival->name ?></label></p>
			<?php endforeach ?>
		</form>

		<p><strong>Sett aktivt år</strong></p>
		<p class="description">Valget ditt er lagret når du har trykket "Lagre".</p>
		<p id="progress-year" style="color:green;display:none"></p>
		<form id="active_year_forms" method="POST">
			<p>
				<input type="text" class="festival-year" value="<?php echo FjellCommerce()->get_active_year() ?>">
				<input type="submit" class="button-primary" value="Lagre">
			</p>
		</form>

		<p><strong>Festivalinnstillinger</strong></p>
		<?php foreach(get_terms('taxonomy=nff_program_festival&hide_empty=0') as $festival) : ?>
			<p>
				<?php echo $festival->name ?>
				<?php self::festival_options($festival) ?>
			</p>
		<?php endforeach ?>
		<?php
		endif;
	}

	/**
	 * Renders a menu with shortcuts for each festival
	 * @param  WP_Term $festival
	 * @return void
	 */
	public static function festival_options($festival) {
		$options = array();
		$options[] = '<a href="'. self::get_festival_settings($festival) .'">Innstillinger</a>';
		if($front_page = self::get_festival_front_page($festival)) {
			$options[] = '<a href="'. get_edit_post_link($front_page->ID) .'">Forside</a>';
		}
		if($options)
			echo ' – '. implode( ' | ', $options );
	}

	/**
	 * Gets the shortcut to the festival edit link
	 * @param  WP_Term $festival
	 * @return string
	 */
	public static function get_festival_settings($festival) {
		return get_edit_term_link( $festival->term_id, 'nff_program_festival' );
	}

	/**
	 * Tries to find the front page for a festival
	 * @param  WP_Term $festival
	 * @return WP_Post
	 */
	public static function get_festival_front_page($festival) {
		$pages = get_posts(array(
			'post_type' => 'page',
			'meta_query' => array(
		        array(
		            'key'   => '_wp_page_template',
		            'value' => 'page-templates/front-page.php'
		        )
		    ),
		    'tax_query' => array(
		    	array(
		    		'taxonomy' => 'nff_program_festival',
		    		'terms' => $festival->slug,
		    		'field' => 'slug'
		    	)
		    )
		));

		if($pages) {
			return $pages[0];
		}

		return false;
	}

	/**
	 * Adds inline script for the widget
	 * @param  strubg $hook
	 * @return void
	 */
	public static function scripts($hook) {
		if($hook === 'index.php') {
			wp_enqueue_style( 'fjellcommerce/dashboard', plugins_url( 'assets/css/dashboard.css', FC_PLUGIN_FILE ) );
			wp_add_inline_script( 'common', "
				jQuery(document).ready(function($) {
					$('input.festival-select').on('click', function() {
						$.post(ajaxurl, {action: 'fjellcommerce_save_active_festival', festival: $(this).data('festival')}, function() {
							$('#progress-festival').fadeIn();
							$('#progress-festival').text('Lagret!');
							setTimeout(function() {
								$('#progress-festival').fadeOut();
							}, 2000);
						})
					})
					$('#active_year_forms').on('submit', function(e) {
						e.preventDefault()
						$.post(ajaxurl, {action: 'fjellcommerce_save_active_year', year: $('input.festival-year').val()}, function() {
							$('#progress-year').fadeIn();
							$('#progress-year').text('Lagret!');
							setTimeout(function() {
								$('#progress-year').fadeOut();
							}, 2000);
						})
					})
					$('#create-festival').on('click', function(e) {
						e.preventDefault()
						$.get(ajaxurl, {action: 'fjellcommerce_get_new_festival_modal'}, function(response) {
							$('body').append(response);
						})
					});
					$(document).on('click', '#create-new-festival-submit', function(e) {
						e.preventDefault()
						var festival = $('#festival-festival').val();
						var year = $('#festival-year').val();
						var new_year = $('#festival-new-year').val();
						$.post(ajaxurl, {action: 'fjellcommerce_get_new_festival_process', year: year, festival: festival, new_year: new_year}, function(response) {
							if(response.success) {
								$('.festival-modal-content').html(`
									<h2>Fant \${response.data.total} arrangementer.</h2>
									<p class='description'>Alle disse arrangementene vil bli kopiert til det året du valgte.</div>
									<p><button class='button-primary' id='copy-items-now' data-total='\${response.data.total}' data-new='\${new_year}' data-year='\${year}' data-festival='\${festival}'>Kopier nå!</button></p>
								`);
							} else {
								jQuery.each(response.data, function(index, item) {
									$(item.id + '-error').html(item.message);
								})
							}
						})
					});
					function batchAjax(festival, year, new_year, start_from) {
						$.post(ajaxurl, {action: 'fjellcommerce_get_new_festival_batch', year: year, festival: festival, new_year: new_year, start_from: start_from}, function(response) {
							if(response.success) {
								if(response.data.has_more) {
									start_from = start_from + 10;
									batchAjax(festival, year, new_year, start_from);
									$('#copy-status').text(start_from);
								} else {
									jQuery('#new-festival-modal').fadeOut(function() {
										window.location = response.data.url;
									})
								}
							} else {
								jQuery.each(response.data, function(index, item) {
									$(item.id + '-error').html(item.message);
								})
							}
						})
					}
					$(document).on('click', '#copy-items-now', function(e) {
						e.preventDefault()
						var festival = $(this).data('festival');
						var year = $(this).data('year');
						var new_year = $(this).data('new');
						var start_from = 0;
						var total = $(this).data('total');
						batchAjax(festival, year, new_year, start_from);
						$('.festival-modal-content').html(`
							<h2>Kopierer arrangementer</h2>
							<span id='copy-status'>\${start_from}</span> av \${total}
						`);
					});
				})
			" );
		}
	}

	/**
	 * Ajax action to set the current active festival
	 * @return void
	 */
	public static function save_active_festival() {
		if(current_user_can( 'manage_woocommerce' ) || current_user_can( 'manage_options' )) { // only for shop managers or admins
			update_option( 'options_active_festival', $_REQUEST['festival'] );
		}
		wp_send_json_error();
		die;
	}

	/**
	 * Ajax action to set the current active festival year
	 * @return void
	 */
	public static function save_active_year() {
		if(current_user_can( 'manage_woocommerce' ) || current_user_can( 'manage_options' )) { // only for shop managers or admins
			update_option( 'options_active_year', $_REQUEST['year'] );
		}
		wp_send_json_error();
		die;
	}

	/**
	 * Ajax action to process a new festival
	 * @return void
	 */
	public static function new_festival_process() {
		if(current_user_can( 'manage_woocommerce' ) || current_user_can( 'manage_options' )) { // only for shop managers or admins
			$festival = isset($_REQUEST['festival']) ? $_REQUEST['festival'] : '';
			$year = isset($_REQUEST['year']) ? $_REQUEST['year'] : '';
			$new_year = isset($_REQUEST['new_year']) ? $_REQUEST['new_year'] : '';

			$errors = array();
			if(!$festival) {
				$errors[] = array(
					'id' => '#festival-festival',
					'message' => 'Du må velge en festival.'
				);
			}
			if(!$year) {
				$errors[] = array(
					'id' => '#festival-year',
					'message' => 'Du må velge ett år.'
				);
			}
			if(!$new_year) {
				$errors[] = array(
					'id' => '#festival-new-year',
					'message' => 'Du må skrive inn ett nytt år.'
				);
			}
			if($new_year === $year) {
				$errors[] = array(
					'id' => '#festival-new-year',
					'message' => 'Nytt år kan ikke hete det samme som året du vil kopiere.'
				);
			}

			if($errors) {
				wp_send_json_error($errors);
			} else {
				$duplicate = new FC_Duplicate($festival, $year, $new_year);
				wp_send_json_success( array('total' => $duplicate->before()) );
			}
		}
		wp_send_json_error([]);
		die;
	}

	/**
	 * Ajax action to process a new festival
	 * @return void
	 */
	public static function new_festival_batch() {
		if(current_user_can( 'manage_woocommerce' ) || current_user_can( 'manage_options' )) { // only for shop managers or admins
			$festival = isset($_REQUEST['festival']) ? $_REQUEST['festival'] : '';
			$year = isset($_REQUEST['year']) ? $_REQUEST['year'] : '';
			$new_year = isset($_REQUEST['new_year']) ? $_REQUEST['new_year'] : '';
			$start_from = isset($_REQUEST['start_from']) ? $_REQUEST['start_from'] : 0;


			$duplicate = new FC_Duplicate($festival, $year, $new_year);
			$before = $duplicate->before();
			$result = $duplicate->batch($start_from);
			$has_more = true;
			if(($start_from + 10) > $before) {
				$has_more = false;
			}
			wp_send_json_success( array('has_more' => $has_more, 'url' => admin_url('edit.php?post_type=nff_program&nff_program_year='.$new_year.'&nff_program_festival='.$festival.'')) );
		}
		wp_send_json_error([]);
		die;
	}

	/**
	 * Ajax action to get the "new festival modal"
	 * @return void
	 */
	public static function new_festival_modal() {
		?>
		<div id="new-festival-modal">
			<div class="festival-modal-content">
				<h2>Opprett nytt år</h2>
				<p class="description">Når du oppretter et nytt år kopierer du alle arrangement fra disse årene over til et nytt år.</p>
				<p>
				<strong>Velg først festivalen du ønsker å bruke som mal</strong><br>
				<?php wp_dropdown_categories( array(
					'show_option_all' =>  __("Velg festival"),
					'taxonomy'        =>  'nff_program_festival',
					'name'            => 'festival-festival',
					'orderby'         =>  'name',
					'selected'        =>  FC_Admin_Switching::current_festival()->slug,
					'value_field'     => 'slug',
					'hierarchical'    =>  true,
					'depth'           =>  3,
					'show_count'      =>  false, // Show # listings in parens
					'hide_empty'      =>  true, // Don't show businesses w/o listings
				) ) ?>
				<br><span class="error" id="festival-festival-error"></span>
				</p>

				<p>
				<strong>Velg så hvilket år du ønsker å kopiere</strong><br>
				<?php
		        wp_dropdown_categories(array(
					'show_option_all' =>  __("Velg år"),
					'taxonomy'        =>  'nff_program_year',
					'name'            =>  'festival-year',
					'orderby'         =>  'name',
					'selected'        =>  FjellCommerce()->get_active_year(),
					'value_field'     => 'slug',
					'hierarchical'    =>  true,
					'depth'           =>  3,
					'show_count'      =>  false, // Show # listings in parens
					'hide_empty'      =>  true, // Don't show businesses w/o listings
		        )); ?>
		        <br><span class="error" id="festival-year-error"></span>
				</p>

				<p>
					<strong>Hvilket år er det du kopierer til?</strong><br>
					<input type="text" value="<?php echo FjellCommerce()->get_active_year() + 1 ?>" id="festival-new-year">
					<br><span class="error" id="festival-new-year-error"></span>
				</p>

				<p>
					<button class="button-primary" id="create-new-festival-submit">Opprett nytt år</button>
				</p>
			</div>
		</div>
		<?php
		die;
	}
}

FC_Admin_Dashboard::init();
