<?php
/**
 * Post Types
 *
 * Registers post types and taxonomies.
 *
 * @class     FC_Admin_Sync
 * @version   2.5.0
 * @package   WooCommerce/Classes/Products
 * @category  Class
 * @author    WooThemes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * FC_Post_types Class.
 */
class FC_Admin_Sync {


	/**
	 * Hook in methods.
	 */
	public static function init() {
		add_action( 'save_post', array( __CLASS__, 'sync' ), 100, 2 );
		add_action('wp_ajax_sync_all_events', array(__CLASS__, 'sync_all_events'));
		add_action('wp_ajax_fix_products', array(__CLASS__, 'fix_products'));
		add_filter('acf/load_value/key=field_nff_program_7', array(__CLASS__, 'sync_tickets_qty'), 10, 3);
		// add_action('wp_ajax_sync_all_orders', array(__CLASS__, 'sync_all_orders'));
	}

	/**
	 * Fixes so product start dates are stored searchable
	 * @return void
	 */
	public static function fix_products() {
		$products = get_posts(array(
			'post_type'      => 'product',
			'product_cat'    => 'Events',
			'order'          => 'ASC',
			'meta_key'       => 'event_date',
			'orderby'        => 'meta_value_num',
			'posts_per_page' => -1
		));

		foreach($products as $product) {
			$date = $product->event_date;
			$date = date_create_from_format('d.m.Y', $date);
			update_post_meta($product->ID, 'event_date', $date->format('Ymd'));
		}
		die;
	}

	/**
	 * Syncs the quantity in the tickets types if there are only one event
	 * @param  string $value
	 * @param  integer $post_id
	 * @param  array $field_options
	 * @return mixed
	 */
	public static function sync_tickets_qty($value, $post_id, $field_options) {
		$products = get_post_meta($post_id, 'event_products', true);

		if(count($products) === 1) {
			if(isset($products[0])) {
				$exisiting_variations = get_posts( 'post_type=product_variation&post_parent='. $products[0] );
				if($exisiting_variations) {
					foreach($value as $index => $val) {
						$variation = $exisiting_variations[$index];
						if($variation->_manage_stock === 'yes') {
							$value[$index]['field_nff_program_7_2X231'] = (int)$variation->_stock;
						}
					}
				}
			}
		}
		return $value;
	}

	/**
	 * An ajax action that can be used to perform a mass sync of all events in "nff_program"-post type with WooCommerce
	 * @return void
	 */
	public function sync_all_events() {
		$posts = get_posts('post_type=nff_program&posts_per_page=-1');
		$created = 0;
		foreach($posts as $post) {
			$products = get_posts('post_type=product&meta_key=fjellcommerce_post&meta_value='. $post->ID);
			if(!$products) {
				self::sync($post->ID, $post);
				$created++;
			}
		}

		echo 'Created '. $created .' events.';

		die;
	}

	/**
	 * Sync the event with WooCommerce
	 *
	 * @return void
	 */
	public static function sync( $post_id, $post ) {

		// If this is just a revision, don't send the email.
		if ( wp_is_post_revision( $post_id ) )
			return;

		// If this isn't a 'program' post, don't update it.
		if ( 'nff_program' != $post->post_type ) return;

		if( 'draft' === $post->post_status ) return;

		// Easy access
		$woocommerce = FjellCommerce()->WC;

		// Events and tickets
		$events = get_field( 'nff_program_events', $post_id );
		$tickets = get_field( 'nff_program_tickets_tickets', $post_id );

		// Get the default sales start for the selected festival
		$festival = wp_get_object_terms( $post_id, 'nff_program_festival' );
		$festival = array_pop( $festival );
		$sale_start_default = get_field( 'sales_start', $festival );
		$sale_stop_default = get_field( 'sales_stop', $festival );

		// Sync all events
		$event_products = array();
		if($events) {
			foreach ( $events as $event_id => $event ) {
				$args = array(
					'title'                => get_the_title($post_id) . ' – ' . $event['nff_program_date'],
					'quantity'             => $event['nff_program_tickets_quantity'],
					'post'                 => $post_id,
					'event'                => $event_id,
					'event_date'           => $event['nff_program_date'],
					'event_time'           => $event['nff_program_time'],
					'age_from'             => get_field('nff_program_ages_from', $post_id),
					'age_to'               => get_field('nff_program_ages_to', $post_id),
					'sales_start'          => $event['sales_start'] ? $event['sales_start'] : $sale_start_default,
					'sales_stop'           => $event['sales_stop'] ? $event['sales_stop'] : $sale_stop_default,
					'checkout_rule'        => get_field('checkout_rule', $post_id),
					'ticket_type'          => get_field('nff_program_tickets_ticket_type', $post_id),
					'requires_information' => get_field('requires_information', $post_id),
					'venue'                => wp_get_object_terms( $post_id, 'nff_program_venue', array('fields' => 'tt_ids') ),
					'category'             => wp_get_object_terms( $post_id, 'nff_program_category', array('fields' => 'tt_ids') ),
					'activity'             => wp_get_object_terms( $post_id, 'nff_program_activity', array('fields' => 'tt_ids') ),
					'tags'                 => wp_get_object_terms( $post_id, 'nff_program_tags', array('fields' => 'tt_ids') ),
					'festival'             => wp_get_object_terms( $post_id, 'nff_program_festival', array('fields' => 'tt_ids') ),
					'year'                 => wp_get_object_terms( $post_id, 'nff_program_year', array('fields' => 'tt_ids') )
				);

				$variations = array();
				// each ticket should be a variation on the product
				foreach($tickets as $ticket_id => $ticket) {
					$variations[] = array(
						'title' => $ticket['name'],
						'price' => $ticket['price'],
						'quantity' => $ticket['quantity'] !== '' ? $ticket['quantity'] : false
					);
				}

				// save the product via our woocommerce api
				$product = $woocommerce->save( $args, $variations );

				// get the product id and put it to an array
				$event_products[$event_id] = $product->get_product_id();
			}

			// store all product ids
			update_post_meta( $post_id, 'event_products', $event_products );
		}
	}

	/**
	 * An ajax action to sync order from the "nff_program_ticket" post type to WooCommerce
	 *
	 * !!Does not work, and is not hooked into the AJAX-system!!
	 *
	 * @return void
	 * @todo  Maybe complete and make sure it works?
	 */
	public static function sync_all_orders() {
		static::sync_cc_orders();
		die;
	}

	protected static function sync_cc_orders() {

		$posts = get_posts(array(
			'post_type' => 'nff_program_ticket',
			'posts_per_page' => -1,
			'post_status' => 'any',
			'meta_query' => array(
				array(
					'key' => '_stripe_charge',
					'compare' => 'EXISTS'
				),
				array(
					'key' => 'pay',
					'value' => '1'
				)
			)
		));
		// var_dump($posts);

		self::sync_order(4524, get_post(4524));
	}

	/**
	 * Sync point of sale orders
	 * @return void
	 * @todo  Implement?
	 */
	protected function sync_pos_orders() {

	}

	/**
	 * Syncs a order with the WooCommerce
	 * @param  integer $post_id
	 * @param  WP_Post $post
	 * @return void
	 */
	public function sync_order($post_id, $post) {

		// If this is just a revision, don't send the email.
		if ( wp_is_post_revision( $post_id ) )
			return;

		// If this isn't a 'ticket' post, don't update it.
		if ( 'nff_program_ticket' != $post->post_type ) return;

		// create a new woocommerce order
		$order = new WC_Order();

		// get buyer basics
		$buyer_name  = get_post_meta($post_id, 'buyer_0_name', true);
		$buyer_phone = get_post_meta($post_id, 'buyer_0_phone', true);
		$buyer_email = get_post_meta($post_id, 'buyer_0_email', true);
		$buyer_user  = get_post_meta($post_id, 'buyer_0_user', true);

		// items
		$items = static::get_product_items($post);
		$refunded = static::get_refunded_product_items($post);

		// set order data
		$order->set_billing_first_name(static::get_first_name($buyer_name));
		$order->set_billing_last_name(static::get_last_name($buyer_name));
		$order->set_billing_phone($buyer_phone);
		$order->set_billing_email('tormorten@tormorten.no');

		if($buyer_user) {
			$this->set_customer($buyer_user);
		}

		$order->set_payment_method('stripe');
		$order->set_payment_method_title('Kredittkort');
		$order->set_created_via('Web');
		$order->payment_complete(get_post_meta($post_id, '_stripe_charge', true));
		$order->set_date_completed(get_the_time( 'U', $post_id ));
		$order->set_date_paid(get_the_time( 'U', $post_id ));
		$order->set_status('completed');
		$order->save();

		// add products to order
		foreach($items as $item) {
			$qty = $item['quantity'];
			$product = $order->add_product($item['product'], $qty, array(
				'name'         => $item['product']->get_name(),
				'tax_class'    => $item['variation']->get_tax_class(),
				'product_id'   => $item['product']->id,
				'variation_id' => $item['variation']->id,
				'variation'    => $item['variation']->get_attributes(),
				'subtotal'     => wc_get_price_excluding_tax( $item['variation'], array( 'qty' => $qty ) ),
				'total'        => wc_get_price_excluding_tax( $item['variation'], array( 'qty' => $qty ) ),
				'quantity'     => $qty,
				'meta_data'    => array(
					'comment' => $item['comment'],
					'name' => $item['name'],
					'email' => $item['email'],
					'phone' => $item['phone'],
					'checked' => $item['checked'],
					'seatmap' => $item['seatmap']
				)
			));
		}

		// set totals
		$order->calculate_totals(true);

	}

	/**
	 * Gets the items of a product in the old order system
	 * @param  string $post
	 * @return array
	 */
	protected static function get_product_items($post) {
		$tickets = get_post_meta($post->ID, 'tickets', true);
		$items = array();
		for ($i=0; $i < $tickets; $i++) {
			$checked      = get_post_meta($post->ID, "tickets_{$i}_ticket_checked", true);
			$post_id      = get_post_meta($post->ID, "tickets_{$i}_post_id", true);
			$event_id     = get_post_meta($post->ID, "tickets_{$i}_event_id", true);
			$ticket_id    = get_post_meta($post->ID, "tickets_{$i}_ticket_id", true);
			$comment      = get_post_meta($post->ID, "tickets_{$i}_comment", true);
			$user_name    = get_post_meta($post->ID, "tickets_{$i}_user_0_name", true);
			$user_phone   = get_post_meta($post->ID, "tickets_{$i}_user_0_phone", true);
			$user_email   = get_post_meta($post->ID, "tickets_{$i}_user_0_email", true);
			$seatmap_row  = get_post_meta($post->ID, "tickets_{$i}_seatmap_0_row", true);
			$seatmap_seat = get_post_meta($post->ID, "tickets_{$i}_seatmap_0_seat", true);

			$products = get_posts(array(
				'post_type' => 'product',
				'posts_per_page' => 1,
				'meta_query' => array(
					array(
						'fjellcommerce_post' => $post_id,
						'fjellcommerce_event' => $event_id
					)
				)
			));

			$product = wc_get_product($products[0]);

			$variations = get_posts(array(
				'name' => 'product-'.$product->id.'-variation-'.$ticket_id,
				'post_type' => 'product_variation',
				'posts_per_page' => 1
			));

			$variation = wc_get_product($variations[0]);

			$items[] = array(
				'product' => $product,
				'variation' => $variation,
				'quantity' => 1,
				'comment' => $comment,
				'name' => $name,
				'email' => $email,
				'phone' => $phone,
				'checked' => $checked === '1' ? true : false,
				'seatmap' => array(
					'row' => $seatmap_row,
					'seat' => $seatmap_seat
				)
			);
		}

		return $items;

	}

	/**
	 * Gets the refunded items of a product in the old order system
	 * @param  string $post
	 * @return array
	 */
	protected static function get_refunded_product_items($post) {
		$tickets = get_post_meta($post->ID, '_refunded_tickets', true);
		for ($i=0; $i < $tickets; $i++) {
			$checked      = get_post_meta($post->ID, "tickets_{$i}_ticket_checked", true);
			$post_id      = get_post_meta($post->ID, "tickets_{$i}_post_id", true);
			$event_id     = get_post_meta($post->ID, "tickets_{$i}_event_id", true);
			$ticket_id    = get_post_meta($post->ID, "tickets_{$i}_ticket_id", true);
			$comment      = get_post_meta($post->ID, "tickets_{$i}_comment", true);
			$user_name    = get_post_meta($post->ID, "tickets_{$i}_user_0_name", true);
			$user_phone   = get_post_meta($post->ID, "tickets_{$i}_user_0_phone", true);
			$user_email   = get_post_meta($post->ID, "tickets_{$i}_user_0_email", true);
			$seatmap_row  = get_post_meta($post->ID, "tickets_{$i}_seatmap_0_row", true);
			$seatmap_seat = get_post_meta($post->ID, "tickets_{$i}_seatmap_0_seat", true);
			var_dump($user_name);
		}

	}

	/**
	 * Gets the first name
	 * @param  string $name
	 * @return string
	 */
	protected static function get_first_name($name) {
		$fragments = explode(' ', $name);
		$first_name = str_replace(end($fragments), '', $name);
		return trim($first_name);
	}

	/**
	 * Gets the first name
	 * @param  string $name
	 * @return string
	 */
	protected static function get_last_name($name) {
		$fragments = explode(' ', $name);
		return trim(end($fragments));
	}


}

FC_Admin_Sync::init();
