<?php
/**
 * Sends an email when an order is marked as completed
 */
class FC_Emails_Ticket extends WC_Email {

	/**
	 * Constructor.
	 */
	public function __construct() {

		$this->id             = 'customer_order_tickets';
		$this->customer_email = true;
		$this->title          = 'Billetter';
		$this->description    = 'Epost sendt med billetter etter at ordren er fullført.';

		$this->heading        = 'Dine billetter til '. FjellCommerce()->get_active_festival()->name;
		$this->subject        = 'Dine billetter til '. FjellCommerce()->get_active_festival()->name;

		$this->template_html  = 'emails/tickets.php';
		$this->template_plain = 'emails/plain/tickets.php';

		// Triggers for this email
		add_action( 'fjellcommerce_send_order_tickets', array( $this, 'trigger' ) );

		// Call parent constuctor
		parent::__construct();
	}

	/**
	 * Trigger.
	 *
	 * @param int $order_id
	 */
	public function trigger( $order_id ) {


		if ( $order_id ) {
			$this->object                  = wc_get_order( $order_id );
			$this->recipient               = $this->object->billing_email;

			$this->find['order-date']      = '{order_date}';
			$this->find['order-number']    = '{order_number}';

			$this->replace['order-date']   = date_i18n( wc_date_format(), strtotime( $this->object->order_date ) );
			$this->replace['order-number'] = $this->object->get_order_number();
		}

		if ( ! $this->get_recipient() ) {
			return;
		}

		$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
	}

	/**
	 * Get email subject.
	 *
	 * @access public
	 * @return string
	 */
	public function get_subject() {
		return apply_filters( 'woocommerce_email_subject_customer_order_tickets', $this->format_string( $this->subject ), $this->object );
	}

	/**
	 * Get email heading.
	 *
	 * @access public
	 * @return string
	 */
	public function get_heading() {
		return apply_filters( 'woocommerce_email_heading_customer_order_tickets', $this->format_string( $this->heading ), $this->object );
	}

	/**
	 * Get email attachments.
	 *
	 * @return string
	 */
	public function get_attachments() {
		$attachments = array();
		if( isset($this->object->order_key) && $this->object->order_key && $pdf = FC_Tickets::create_pdf( $this->object->order_key ) ) {
			$attachments[] = $pdf->save();
		}
		return $attachments;
	}

	/**
	 * Get content html.
	 *
	 * @access public
	 * @return string
	 */
	public function get_content_html() {
		return wc_get_template_html( $this->template_html, array(
			'order'         => $this->object,
			'email_heading' => $this->get_heading(),
			'sent_to_admin' => false,
			'plain_text'    => false,
			'email'			=> $this
		), '', plugin_dir_path(FC_PLUGIN_FILE) . 'templates/' );
	}

	/**
	 * Get content plain.
	 *
	 * @return string
	 */
	public function get_content_plain() {
		return wc_get_template_html( $this->template_plain, array(
			'order'         => $this->object,
			'email_heading' => $this->get_heading(),
			'sent_to_admin' => false,
			'plain_text'    => true,
			'email'			=> $this
		), '', plugin_dir_path(FC_PLUGIN_FILE) . 'templates/' );
	}

}
