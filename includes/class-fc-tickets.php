<?php
/**
 * Tickets
 *
 * @class     FC_Tickets
 * @version   2.5.0
 * @package   WooCommerce/Classes/Products
 * @category  Class
 * @author    WooThemes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

include_once 'pdf/class-fc-pdf-ticket.php';

/**
 * FC_Tickets Class.
 */
class FC_Tickets {

	/**
	 * Hook in methods.
	 */
	public static function init() {
		add_action( 'parse_request', array( __CLASS__, 'ticket_view' ) );
	}

	/**
	 * Creates a new PDF instance
	 * @param  string $id WooCommerce Order Key
	 * @return FC_PDF_Tickets
	 */
	public static function create_pdf($id) {
		$tickets = static::create_tickets($id);
		if(!$tickets) {
			return false;
		}
		return new FC_PDF_Tickets($tickets['tickets'], $tickets['summary']);
	}

	/**
	 * Find all tickets from the selected order
	 * @param  string $id WooCommerce Order Key
	 * @return array
	 */
	public static function create_tickets($id) {
		if(substr($id, 0, 6) === 'order_') {
			$id = str_replace('order_', '', $id);
		}
		if(substr($id, 0, 9) !== 'wc_order_') {
			$id = 'wc_order_'. $id;
		}
		$order_key = str_replace('wc_', '', $id);
		$order_key = 'wc_'. $order_key;
		$order_id = wc_get_order_id_by_order_key( $order_key );
		$order = wc_get_order($order_id);
		if(!$order) {
			return false;
		}
		$tickets = array();
		$summary = array(
			'total' => $order->get_total(),
			'order_id' => $order_id,
			'order' => $order,
			'items' => array()
		);
		$ticket_order = 0;

		foreach($order->get_items() as $item) {
			$product_id = $item['product_id'];
			if(has_term( 'Events', 'product_cat', $product_id )) {
				$post = get_post($product_id);
				$event = get_post($post->fjellcommerce_post);
				$time = str_replace( '.', ':', substr($post->event_time, 0, 5) );
				$date = $post->event_date;
				$date = date_create_from_format( 'Ymd H:i', $date . ' ' . $time );
				$ticket_type = array();
				$ticket_type_slugs = array();
				$attributes = wc_get_product($item['variation_id'])->get_variation_attributes();
				foreach($attributes as $taxonomy => $slug) {
					$term = get_term_by('slug', $slug, str_replace('attribute_', '', $taxonomy));
					$ticket_type[] = $term->name;
					$ticket_type_slugs[] = $term->slug;
				}
				$summary['items'][] = array(
					'name' => $post->post_title,
					'ticket_type' => implode(', ', $ticket_type),
					'qty' => $item['qty'],
					'total' => $item['line_total']
				);
				for ($i=1; $i <= $item['qty']; $i++) {
					$tickets[] = apply_filters( 'fjellcommerce_ticket_array', array( // this filter can be used to add stuff to the ticket array. see 'fjellcommerce-seatmap' for implementation example
						'title'            => $event->post_title,
						'name'             => apply_filters('fjellcommerce_ticket_holder_name', $order->billing_first_name . ' ' . $order->billing_last_name, $order_id, $product_id, $item['variation_id'], $i),
						'venue'            => wp_get_object_terms( $product_id, 'nff_program_venue', array('fields' => 'names') ),
						'check_link'       => home_url('/tickets/'. $order_key .'/check/'. $ticket_order),
						'date'             => $date,
						'ticket_type'      => implode(', ', $ticket_type),
						'ticket_type_slug' => implode(', ', $ticket_type_slugs),
						'order_item'       => $item,
						'order_id'         => $order_id,
						'order_ticket'     => $i,
						'is_active'        => ( !isset( $active[$ticket_order] ) || $active[$ticket_order] === 'yes' ? true : false ),
						'item_price'       => wc_get_product($item['variation_id'])->get_price()
					), $item, $ticket_order, $order );
					$ticket_order++;
				}
			}
		}

		if(!$tickets) {
			return false;
		}

		return array(
			'summary' => $summary,
			'tickets' => $tickets
		);
	}

	/**
	 * Handle the ticket request
	 * @return void
	 */
	public static function ticket_view() {
		global $wp;

		$request = explode( '/', $wp->request );

		if ( isset($request[0]) && $request[0] === 'ticket' ) {
			if ( isset( $request[1] ) && $request[1] === 'login' ) {

				$token = isset($request[2]) ? $request[2] : false;
				if($token !== md5('fjellFest'. date('Y'))) {
					?>
					<h1 style="color:red">Innlogging feilet.</h1>
					<p>Du har ikke tilgang til denne innlogginga.</p>
					<?php
					die;
				}
				$credentials = array(
					'user_login' => 'billettsjekk',
					'user_password' => 'iI)^)SElx!4tnhFl(x8d(T)g',
					'remember' => true
				);

				$user = wp_signon( $credentials, false );

				if ( is_wp_error( $user ) ) {
					?>
					<h1 style="color:red">Innlogging feilet.</h1>
					<p><?php echo $user->get_error_message(); ?></p>
					<?php
				} else {
					?>
					<h1 style="color:green">Innlogging fullført.</h1>
					<?php
				}
			} else {
				$id = $request[1];
				if(substr($id, 0, 6) === 'order_') {
					$id = str_replace('order_', '', $id);
				}
				if(substr($id, 0, 9) !== 'wc_order_') {
					$id = 'wc_order_'. $id;
				}
				$order_key = str_replace('wc_', '', $id);
				$order_id = wc_get_order_id_by_order_key( 'wc_'. $order_key );
				$order = wc_get_order($order_id);
				if($order_id) {
					if ( isset( $request[2] ) && $request[2] === 'check' && isset($request[3]) ) {
						$ticket_id = $request[3];
						if ( current_user_can( 'check_tickets' ) ) {
							$tickets = static::create_tickets($order_key);
							if($tickets && isset($tickets['tickets'][$ticket_id])) {
								$ticket = $tickets;
							}
							// set the ticket as checked
							die;
						} else {
							wp_die( 'Du har ikke tilgang. <p><a href="'.wp_login_url().'">Logg inn her</a></p>', 'Ingen tilgang' );
						}
					} else {
						$ticket = static::create_pdf($order_key);
						if($ticket) {
							$ticket->render();
							// var_dump($tickets);
							die;
						}
					}
				}
			}
		}
	}

}

FC_Tickets::init();
