<?php

class FC_Duplicate {

	protected $festival;
	protected $year;
	protected $new_year;
	protected $before;
	protected $after = array();

	public function __construct($festival, $year, $new_year) {
		$this->festival = $festival;
		$this->year     = $year;
		$this->new_year = $new_year;
		$this->process();
	}

	public function before() {
		return count($this->before);
	}

	public function batch($start_from) {
		$events = $this->prepare();
		$events = array_slice($events, $start_from, 10);
		foreach($events as $event) {
			$post = wp_insert_post($event);
			if($post) {
				$this->after[] = $post;
			}
		}
		return count($this->after) > 0;
	}

	protected function process() {
		$this->before = $this->get_events();
	}

	public function run() {
		$events = $this->prepare();
		foreach($events as $event) {
			$post = wp_insert_post($event);
			if($post) {
				$this->after[] = $post;
			}
		}

		return count($this->after) > 0;
	}

	protected function prepare() {
		return array_map(function($event) {
			unset($event->ID);
			unset($event->post_name);
			unset($event->post_mime_type);
			unset($event->comment_count);
			unset($event->post_date);
			unset($event->post_date_gmt);
			unset($event->post_modified);
			unset($event->post_modified_gmt);
			unset($event->filter);
			unset($event->guid);
			unset($event->meta_input['event_products']);
			$event->post_status = 'draft';
			$event->post_author = get_current_user_id();
			return $event;
		}, $this->before);
	}

	protected function get_events() {
		$events = get_posts(array(
			'post_type' => 'nff_program',
			'posts_per_page' => -1,
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'nff_program_festival',
					'field' => 'slug',
					'terms' => $this->festival,
				),
				array(
					'taxonomy' => 'nff_program_year',
					'field' => 'slug',
					'terms' => $this->year,
				)
			)
		));

		$events = array_map(array($this, 'add_meta_and_taxonomies'), $events);

		return $events;
	}

	public function add_meta_and_taxonomies($event) {
		$event->meta_input = array_map(function($meta){
			return $meta[0];
		}, get_post_meta($event->ID));

		$festival = get_term_by( 'slug', $this->festival, 'nff_program_festival' );

		$new_year_term = term_exists( $this->new_year, 'nff_program_year' );
		if ( !$new_year_term ) {
		    $new_year_term = wp_insert_term( $this->new_year, 'nff_program_year', array('parent' => 0) );
		}
		$event->tax_input = array(
			'nff_program_venue' => array(),
			'nff_program_category' => array(),
			'nff_program_activity' => array(),
			'nff_program_tags' => array(),
			'nff_program_festival' => array( $festival->term_taxonomy_id ),
			'nff_program_year' => array( $new_year_term['term_taxonomy_id'] ),
		);

		$terms = wp_get_object_terms( $event->ID, array('nff_program_venue', 'nff_program_category', 'nff_program_activity', 'nff_program_tags') );

		foreach($terms as $term) {
			$event->tax_input[$term->taxonomy][] = $term->term_taxonomy_id;
		}

		return $event;
	}

}
