<?php
/**
 * Ticket
 *
 * @class     FC_PDF_Participant
 * @version   2.5.0
 * @package   WooCommerce/Classes/Products
 * @category  Class
 * @author    WooThemes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * FC_Tickets Class.
 */
class FC_Excel_Participant {

	/**
	 * All tickets for the order
	 * @var array
	 */
	protected $events;

	/**
	 * All tickets for the order
	 * @var array
	 */
	protected $product;

	/**
	 * Order summary
	 * @var array
	 */
	protected $summary;

	/**
	 * TCPDF Instance
	 * @var TCPDF
	 */
	protected $xls;

	/**
	 * Hooks up everything
	 * @param array $tickets
	 * @param array $summary
	 */
	public function __construct( $events, $product_id = null ) {

		// if tcpdf does not exist, lets include it
		if ( !class_exists( 'TCPDF' ) ) {
			include_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/vendor/php-export-data/php-export-data.php';
		}
		if($product_id)
			$this->product = $this->get_product($product_id);

		// add properties
		$this->events = $events;
		$this->xls    = new ExportDataExcel( 'browser', 'event_particpants.xls' );
	}

	protected function get_product($product_id) {
		$product = wc_get_product($product_id);
		$attributes = $product->get_formatted_variation_attributes(true);
		return $attributes;
	}

	/**
	 * Render the PDF (output to browser)
	 * @return void
	 */
	public function render() {
		$this->build();
		$this->xls->finalize();
		exit();
	}

	/**
	 * Build the document
	 * @return void
	 */
	protected function build() {
		$this->xls->initialize();
		$this->participants();
	}

	/**
	 * Create the order summary page HTML
	 * @return string
	 */
	protected function participants() {

		$this->xls->addRow(array(
			'Navn', 'Telefon', 'Epost', 'Kommentar'
		));

		foreach($this->events as $event) {

			foreach($event['participants'] as $user) {
				$this->xls->addRow(array(
					$user['name'], $user['phone'], $user['email'], $user['comment']
				));
			}

		}
	}


}
