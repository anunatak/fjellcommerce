<?php
/**
 * Tickets
 *
 * @class     FC_Tickets
 * @version   2.5.0
 * @package   WooCommerce/Classes/Products
 * @category  Class
 * @author    WooThemes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

include_once 'pdf/class-fc-pdf-revenue.php';
include_once 'pdf/class-fc-pdf-event.php';

/**
 * FC_Tickets Class.
 */
class FC_Revenue {

	/**
	 * Hook in methods.
	 */
	public static function init() {
		add_action( 'parse_request', array( __CLASS__, 'view' ) );
		add_action( 'admin_menu', array(__CLASS__, 'admin_menu') );
	}

	/**
	 * Add a new menu page to get transfer reports
	 * @return void
	 */
	public static function admin_menu() {
		add_submenu_page( 'edit.php?post_type=nff_program', 'Overføringer til konto', 'Overføringer', 'edit_posts', 'payment_transfers', array(__CLASS__, 'render_transfers_page') );
	}

	/**
	 * Renders the transfers page
	 * @return void
	 */
	public static function render_transfers_page() {
		?>
		<div class="wrap">
			<h2>Overføringer til konto fra Stripe</h2>
			<p>Hent ut mer detaljere overføringer fra Stripe</p>
			<?php if($transfers = self::get_transfers()) : ?>
			<select onchange="window.location = this.value">
			<option>Velg overføring</option>
			<?php foreach($transfers as $transfer) : ?>
				<option value="<?php echo home_url('report/transfer/'. $transfer->id) ?>">
					<?php echo date_i18n('d.m.Y H:i', $transfer->date) ?> (kr <?php echo number_format($transfer->amount / 100, 2, ',', '.') ?>)
				</option>
			<?php endforeach ?>
			</select>
			<?php else : ?>
				<p>Klarte ikke å etablere tilkobling til Stripe. Prøv igjen senere.</p>
			<?php endif ?>
		</div>
		<?php
	}

	/**
	 * Gets transfers
	 * @return array
	 */
	public static function get_transfers() {
		$transfers = FC_Stripe_API::fetch_transfers();
		return $transfers->data;
	}

	/**
	 * Creates a new PDF instance
	 * @param  string $id WooCommerce Order Key
	 * @return FC_PDF_Event
	 */
	public static function create_transfer_pdf($transfer_id) {
		$report = new FC_Stripe_Report($transfer_id);
		return new FC_PDF_Revenue($report);
	}

	/**
	 * Creates a new PDF instance
	 * @param  string $id WooCommerce Order Key
	 * @return FC_PDF_Event
	 */
	public static function create_event_pdf($data) {
		return new FC_PDF_Event($data);
	}

	/**
	 * Handle the transfer request
	 * @return void
	 */
	public static function view() {
		if(!is_user_logged_in() || !current_user_can( 'view_woocommerce_reports' )) {
			return;
		}
		global $wp;
		$request = explode( '/', $wp->request );
		if ( isset($request[0]) && $request[0] === 'report' ) {
			if ( isset( $request[1] ) && $request[1] === 'transfer' ) {
				$transfer_id = $request[2];
				$pdf = static::create_transfer_pdf($transfer_id);
				$pdf->render();
				die;
			}
			if ( isset( $request[1] ) && $request[1] === 'transfer_legacy' ) {
				$transfer_id = $request[2];
				$report = new FC_Stripe_Report_Legacy($transfer_id);
				$items = $report->get_items();
				foreach($items['connected']['refunds'] as $refund) {
					$tickets = $report->get_refunded_tickets($refund['wp']->ID);
					var_dump($tickets);
				}
				// var_dump($items);
				die;
			}
			if ( isset( $request[1] ) && $request[1] === 'revenue' ) {
				$post_id = $request[2];
				$post = get_post($post_id);
				if($post->post_type === 'nff_program') {
					$events = $post->event_products;
					$products = array();
					if($events) {
						foreach($events as $event) {
							$products[] = wc_get_product($event);
						}
					}
				} elseif($post->post_type === 'product') {
					$products[] = wc_get_product($post);
				}

				$revenues = array();

				$gateways = array(
					'stripe' => 'Kredittkort',
					'fjellcommerce-office' => 'Kasse'
				);

				foreach($products as $product) {
					$revenue = array(
						'ticket_types' => array(),
						'payment_methods' => array(),
						'sales' => array(),
						'discounts' => 0,
						'fees' => 0,
						'total' => 0,
						'sold' => 0,
						'product' => $product,
						'date' => get_post_meta($product->id, 'event_date', true)
					);
					$variations = $product->get_available_variations();
					foreach($variations as $variation) {
						$product_variation = wc_get_product($variation['variation_id']);
						$term = get_term_by( 'slug', $variation['attributes']['attribute_pa_ticket_type'], 'pa_ticket_type' );
						$revenue['ticket_types'][] = array(
							'name' => $term->name,
							'sold' => FC_WooCommerce_Api::get_sold_variation_items($variation['variation_id'])
						);

						$sales = FC_WooCommerce_Api::get_sold_variation_total($variation['variation_id']);
						$revenue['sales'][] = array(
							'name' => $term->name,
							'sold' => $sales['subtotal']
						);
						$revenue['discounts'] += $sales['discount'];
						$revenue['total'] += $sales['total'];
						$revenue['fees'] += $sales['fee'];
						$revenue['sold'] += FC_WooCommerce_Api::get_sold_variation_items($variation['variation_id']);
					}

					foreach($gateways as $id => $name) {
						$revenue['payment_methods'][] = array(
							'name' => $name,
							'sold' => FC_WooCommerce_Api::get_sold_payment_method_items($product->id, $id)
						);
					}

					$revenues[] = $revenue;
				}

				$pdf = static::create_event_pdf($revenues);
				$pdf->render();



				// $pdf = static::create_pdf($transfer_id);
				// $pdf->render();
				die;
			}
		}
	}

}

FC_Revenue::init();
