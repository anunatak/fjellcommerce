<?php
/**
 * Post Types
 *
 * Registers post types and taxonomies.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
class FC_Post_types {

	/**
	 * Hook in methods.
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'register_taxonomies' ), 5 );
		add_action( 'init', array( __CLASS__, 'register_post_types' ), 5 );
		add_action( 'init', array( __CLASS__, 'register_custom_fields' ), 5 );

		// Columns
		add_filter('manage_nff_program_posts_columns', array(__CLASS__, 'manage_columns'));
		add_filter('manage_nff_program_posts_custom_column', array(__CLASS__, 'populate_columns'), 20, 2);

		add_action('restrict_manage_posts', array(__CLASS__, 'restrict_events'));

		add_action( 'admin_footer', array(__CLASS__, 'column_scripts'));

		add_shortcode( 'nff_program', array(__CLASS__, 'shortcode') );

		add_action( 'template_include', array(__CLASS__, 'force_festival_to_load_from_theme') );
	}

	public static function force_festival_to_load_from_theme($template) {
		if(is_tax( 'nff_program_festival' )) {
			$template = get_template_directory() . '/taxonomy-nff_program_festival.php';
		}
		return $template;
	}

	/**
	 * Adds dropdown to restrict events to a set of filters
	 * @param  string $post_type
	 * @return void
	 */
	public static function restrict_events($post_type) {
		global $wp_query;
		if($post_type === 'nff_program') {

	        wp_dropdown_categories(array(
				'show_option_all' =>  __("Vis alle kategorier"),
				'taxonomy'        =>  'nff_program_category',
				'name'            =>  'nff_program_category',
				'orderby'         =>  'name',
				'selected'        =>  isset($wp_query->query['nff_program_category']) ? $wp_query->query['nff_program_category'] : 0,
				'value_field'     => 'slug',
				'hierarchical'    =>  true,
				'depth'           =>  3,
				'show_count'      =>  false, // Show # listings in parens
				'hide_empty'      =>  true, // Don't show businesses w/o listings
	        ));

	        wp_dropdown_categories(array(
				'show_option_all' =>  __("Vis alle aktiviteter"),
				'taxonomy'        =>  'nff_program_activity',
				'name'            =>  'nff_program_activity',
				'orderby'         =>  'name',
				'selected'        =>  isset($wp_query->query['nff_program_activity']) ? $wp_query->query['nff_program_activity'] : 0,
				'value_field'     => 'slug',
				'hierarchical'    =>  true,
				'depth'           =>  3,
				'show_count'      =>  false, // Show # listings in parens
				'hide_empty'      =>  true, // Don't show businesses w/o listings
	        ));

	        wp_dropdown_categories(array(
				'show_option_all' =>  __("Vis alle år"),
				'taxonomy'        =>  'nff_program_year',
				'name'            =>  'nff_program_year',
				'orderby'         =>  'name',
				'selected'        =>  isset($wp_query->query['nff_program_year']) ? $wp_query->query['nff_program_year'] : 0,
				'value_field'     => 'slug',
				'hierarchical'    =>  true,
				'depth'           =>  3,
				'show_count'      =>  false, // Show # listings in parens
				'hide_empty'      =>  true, // Don't show businesses w/o listings
	        ));

		}
	}

	/**
	 * Creates a shortcode so the program can be implemented on pages
	 * @param  array  $atts
	 * @return void
	 */
	public static function shortcode( $atts = array() ) {
		$atts = shortcode_atts( array(
	        'festival' => FjellCommerce()->get_active_festival()->slug,
	        'year' => FjellCommerce()->get_active_year(),
	    ), $atts );

	    return '<container festival="'.$atts['festival'].'" year="'.$atts['year'].'"></container>';
	}

	/**
	 * Adds scripts to the list table screen
	 * @param  string $hook
	 * @return void
	 */
	public static function column_scripts($hook)
	{
		$screen = get_current_screen();
		if($screen->id === 'edit-nff_program') { ?>
		<script>
			jQuery(document).ready(function($) {
				$('.nff-lists').each(function() {
					var $trigger = $(this).find('.list-trigger');
					var $list = $(this).find('.list-list');

					$trigger.on('click', function(e) {
						e.preventDefault();
						$('.list-list').not($list).slideUp();
						$list.slideToggle();
					});
				});
			})
		</script>
		<?php }
	}

	/**
	 * Populates the custom columns
	 * @param  string $column
	 * @param  integer $post_id
	 * @return void
	 */
	public static function populate_columns($column, $post_id)
	{
		if($column === 'tickets') {
			$products = get_post_meta( $post_id, 'event_products', true );
			$events = get_field('nff_program_events', $post_id);
			if($products && is_array($products)) {
				foreach($products as $event_id => $product_id) {
					$event = $events[$event_id];
					$product = wc_get_product($product_id);
					$sold = get_post_meta( $product->id, 'total_sales', true );
					echo '<p><strong>'. $event['nff_program_date'] .'</strong><br>';
					echo ($sold ? $sold : '0') . ' solgt<br>';
					echo $product->get_stock_quantity() . ' ledig</p>';
				}
			}
		}

		if($column === 'lists') {
			if(get_field('requires_information', $post_id)) {
				$products = get_post_meta( $post_id, 'event_products', true );
				$events = get_field('nff_program_events', $post_id);
				if($products) {
					?>
					<div class="nff-lists" style="position: relative;display:inline-block;margin: 0 10px 10px 0">
					<a class="list-trigger button-primary" href="#">Deltakerliste</a>
					<ul class="list-list" style="
						position: absolute;
						background-color:  #fff;
						top: 35px;
						margin: 0;
						padding: 10px;
						width: 150px;
						left: 50%;
						margin-left: -85px;
						z-index: 900;
						display: none;
					">
					<li><strong>Arrangement (PDF)</strong></li>
					<li><a target="_blank" href="<?php echo home_url('/participants/'. $post_id .'/all/pdf/') ?>">Alle arrangement</a></li>
					<?php
					foreach($products as $event_id => $product_id) {
						$event = $events[$event_id];
						$product = wc_get_product($product_id);
						$sold = get_post_meta( $product->id, 'total_sales', true );
						?>
						<li><a target="_blank" href="<?php echo home_url('/participants/'. $post_id .'/'. $product_id .'/pdf/') ?>"><?php echo $event['nff_program_date'] ?></a></li>

					<?php } ?>
					<li><strong>Billett typer (PDF)</strong></li>
					<?php $tickets = nff_get_tickets($post_id) ?>
					<?php foreach($tickets as $ticket) : ?>
					<li>
						<a target="_blank" href="<?php echo home_url('/participants/'. $post_id .'/ticket-'.$ticket['id'].'/pdf/') ?>"><?php echo $ticket['name'] ?></a>
					</li>
					<?php endforeach ?>
					<li><strong>Arrangement (Excel)</strong></li>
					<li><a target="_blank" href="<?php echo home_url('/participants/'. $post_id .'/all/excel/') ?>">Alle arrangement</a></li>
					<?php
					foreach($products as $event_id => $product_id) {
						$event = $events[$event_id];
						$product = wc_get_product($product_id);
						$sold = get_post_meta( $product->id, 'total_sales', true );
						?>
						<li><a target="_blank" href="<?php echo home_url('/participants/'. $post_id .'/'. $product_id .'/excel/') ?>"><?php echo $event['nff_program_date'] ?></a></li>

					<?php } ?>
					<li><strong>Billett typer (Excel)</strong></li>
					<?php $tickets = nff_get_tickets($post_id) ?>
					<?php foreach($tickets as $ticket) : ?>
					<li>
						<a target="_blank" href="<?php echo home_url('/participants/'. $post_id .'/ticket-'.$ticket['id'].'/excel/') ?>"><?php echo $ticket['name'] ?></a>
					</li>
					<?php endforeach ?>
					</ul>
					</div>
					<?php
				}
			}
			$products = get_post_meta( $post_id, 'event_products', true );
			$events = get_field('nff_program_events', $post_id);
			if($products) {
				?>
				<div class="nff-lists" style="position: relative;display:inline-block;margin: 0 10px 10px 0">
				<a class="list-trigger button-primary" href="#">Rapport</a>
				<ul class="list-list" style="
					position: absolute;
					background-color:  #fff;
					top: 35px;
					margin: 0;
					padding: 10px;
					width: 150px;
					left: 50%;
					margin-left: -85px;
					z-index: 900;
					display: none;
				">
				<li><a target="_blank" href="<?php echo home_url('/report/revenue/'. $post_id) ?>">Alle arrangement</a></li>
				<?php
				foreach($products as $event_id => $product_id) {
					$event = $events[$event_id];
					$product = wc_get_product($product_id);
					$sold = get_post_meta( $product->id, 'total_sales', true );
					?>
					<li><a target="_blank" href="<?php echo home_url('/report/revenue/'. $product_id) ?>"><?php echo $event['nff_program_date'] ?></a></li>
				<?php } ?>
				</ul>
				</div>
				<?php
			}
		}
	}

	/**
	 * Add columns
	 * @param  array $columns
	 * @return array
	 */
	public static function manage_columns($columns)
	{
		unset($columns['date']);
		$columns['tickets'] = 'Billetter';
		$columns['lists'] = 'Lister';
		return $columns;
	}

	/**
	 * Register core taxonomies.
	 */
	public static function register_taxonomies() {

		register_taxonomy( 'nff_program_venue', array( 'nff_program', 'product' ), array(
			'labels'                     => array(
				'name'                       => __( 'Steder', 'fjellcommerce' ),
				'singular_name'              => __( 'Sted', 'fjellcommerce' ),
				'menu_name'                  => __( 'Steder', 'fjellcommerce' ),
				'all_items'                  => __( 'Alle steder', 'fjellcommerce' ),
				'parent_item'                => __( 'Foreldrested', 'fjellcommerce' ),
				'parent_item_colon'          => __( 'Foreldrested:', 'fjellcommerce' ),
				'new_item_name'              => __( 'Navn på nytt sted', 'fjellcommerce' ),
				'add_new_item'               => __( 'Legg til nytt sted', 'fjellcommerce' ),
				'edit_item'                  => __( 'Endre sted', 'fjellcommerce' ),
				'update_item'                => __( 'Oppdater sted', 'fjellcommerce' ),
				'view_item'                  => __( 'Vis sted', 'fjellcommerce' ),
				'separate_items_with_commas' => __( 'Skill steder med komma', 'fjellcommerce' ),
				'add_or_remove_items'        => __( 'Legg til eller fjern steder', 'fjellcommerce' ),
				'choose_from_most_used'      => __( 'Velg fra mest brukte steder', 'fjellcommerce' ),
				'popular_items'              => __( 'Populære steder', 'fjellcommerce' ),
				'search_items'               => __( 'Søk etter steder', 'fjellcommerce' ),
				'not_found'                  => __( 'Sted ikke funnet', 'fjellcommerce' ),
			),
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => false,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
		) );

		register_taxonomy( 'nff_program_category', array( 'nff_program', 'product' ), array(
			'labels'                     => array(
				'name'                       => __( 'Kategorier', 'fjellcommerce' ),
				'singular_name'              => __( 'Kategori', 'fjellcommerce' ),
				'menu_name'                  => __( 'Kategorier', 'fjellcommerce' ),
				'all_items'                  => __( 'Alle kategorier', 'fjellcommerce' ),
				'parent_item'                => __( 'Foreldrekategori', 'fjellcommerce' ),
				'parent_item_colon'          => __( 'Foreldrekategori:', 'fjellcommerce' ),
				'new_item_name'              => __( 'Navn på ny kategori', 'fjellcommerce' ),
				'add_new_item'               => __( 'Legg til ny kategori', 'fjellcommerce' ),
				'edit_item'                  => __( 'Endre kategori', 'fjellcommerce' ),
				'update_item'                => __( 'Oppdater kategori', 'fjellcommerce' ),
				'view_item'                  => __( 'Vis kategori', 'fjellcommerce' ),
				'separate_items_with_commas' => __( 'Skill kategorier med komma', 'fjellcommerce' ),
				'add_or_remove_items'        => __( 'Legg til eller fjern kategorier', 'fjellcommerce' ),
				'choose_from_most_used'      => __( 'Velg fra mest brukte kategorier', 'fjellcommerce' ),
				'popular_items'              => __( 'Populære kategorier', 'fjellcommerce' ),
				'search_items'               => __( 'Søk etter kategorier', 'fjellcommerce' ),
				'not_found'                  => __( 'Kategori ikke funnet', 'fjellcommerce' ),
			),
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
		) );

		register_taxonomy( 'nff_program_activity', array( 'nff_program', 'product' ), array(
			'labels'                     => array(
				'name'                       => __( 'Aktiviteter', 'fjellcommerce' ),
				'singular_name'              => __( 'Aktivitet', 'fjellcommerce' ),
				'menu_name'                  => __( 'Aktiviteter', 'fjellcommerce' ),
				'all_items'                  => __( 'Alle aktiviteter', 'fjellcommerce' ),
				'parent_item'                => __( 'Foreldreaktivitet', 'fjellcommerce' ),
				'parent_item_colon'          => __( 'Foreldreaktivitet:', 'fjellcommerce' ),
				'new_item_name'              => __( 'Navn på ny aktivitet', 'fjellcommerce' ),
				'add_new_item'               => __( 'Legg til ny aktivitet', 'fjellcommerce' ),
				'edit_item'                  => __( 'Endre aktivitet', 'fjellcommerce' ),
				'update_item'                => __( 'Oppdater aktivitet', 'fjellcommerce' ),
				'view_item'                  => __( 'Vis aktivitet', 'fjellcommerce' ),
				'separate_items_with_commas' => __( 'Skill aktiviteter med komma', 'fjellcommerce' ),
				'add_or_remove_items'        => __( 'Legg til eller fjern aktiviteter', 'fjellcommerce' ),
				'choose_from_most_used'      => __( 'Velg fra mest brukte aktiviteter', 'fjellcommerce' ),
				'popular_items'              => __( 'Populære aktiviteter', 'fjellcommerce' ),
				'search_items'               => __( 'Søk etter aktiviteter', 'fjellcommerce' ),
				'not_found'                  => __( 'Aktivitet ikke funnet', 'fjellcommerce' ),
			),
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
		) );

		register_taxonomy( 'nff_program_tags', array( 'nff_program', 'product' ), array(
			'labels'                     => array(
				'name'                       => __( 'Tagger', 'fjellcommerce' ),
				'singular_name'              => __( 'Tagg', 'fjellcommerce' ),
				'menu_name'                  => __( 'Tagger', 'fjellcommerce' ),
				'all_items'                  => __( 'Alle tagger', 'fjellcommerce' ),
				'parent_item'                => __( 'Foreldretagg', 'fjellcommerce' ),
				'parent_item_colon'          => __( 'Foreldretagg:', 'fjellcommerce' ),
				'new_item_name'              => __( 'Navn på ny tagg', 'fjellcommerce' ),
				'add_new_item'               => __( 'Legg til ny tagg', 'fjellcommerce' ),
				'edit_item'                  => __( 'Endre tagg', 'fjellcommerce' ),
				'update_item'                => __( 'Oppdater tagg', 'fjellcommerce' ),
				'view_item'                  => __( 'Vis tagg', 'fjellcommerce' ),
				'separate_items_with_commas' => __( 'Skill tagger med komma', 'fjellcommerce' ),
				'add_or_remove_items'        => __( 'Legg til eller fjern tagger', 'fjellcommerce' ),
				'choose_from_most_used'      => __( 'Velg fra mest brukte tagger', 'fjellcommerce' ),
				'popular_items'              => __( 'Populære tagger', 'fjellcommerce' ),
				'search_items'               => __( 'Søk etter tagger', 'fjellcommerce' ),
				'not_found'                  => __( 'Tagg ikke funnet', 'fjellcommerce' ),
			),
			'hierarchical'               => false,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => false,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
		) );

		register_taxonomy( 'nff_program_festival', array( 'nff_program', 'product' ), array(
			'labels'                     => array(
				'name'                       => __( 'Festivaler', 'fjellcommerce' ),
				'singular_name'              => __( 'Festival', 'fjellcommerce' ),
				'menu_name'                  => __( 'Festivaler', 'fjellcommerce' ),
				'all_items'                  => __( 'Alle festivaler', 'fjellcommerce' ),
				'parent_item'                => __( 'Foreldrefestival', 'fjellcommerce' ),
				'parent_item_colon'          => __( 'Foreldrefestival:', 'fjellcommerce' ),
				'new_item_name'              => __( 'Navn på ny festival', 'fjellcommerce' ),
				'add_new_item'               => __( 'Legg til ny festival', 'fjellcommerce' ),
				'edit_item'                  => __( 'Endre festival', 'fjellcommerce' ),
				'update_item'                => __( 'Oppdater festival', 'fjellcommerce' ),
				'view_item'                  => __( 'Vis festival', 'fjellcommerce' ),
				'separate_items_with_commas' => __( 'Skill festivaler med komma', 'fjellcommerce' ),
				'add_or_remove_items'        => __( 'Legg til eller fjern festivaler', 'fjellcommerce' ),
				'choose_from_most_used'      => __( 'Velg fra mest brukte festivaler', 'fjellcommerce' ),
				'popular_items'              => __( 'Populære festivaler', 'fjellcommerce' ),
				'search_items'               => __( 'Søk etter festivaler', 'fjellcommerce' ),
				'not_found'                  => __( 'Festival ikke funnet', 'fjellcommerce' ),
			),
			'hierarchical'      => true,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'rewrite'           => array('slug' => 'nyheter', 'with_front' => false),
			'show_tagcloud'     => false,
		) );

		register_taxonomy( 'nff_program_year', array( 'nff_program', 'product' ), array(
			'labels'                     => array(
				'name'                       => __( 'År', 'fjellcommerce' ),
				'singular_name'              => __( 'År', 'fjellcommerce' ),
				'menu_name'                  => __( 'År', 'fjellcommerce' ),
				'all_items'                  => __( 'Alle år', 'fjellcommerce' ),
				'parent_item'                => __( 'Foreldreår', 'fjellcommerce' ),
				'parent_item_colon'          => __( 'Foreldreår:', 'fjellcommerce' ),
				'new_item_name'              => __( 'Navn på nytt år', 'fjellcommerce' ),
				'add_new_item'               => __( 'Legg til nytt år', 'fjellcommerce' ),
				'edit_item'                  => __( 'Endre år', 'fjellcommerce' ),
				'update_item'                => __( 'Oppdater år', 'fjellcommerce' ),
				'view_item'                  => __( 'Vis år', 'fjellcommerce' ),
				'separate_items_with_commas' => __( 'Skill år med komma', 'fjellcommerce' ),
				'add_or_remove_items'        => __( 'Legg til eller fjern år', 'fjellcommerce' ),
				'choose_from_most_used'      => __( 'Velg fra mest brukte år', 'fjellcommerce' ),
				'popular_items'              => __( 'Populære år', 'fjellcommerce' ),
				'search_items'               => __( 'Søk etter år', 'fjellcommerce' ),
				'not_found'                  => __( 'År ikke funnet', 'fjellcommerce' ),
			),
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
		) );
	}

	/**
	 * Register core post types.
	 */
	public static function register_post_types() {

		register_post_type( 'nff_program', array(
			'label'               => __( 'Arrangementer', 'fjellcommerce' ),
			'description'         => __( 'Beskrivelse', 'fjellcommerce' ),
			'labels'              => array(
				'name'                => __( 'Arrangementer', 'fjellcommerce' ),
				'singular_name'       => __( 'Arrangement', 'fjellcommerce' ),
				'menu_name'           => __( 'Arrangementer', 'fjellcommerce' ),
				'name_admin_bar'      => __( 'Arrangementer', 'fjellcommerce' ),
				'parent_item_colon'   => __( 'Foreldrearrangement:', 'fjellcommerce' ),
				'all_items'           => __( 'Alle arrangementer', 'fjellcommerce' ),
				'add_new_item'        => __( 'Legg til nytt arrangement', 'fjellcommerce' ),
				'add_new'             => __( 'Legg til nytt', 'fjellcommerce' ),
				'new_item'            => __( 'Nytt arrangement', 'fjellcommerce' ),
				'edit_item'           => __( 'Endre arrangement', 'fjellcommerce' ),
				'update_item'         => __( 'Oppdater arrangement', 'fjellcommerce' ),
				'view_item'           => __( 'Vis arrangement', 'fjellcommerce' ),
				'search_items'        => __( 'Søk etter arrangement', 'fjellcommerce' ),
				'not_found'           => __( 'Ingen arrangementer funnet', 'fjellcommerce' ),
				'not_found_in_trash'  => __( 'Ingen arrangementer funnet i papirkurven', 'fjellcommerce' ),
			),
			'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'taxonomies'          => array(  ),
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => false,
			'can_export'          => true,
			'has_archive'         => false,
			'rewrite'             => array( 'slug' => 'arrangement' ),
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
		) );
	}

	/**
	 * Register core custom fields.
	 */
	public static function register_custom_fields() {

		acf_add_local_field_group(array (
			'key' => 'group_nff_program',
			'title' => 'Arrangement',
			'fields' => array (),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'nff_program',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));

		do_action('nff_program_metabox_before', 'group_nff_program');

		acf_add_local_field(array (
			'key' => 'field_nff_program_1',
			'label' => 'Informasjon',
			'name' => 'informasjon',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
			'parent' => 'group_nff_program'
		));

		acf_add_local_field(array (
			'key' => 'field_nff_program_10',
			'label' => 'Krever deltakerinformasjon',
			'name' => 'requires_information',
			'type' => 'true_false',
			'instructions' => '',
			'default' => false,
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'endpoint' => 0,
			'parent' => 'group_nff_program'
		));

		// acf_add_local_field(array (
		// 	'key' => 'field_nff_program_11',
		// 	'label' => 'Påmelding',
		// 	'name' => 'nff_program_signup',
		// 	'type' => 'true_false',
		// 	'instructions' => '',
		// 	'default' => false,
		// 	'required' => 0,
		// 	'conditional_logic' => 0,
		// 	'wrapper' => array (
		// 		'width' => '',
		// 		'class' => '',
		// 		'id' => '',
		// 	),
		// 	'endpoint' => 0,
		// 	'parent' => 'group_nff_program'
		// ));

		do_action('nff_program_metabox_before_information', 'group_nff_program');

		acf_add_local_field(array (
			'key' => 'field_nff_program_8',
			'label' => 'Alder fra',
			'name' => 'nff_program_ages_from',
			'type' => 'number',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '50%',
				'class' => '',
				'id' => '',
			),
			'default_value' => '0',
			'placeholder' => '',
			'prepend' => '',
			'append' => 'år',
			'min' => 0,
			'max' => 99,
			'step' => '',
			'readonly' => 0,
			'disabled' => 0,
			'parent' => 'group_nff_program'
		));
		acf_add_local_field(array (
			'key' => 'field_nff_program_9',
			'label' => 'Alder til',
			'name' => 'nff_program_ages_to',
			'type' => 'number',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '50%',
				'class' => '',
				'id' => '',
			),
			'default_value' => '99',
			'placeholder' => '',
			'prepend' => '',
			'append' => 'år',
			'min' => 0,
			'max' => 99,
			'step' => 1,
			'readonly' => 0,
			'disabled' => 0,
			'parent' => 'group_nff_program'
		));

		acf_add_local_field(array (
			'key' => 'field_nff_program_14',
			'label' => 'Varighet',
			'name' => 'nff_program_length',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => 0,
			'max' => 99,
			'step' => '',
			'readonly' => 0,
			'disabled' => 0,
			'parent' => 'group_nff_program'
		));

		do_action('nff_program_metabox_after_information', 'group_nff_program');

		acf_add_local_field(array (
			'key' => 'field_nff_program_4',
			'label' => 'Billetter',
			'name' => 'billetter',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
			'parent' => 'group_nff_program'
		));

		do_action('nff_program_metabox_before_tickets', 'group_nff_program');

		acf_add_local_field(array (
			'key' => 'field_nff_program_5',
			'label' => 'Bilett type',
			'name' => 'nff_program_tickets_ticket_type',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => apply_filters('nff_program_event_ticket_types', array(
				'standard' => __( 'Standard', 'fjellcommerce' )
			)),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
			'parent' => 'group_nff_program'
		));

		acf_add_local_field(array (
			'key' => 'field_nff_program_X872',
			'label' => 'Kasseregel',
			'name' => 'checkout_rule',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'none' => 'Ingen',
				'crs' => 'Camp Romsdal Sommer',
				'yoga' => 'Yoga'
			),
			'default_value' => array (
				'none'
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
			'parent' => 'group_nff_program'
		));

		do_action('nff_program_metabox_after_ticket_type', 'group_nff_program');

		acf_add_local_field(array (
			'key' => 'field_nff_program_7',
			'label' => 'Billetter',
			'name' => 'nff_program_tickets_tickets',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => 'field_nff_program_7_1',
			'min' => '',
			'max' => '',
			'layout' => 'table',
			'button_label' => 'Legg til rad',
			'parent' => 'group_nff_program',
			'sub_fields' => array (),
		));

		acf_add_local_field(array (
			'key' => 'field_nff_program_7_1',
			'label' => 'Navn på billett',
			'name' => 'name',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
			'parent' => 'field_nff_program_7'
		));

		acf_add_local_field(array (
			'key' => 'field_nff_program_7_2',
			'label' => 'Pris',
			'name' => 'price',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
			'parent' => 'field_nff_program_7'
		));

		acf_add_local_field(array (
			'key' => 'field_nff_program_7_2X231',
			'label' => 'Antall billetter',
			'name' => 'quantity',
			'type' => 'number',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
			'parent' => 'field_nff_program_7'
		));

		// acf_add_local_field(array (
		// 	'key' => 'field_nff_program_7_3',
		// 	'label' => 'Billett type',
		// 	'name' => 'ticket_type',
		// 	'type' => 'select',
		// 	'instructions' => '',
		// 	'required' => 0,
		// 	'conditional_logic' => 0,
		// 	'wrapper' => array (
		// 		'width' => '',
		// 		'class' => '',
		// 		'id' => '',
		// 	),
		// 	'choices' => wp_parse_args(array('parent' => __( 'Arrangmentets standard', 'fjellcommerce' ) ), apply_filters('nff_program_event_ticket_types', array(
		// 		'standard' => __( 'Standard', 'fjellcommerce' )
		// 	))),
		// 	'default_value' => array (
		// 		'parent'
		// 	),
		// 	'allow_null' => 0,
		// 	'multiple' => 0,
		// 	'ui' => 0,
		// 	'ajax' => 0,
		// 	'placeholder' => '',
		// 	'disabled' => 0,
		// 	'readonly' => 0,
		// 	'parent' => 'field_nff_program_7'
		// ));

		do_action('nff_program_metabox_before_tickets', 'group_nff_program');



		acf_add_local_field(array (
			'key' => 'field_nff_program_tab_3',
			'label' => 'Arrangementer',
			'name' => 'arrangementer',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
			'parent' => 'group_nff_program'
		));

		acf_add_local_field(array (
			'key' => 'field_nff_program_0',
			'label' => 'Arrangementer',
			'name' => 'nff_program_events',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => 'field_nff_program_7_1',
			'min' => '',
			'max' => '',
			'layout' => 'table',
			'button_label' => 'Legg til rad',
			'parent' => 'group_nff_program',
			'sub_fields' => array (),
		));

		acf_add_local_field(array (
			'key' => 'field_nff_program_2',
			'label' => 'Dato',
			'name' => 'nff_program_date',
			'type' => 'date_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'display_format' => 'd/m/Y',
			'return_format' => 'd.m.Y',
			'first_day' => 1,
			'parent' => 'field_nff_program_0'
		));

		acf_add_local_field(array (
			'key' => 'field_nff_program_3',
			'label' => 'Tidspunkt',
			'name' => 'nff_program_time',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '00:00',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
			'parent' => 'field_nff_program_0'
		));

		acf_add_local_field(array (
			'key' => 'field_nff_program_6',
			'label' => 'Antall billetter',
			'name' => 'nff_program_tickets_quantity',
			'type' => 'number',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 0,
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
			'readonly' => 0,
			'disabled' => 0,
			'parent' => 'field_nff_program_0'
		));

		acf_add_local_field(array (
			'key' => 'field_nff_program_9878',
			'label' => 'Salgstart',
			'name' => 'sales_start',
			'type' => 'date_picker',
			'instructions' => 'Fylles kun ut dersom det avviker fra festivalens salgsstart.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'display_format' => 'd/m/Y',
			'return_format' => 'd.m.Y',
			'first_day' => 1,
			'parent' => 'field_nff_program_0'
		));

		acf_add_local_field(array (
			'key' => 'field_nff_program_9878_X',
			'label' => 'Salgslutt',
			'name' => 'sales_stop',
			'type' => 'date_picker',
			'instructions' => 'Fylles kun ut dersom det avviker fra festivalens salgsstopp.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'display_format' => 'd/m/Y',
			'return_format' => 'd.m.Y',
			'first_day' => 1,
			'parent' => 'field_nff_program_0'
		));


		acf_add_local_field_group(array (
			'key' => 'group_573c286c7baeb',
			'title' => 'Sted',
			'fields' => array (
				array (
					'key' => 'field_573c287b13e12',
					'label' => 'Kartplassering',
					'name' => 'location',
					'type' => 'google_map',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'center_lat' => '',
					'center_lng' => '',
					'zoom' => '',
					'height' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'taxonomy',
						'operator' => '==',
						'value' => 'nff_program_venue',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));

		acf_add_local_field_group(array (
			'key' => 'group_573c27bfc85b2',
			'title' => 'Festival',
			'fields' => array (
				array (
					'key' => 'field_573c27c8fee77',
					'label' => 'Billettslag starter',
					'name' => 'sales_start',
					'type' => 'date_picker',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'display_format' => 'd/m/Y',
					'return_format' => 'd.m.Y',
					'first_day' => 1,
				),
				array (
					'key' => 'field_573c27c8fee77_2',
					'label' => 'Billettsalg slutter',
					'name' => 'sales_stop',
					'type' => 'date_picker',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'display_format' => 'd/m/Y',
					'return_format' => 'd.m.Y',
					'first_day' => 1,
				),
				array(
					'key' => 'field_573c27c8fee77_1',
					'label' => 'Tekstlinje for dato',
					'name' => 'date_text',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0
				),
				array(
					'key' => 'field_573c27c8fee77_2',
					'label' => 'Tekstlinje for sted',
					'name' => 'location_text',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0
				)
			),
			'location' => array (
				array (
					array (
						'param' => 'taxonomy',
						'operator' => '==',
						'value' => 'nff_program_festival',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));

	}

}

FC_Post_types::init();
