<?php
/**
 * Events Filter
 */
class FC_Events_Filter {

	/**
	 * Current active filters
	 * @var array
	 */
	protected $filters;

	/**
	 * The parsed query
	 * @var array
	 */
	protected $parsed = array(
		'meta_query' => array(),
		'tax_query' => array()
	);

	/**
	 * Creates the filters
	 * @param array $filters
	 */
	public function __construct($filters)
	{
		$this->filters = $filters;
	}

	/**
	 * Parses the filters
	 * @return array
	 */
	public function parse()
	{
		foreach($this->filters as $name => $filter) {
			if($filter['type'] === 'taxonomy') {
				$this->filter_taxonomy($name, $filter);
			} else if($filter['type'] === 'field') {
				$this->filter_field($name, $filter);
			} else if($filter['type'] === 'search') {
				$this->filter_search($name, $filter);
			}
		}
		return $this->parsed;
	}

	/**
	 * Parses a taxonomy filter
	 * @param  string $taxonomy
	 * @param  array $filter
	 * @return void
	 */
	protected function filter_taxonomy($taxonomy, $filter)
	{
		if($filter['items'][0]['value'] !== 'all') {
			$terms = array();
			foreach($filter['items'] as $item) {
				$terms[] = $item['value'];
			}
			$this->parsed['tax_query'][] = array(
				'taxonomy' => $taxonomy,
				'field' => 'slug',
				'terms' => $terms
			);
		}
	}

	/**
	 * Parses a custom field filter
	 * @param  string $name
	 * @param  array $filter
	 * @return void
	 * @todo  age filtering is missing
	 */
	protected function filter_field($name, $filter)
	{
		if($name === 'nff_program_ages') {
			// ages??
		} elseif($name === 'nff_program_date') {
			if($filter['items'][0]['value'] !== 'all') {
				$values = array();
				foreach($filter['items'] as $item) {
					$values[] = date_create_from_format('Ymd', $item['value'])->format('d.m.Y');
				}
				$this->parsed['meta_query'][] = array(
					'field' => 'event_date',
					'compare' => 'IN',
					'value' => $values
				);
			}
		}
	}

	/**
	 * Parses a search filter
	 * @param  string $search
	 * @param  array $filter
	 * @return void
	 */
	protected function filter_search($search, $filter)
	{
		$search = $filter['value'] && $filter['value'] !== 'false' ? $filter['value'] : '';
		if($search) {
			$this->parsed['s'] = $search;
		}
	}

}
