<?php
/**
 * A start for generating a report based on the old orders.
 *
 * Temporarily scratched..
 */
class FC_Stripe_Report_Legacy {

	/**
	 * Holds the items
	 * @var array
	 */
	protected $items = array();

	/**
	 * Holds the summary
	 * @var array
	 */
	protected $summary = array(
		'total'    => 0,
		'subtotal' => 0,
		'discount' => 0,
		'fee'      => 0,
		'refund'   => 0,
		'qty'      => 0
	);

	/**
	 * Holds the transfer
	 * @var mixed
	 */
	protected $transfer = null;

	/**
	 * Holds an array of all transactions
	 * @var array
	 */
	protected $transactions = array();

	/**
	 * The transfer id
	 * @var null
	 */
	protected $transfer_id = null;

	/**
	 * Fetchs the transactions and generate the report data
	 * @param string $transfer_id
	 */
	public function __construct( $transfer_id ) {
		$this->transfer_id = $transfer_id;
		$this->fetch_transfer();
		$this->fetch_transactions();
		$this->generate_report_data();
	}

	protected function fetch_transfer() {
		$result = FC_Stripe_API::fetch_transfer($this->transfer_id);
		$this->transfer = $result;
	}

	/**
	 * Connects to Stripe and fetches all transactions
	 * @return void
	 */
	protected function fetch_transactions() {
		$file = plugin_dir_path( FC_PLUGIN_FILE ) . 'data/transfers.csv';

		$transactions = array();
		if (($handle = fopen($file, "r")) !== FALSE) {
			$first = true;
			$headers = array();
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$num = count($data);
				if($first) {
					for ($c=0; $c < $num; $c++) {
						$headers[$c] = $data[$c];
					}
				} else {
					$transaction = array();
					for ($c=0; $c < $num; $c++) {
						$transaction[$headers[$c]] = $data[$c];
					}
					$transactions[] = $transaction;
				}
				$first = false;
			}
			fclose($handle);
		}
		$this->transactions = $transactions;
	}

	/**
	 * Generates the report data
	 * @return void
	 */
	protected function generate_report_data() {
		if($this->transactions) {
			$chargesDisconnected = array();
			$chargesConnected = array();
			$refundsDisconnected = array();
			$refundsConnected = array();
			foreach($this->transactions as $item) {
				if($item['Type'] === 'Charge') {
					$orders = get_posts(array(
						'post_type' => 'nff_program_ticket',
						'post_status' => 'any',
						'meta_value' => $item['ID'],
						'meta_compare' => '=',
						'meta_key' => '_stripe_charge',
						'posts_per_page' => 1
					));
					if($orders) {
						$chargesConnected[$item['ID']] = array(
							'charge' => $item,
							'wp' => $orders[0]
						);
					} else {
						$chargesDisconnected[$item['ID']] = array(
							'charge' => $item,
							'wp' => null
						);
					}
				} elseif($item['Type'] === 'Refund') {
					$orders = get_posts(array(
						'post_type' => 'nff_program_ticket',
						'post_status' => 'any',
						'meta_value' => $item['ID'],
						'meta_compare' => '=',
						'meta_key' => '_stripe_charge',
						'posts_per_page' => 1
					));
					if($orders) {
						$refundsConnected[$item['ID']] = array(
							'refund' => $item,
							'wp' => $orders[0]
						);
					} else {
						$refundsDisconnected[$item['ID']] = array(
							'refund' => $item,
							'wp' => null
						);
					}
				}
			}
			$this->items = array(
				'connected' => array(
					'refunds' => $refundsConnected,
					'charges' => $chargesConnected,
				),
				'disconnected' => array(
					'refunds' => $refundsDisconnected,
					'charges' => $chargesDisconnected,
				),
			);
		}
	}

	public function get_tickets($ticket_id) {
		$tickets = get_post_meta($ticket_id, 'tickets', true);
		$items = array();
		for ($i=0; $i < $tickets; $i++) {
			$item = array(
				'post_id' => get_post_meta($ticket_id, "tickets_{$i}_post_id", true),
				'event_id' => get_post_meta($ticket_id, "tickets_{$i}_event_id", true),
				'ticket_id' => get_post_meta($ticket_id, "tickets_{$i}_ticket_id", true),
			);

			$item['price'] = get_post_meta($item['post_id'], 'nff_program_tickets_tickets_'. $item['ticket_id'] .'_price', true);

			$items[] = $item;
		}

		return $items;
	}

	public function get_refunded_tickets($ticket_id) {
		$tickets = get_post_meta($ticket_id, '_refunded_tickets', true);
		$items = array();
		if(is_array($tickets) && count($tickets) > 0) {
			foreach($tickets as $ticket) {
				$item = array(
					'post_id' => $ticket['post_id'],
					'event_id' => $ticket['event_id'],
					'ticket_id' => $ticket['ticket_id'],
				);
				$item['price'] = get_post_meta($item['post_id'], 'nff_program_tickets_tickets_'. $item['ticket_id'] .'_price', true);
			}
		}

		return $items;
	}

	/**
	 * Add an item to the report
	 * @param array $params
	 * @param WC_Order $order
	 * @return void
	 */
	protected function add_item($params, $order) {
		if(isset($this->items[$params['product_id']])) { // we are grouping everything by product id, as we dont care about variations in our report
			$params['order_id'] = $order->id;
			$this->items[$params['product_id']]['subtotal'] = $this->items[$params['product_id']]['subtotal'] + $params['subtotal'];
			$this->items[$params['product_id']]['total'] = $this->items[$params['product_id']]['total'] + $params['total'];
			$this->items[$params['product_id']]['fee'] = $this->items[$params['product_id']]['fee'] + $params['fee'];
			$this->items[$params['product_id']]['qty'] = $this->items[$params['product_id']]['qty'] + $params['qty'];
			$this->items[$params['product_id']]['discount'] = $this->items[$params['product_id']]['discount'] + $params['discount'];
			$this->items[$params['product_id']]['items'][] = $params;
		} else { // nothing exists so start a fresh one
			$item = $params;
			$item['order_id'] = $order->id;
			$params['items'] = array($item);
			$this->items[$params['product_id']] = $params;
		}
	}

	/**
	 * Updates the summary
	 * @param  array $params
	 * @return void
	 */
	protected function update_summary($params) {
		$this->summary['subtotal'] = $this->summary['subtotal'] + $params['subtotal'];
		$this->summary['total']    = $this->summary['total'] + $params['total'];
		$this->summary['fee']      = $this->summary['fee'] + $params['fee'];
		$this->summary['qty']      = $this->summary['qty'] + $params['qty'];
		$this->summary['discount'] = $this->summary['discount'] + $params['discount'];
		$this->summary['refund']   = $this->summary['refund'] + $params['refund'];
	}

	/**
	 * Gets the summary
	 * @return void
	 */
	public function get_summary() {
		return json_decode(json_encode($this->summary));
	}

	/**
	 * Gets the items
	 * @return void
	 */
	public function get_items() {
		return $this->items;
	}

	/**
	 * Gets the items
	 * @return void
	 */
	public function get_transfer_id() {
		return $this->transfer_id;
	}

	/**
	 * Gets the items
	 * @return void
	 */
	public function get_transfer() {
		return $this->transfer;
	}


}
