<?php

class FC_Stripe_API {

	/**
	 * Fetches a transfer from the stripe API
	 * @param  string $transfer_id
	 * @return object
	 */
	public static function fetch_transfer($transfer_id) {
		return WC_Stripe_API::request( array(), 'transfers/'. $transfer_id, 'GET' );
	}

	/**
	 * Fetches all transfers after we switched from a custom system to WooCommerce
	 *
	 * We can't generate reports for old transfers as we don't have the order data. Consider
	 * creating a sync method for old orders. See `includes/admin/class-fc-admin-sync.php`,
	 * where the implementation has been started.
	 *
	 * @param  string $transfer_id
	 * @return array
	 */
	public static function fetch_transfers() {
		$result = WC_Stripe_API::request( array(
			'date' => array(
				'gt' => mktime(0,0,0,10,30,2016)
			)
		), 'transfers', 'GET' );
		return $result;
	}

	/**
	 * Fetches a charge from the Stripe API
	 * @param  string $charge_id
	 * @return object
	 */
	public static function get_charge($charge_id) {
		return WC_Stripe_API::request( array(), 'charges/'. $charge_id, 'GET' );
	}

	/**
	 * Fetches all transactions for a specific transfer
	 * @param  string $transfer_id
	 * @param  integer $start_from  The ID where the listings should start from
	 * @return array
	 */
	public static function fetch_transactions($transfer_id, $start_from = null) {
		return WC_Stripe_API::request( array('limit' => 100, 'starting_after' => $start_from), 'transfers/'. $transfer_id .'/transactions', 'GET' );
	}

	/**
	 * Fetches a balance transaction
	 * @param  string $transaction_id
	 * @return object
	 */
	public static function fetch_balance_transaction($transaction_id) {
		return WC_Stripe_API::request( array(), 'balance/history/'. $transaction_id , 'GET' );
	}

	/**
	 * Fetches a complete set of transactions for a transfer
	 *
	 * Stores the transactions in the database for faster recurrence
	 *
	 * @param  string $transfer_id
	 * @return array
	 */
	public static function fetch_complete_transactions($transfer_id)
	{
		if(!$balances = get_option( 'transactions_'. $transfer_id, false )) {
			$has_more = true;
			$start_from = null;
			$balances = array();
			while($has_more) {
				$object = self::fetch_transactions($transfer_id, $start_from);
				$balances = array_merge($balances, $object->data);
				$last = end($object->data);
				$start_from = $last->id;
				$has_more = $object->has_more;
			}
			update_option('transactions_'. $transfer_id, $balances);
		}

		return $balances;
	}

}
