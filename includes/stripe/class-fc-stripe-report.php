<?php
/**
 * Generate a report for a stripe transfer
 */
class FC_Stripe_Report {

	/**
	 * Holds the items
	 * @var array
	 */
	protected $items = array();

	/**
	 * Holds the summary
	 * @var array
	 */
	protected $summary = array(
		'total'    => 0,
		'subtotal' => 0,
		'discount' => 0,
		'fee'      => 0,
		'refund'   => 0,
		'qty'      => 0
	);

	/**
	 * Holds the transfer
	 * @var mixed
	 */
	protected $transfer = null;

	/**
	 * Holds an array of all transactions
	 * @var array
	 */
	protected $transactions = array();

	/**
	 * The transfer id
	 * @var null
	 */
	protected $transfer_id = null;

	/**
	 * Fetchs the transactions and generate the report data
	 * @param string $transfer_id
	 */
	public function __construct( $transfer_id ) {
		$this->transfer_id = $transfer_id;
		$this->fetch_transfer();
		$this->fetch_transactions();
		$this->generate_report_data();
	}

	/**
	 * Fetches the requested transfer
	 * @return void
	 */
	protected function fetch_transfer() {
		$result = FC_Stripe_API::fetch_transfer($this->transfer_id);
		$this->transfer = $result;
	}

	/**
	 * Connects to Stripe and fetches all transactions
	 * @return void
	 */
	protected function fetch_transactions() {
		$result = FC_Stripe_API::fetch_transactions($this->transfer_id);
		$this->transactions = $result->data;
	}

	/**
	 * Generates the report data
	 * @return void
	 */
	protected function generate_report_data() {
		if($this->transactions) {
			foreach($this->transactions as $item) {
				if($item->type == 'charge') {
					$orders = get_posts('post_type=shop_order&post_status=any&meta_key=_stripe_charge_id&meta_value='. $item->id);
					if($orders) {
						$order = wc_get_order( $orders[0] ); // get the order
						$fee = $item->fee / 100; // convert fee to readable money
						$fee_per_product = $fee / count($order->get_items()); // the fee for each product
						foreach($order->get_items() as $order_item) {
							// get the product (regardless of variation or not)
							if($order_item['variation_id']) {
								$product = wc_get_product($order_item['variation_id']);
							} else {
								$product = wc_get_product($order_item['product_id']);
							}
							$price = $order_item['line_total'] / $order_item['qty']; // get the product price
							$subtotal = ($price * $order_item['qty']); // the subtotal
							$discount = $price - ($order_item['line_total'] / $order_item['qty']); // the discount will be the product price minus the price the customer paid
							$params = array(
								'product_id' => $order_item['product_id'],
								'name'       => $order_item['name'],
								'subtotal'   => $subtotal,
								'discount'   => $discount,
								'qty'        => $order_item['qty'],
								'fee'        => $fee_per_product,
								'total'      => $subtotal - $discount - $fee_per_product, // the total transfered from stripe will be the subtotal minus the discount and minus the fee per product
								'refund'     => 0,
							);
							$this->add_item($params, $order); // push it to the items array
							$this->update_summary($params); // update the summary
						}
					}
				} elseif($item->type === 'refund') {
					$orders = get_posts('post_type=shop_order&post_status=any&meta_key=_stripe_charge_id&meta_value='. $item->id);
					if($orders) {
						$order = wc_get_order( $orders[0] ); // get the order
						$fee = $item->fee / 100; // convert fee to readable money
						$fee_per_product = $fee / count($order->get_items()); // the fee for each product
						$refunds = $order->get_total_refunded() / count($order->get_items());
						foreach($order->get_items() as $order_item) {
							// get the product (regardless of variation or not)
							if($order_item['variation_id']) {
								$product = wc_get_product($order_item['variation_id']);
							} else {
								$product = wc_get_product($order_item['product_id']);
							}
							$price = $order_item['line_total'] / $order_item['qty']; // get the product price
							$subtotal = ($price * $order_item['qty']); // the subtotal
							$discount = 0; // the discount will be the product price minus the price the customer paid
							$params = array(
								'product_id' => $order_item['product_id'] . '-ref',
								'name'       => 'Refundert:'. $order_item['name'],
								'subtotal'   => '-'. $refunds,
								'discount'   => $discount,
								'qty'        => $order_item['qty'],
								'fee'        => $fee_per_product,
								'total'      => ('-'. $refunds) - $discount - $fee_per_product, // the total transfered from stripe will be the subtotal minus the discount and minus the fee per product
								'refund'     => $refunds,
							);
							$this->add_item($params, $order); // push it to the items array
							$this->update_summary($params); // update the summary
						}
					}
				}
			}
		}
	}

	/**
	 * Add an item to the report
	 * @param array $params
	 * @param WC_Order $order
	 * @return void
	 */
	protected function add_item($params, $order) {
		if(isset($this->items[$params['product_id']])) { // we are grouping everything by product id, as we dont care about variations in our report
			$params['order_id'] = $order->id;
			$this->items[$params['product_id']]['subtotal'] = $this->items[$params['product_id']]['subtotal'] + $params['subtotal'];
			$this->items[$params['product_id']]['total'] = $this->items[$params['product_id']]['total'] + $params['total'];
			$this->items[$params['product_id']]['fee'] = $this->items[$params['product_id']]['fee'] + $params['fee'];
			$this->items[$params['product_id']]['qty'] = $this->items[$params['product_id']]['qty'] + $params['qty'];
			$this->items[$params['product_id']]['discount'] = $this->items[$params['product_id']]['discount'] + $params['discount'];
			$this->items[$params['product_id']]['items'][] = $params;
		} else { // nothing exists so start a fresh one
			$item = $params;
			$item['order_id'] = $order->id;
			$params['items'] = array($item);
			$this->items[$params['product_id']] = $params;
		}
	}

	/**
	 * Updates the summary
	 * @param  array $params
	 * @return void
	 */
	protected function update_summary($params) {
		$this->summary['subtotal'] = $this->summary['subtotal'] + $params['subtotal'];
		$this->summary['total']    = $this->summary['total'] + $params['total'];
		$this->summary['fee']      = $this->summary['fee'] + $params['fee'];
		$this->summary['qty']      = $this->summary['qty'] + $params['qty'];
		$this->summary['discount'] = $this->summary['discount'] + $params['discount'];
		$this->summary['refund']   = $this->summary['refund'] + $params['refund'];
	}

	/**
	 * Gets the summary
	 * @return void
	 */
	public function get_summary() {
		return json_decode(json_encode($this->summary));
	}

	/**
	 * Gets the items
	 * @return void
	 */
	public function get_items() {
		return json_decode(json_encode($this->items));
	}

	/**
	 * Gets the items
	 * @return void
	 */
	public function get_transfer_id() {
		return $this->transfer_id;
	}

	/**
	 * Gets the items
	 * @return void
	 */
	public function get_transfer() {
		return $this->transfer;
	}


}
