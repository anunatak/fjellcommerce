<?php
/**
 * Creates events based on user input
 */
class FC_Events {

	/**
	 * Parsed filters from the user
	 * @return array
	 */
	protected function parse_filters()
	{
		$filters = isset($_REQUEST['filters']) ? $_REQUEST['filters'] : array();

		if(isset($_REQUEST['festival'])) {
			$filters['nff_program_festival'] = array(
				'type' => 'taxonomy',
				'items' => array(
					array( 'value' => $_REQUEST['festival'])
				)
			);
		}

		if(isset($_REQUEST['year'])) {
			$filters['nff_program_year'] = array(
				'type' => 'taxonomy',
				'items' => array(
					array( 'value' => $_REQUEST['year'])
				)
			);
		}

		$filters = new FC_Events_Filter($filters);

		return $filters->parse();
	}

	/**
	 * Gets a taxonomy
	 * @param  name $taxonomy
	 * @param  array $filters
	 * @return void
	 */
	protected function get_taxonomy($taxonomy, $filters)
	{
		$activeTaxonomy = 0;
		$terms = array_map(function($item) {
			$active = false;
			if(isset($filters[$taxonomy])) {
				foreach($filters[$taxonomy]['items'] as $item) {
					if($category->slug === $item['value']) {
						$active = true;
						$activeTaxonomy++;
					}
				}
			}
			return array(
				'name' => $item->name,
				'value' => $item->slug,
				'active' => $active
			);
		}, get_terms( array('taxonomy' => $taxonomy, 'hide_empty' => false) ));

		array_unshift($terms, array(
			'value' => 'all',
			'name' => 'Alle',
			'active' => $activeTaxonomy ? false : true,
		));

		return $terms;
	}

	/**
	 * Gets the search query
	 * @param  array $filters
	 * @return string
	 */
	protected function get_search_query($filters)
	{
		return isset($filters['nff_program_search']) && $filters['nff_program_search']['value'] !== 'false' ? $filters['nff_program_search']['value'] : '';
	}

	/**
	 * Gets days with events in them
	 * @return array
	 */
	protected function get_event_days()
	{
		$args = $this->get_event_query();
		$days = array();

		$activeDays = 0;
		add_filter('posts_orderby', array($this, 'orderby'));
		$posts = new WP_Query($args);
		remove_filter('posts_orderby', array($this, 'orderby'));
		foreach($posts->posts as $post) {
			$date = date_create_from_format( 'Ymd', $post->event_date );

			if(!isset($days[$date->format('U')])) {
				$active = false;
				if(isset($filters['nff_program_date'])) {
					foreach($filters['nff_program_date']['items'] as $item) {
						if($date->format('Ymd') === $item['value']) {
							$active = true;
							$activeDays++;
						}
					}
				}
				$days[$date->format('U')] = array(
					'name' => date_i18n( 'l j. F', $date->format('U') ),
					'value' => $date->format('Ymd'),
					'active' => $active,
					'hidden' => false
				);
			}
		}

		array_unshift($days, array(
			'value' => 'all',
			'name' => 'Alle',
			'active' => $activeDays ? false : true,
			'hidden' => false
		));

		ksort($days);

		return $days;
	}

	/**
	 * Gets ages
	 * @param  array $filters
	 * @return array
	 * @todo  does nothing
	 */
	protected function get_ages($filters)
	{
		return $filters;
	}

	/**
	 * Gets all filters
	 * @return array
	 */
	public function get_filters()
	{
		$filters    = $this->parse_filters();
		$categories = $this->get_taxonomy('nff_program_category', $filters);
		$activities = $this->get_taxonomy('nff_program_activity', $filters);
		$search     = $this->get_search_query($filters);
		$events     = $this->get_events($filters);
		$days       = $this->get_event_days();
		$ages       = $this->get_ages($filters);
		$pages = 0;
		return array(
			'category' => array('type' => 'taxonomy', 'items' => $categories, 'hidden' => false),
			'activity' => array('type' => 'taxonomy', 'items' => $activities, 'hidden' => false),
			'search'   => array('value' => $search, 'type' => 'search', 'hidden' => false),
			'ages'     => array('type' => 'field', 'value' => $ages, 'hidden' => true),
			'days'     => array('type' => 'field', 'items' => $days, 'hidden' => false),
			'events' => $events,
			'page' => 1,
			'pages' => $pages
		);
	}

	/**
	 * Gets events
	 * @param  array  $filters
	 * @return array
	 */
	public function get_events($filters = array())
	{
		if(!$filters) {
			$filters = $this->parse_filters();
		}

		$query = $this->get_event_query($filters);

		add_filter('posts_fields', array($this, 'fields'));
		add_filter('posts_where', array($this, 'where'));
		add_filter('posts_orderby', array($this, 'orderby'));
		$loop = new WP_Query($query);
		remove_filter('posts_fields', array($this, 'fields'));
		remove_filter('posts_where', array($this, 'where'));
		remove_filter('posts_orderby', array($this, 'orderby'));
		$events = array();
		while($loop->have_posts()) : $loop->the_post();
			$post = get_post();
			$product = wc_get_product($post);
			$fragments = explode('–', get_the_title());
			$title = str_replace(end($fragments), '', $post->post_title);
			$title = htmlentities($title);
			$title = str_replace('&ndash;', '', $title);
			$title = trim(html_entity_decode($title));
			$image = get_post_thumbnail_id($post->fjellcommerce_post);
			$img = '';
			if($image) {
				$img = wp_get_attachment_image_src( $image, $size = 'program-thumbnail' );
				$img = $img[0];
			}
			$events[] = array(
				'id' => get_the_ID(),
				'eventId' => $post->fjellcommerce_event,
				'title' => $title,
				'date' => date_create_from_format('Ymd', $post->event_date)->format('Ymd'),
				'time' => $post->event_time,
				'url' => get_permalink(),
				'activities' => wp_get_object_terms( get_the_ID(), 'nff_program_activity', array('fields' => 'slugs') ),
				'ticket_sale' => $post->sales_start,
				'availiable_tickets' => $product->get_stock_quantity(),
				'img' => $img,
				'tickets' => nff_get_tickets(get_the_ID(), $post->fjellcommerce_event),
				'heart' => nff_event_is_hearted(get_the_ID(), $post->fjellcommerce_event),
				'hidden' => false
		 	);
		endwhile;
		return $events;
	}

	/**
	 * Creates the WP Query args by parsing with the defaults
	 * @param  array  $args
	 * @return array
	 */
	protected function get_event_query($args = array())
	{

		return wp_parse_args( $args, array(
			'post_type'      => 'product',
			'product_cat'    => 'Events',
			'order'          => 'ASC',
			'meta_key'       => 'event_date',
			'orderby'        => 'meta_value_num',
			'posts_per_page' => -1
		) );
	}

	/**
	 * Can be used to do some kind of custom query stuff
	 * @param  string $query
	 * @return string
	 */
	public function fields($query)
	{
		return $query;
	}

	/**
	 * Can be used to do some kind of custom query stuff
	 * @param  string $query
	 * @return string
	 */
	public function where($query)
	{
		return $query;
	}

	/**
	 * Can be used to do some kind of custom query stuff
	 * @param  string $query
	 * @return string
	 */
	public function orderby($query)
	{
		return $query;
	}

}
