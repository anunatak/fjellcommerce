<?php
/**
 * Tickets
 *
 * @class     FC_Participants
 * @version   2.5.0
 * @package   WooCommerce/Classes/Products
 * @category  Class
 * @author    WooThemes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

include_once 'pdf/class-fc-pdf-participant.php';
include_once 'excel/class-fc-excel-participant.php';

/**
 * FC_Participants Class.
 */
class FC_Participants {

	/**
	 * Hook in methods.
	 */
	public static function init() {
		add_action( 'parse_request', array( __CLASS__, 'participant_view' ) );
	}

	/**
	 * Creates a new PDF instance
	 * @param  string $id WooCommerce Order Key
	 * @return FC_PDF_Tickets
	 */
	public static function create_pdf($id) {
		$participants = static::create_participants($id);
		if(!$participants) {
			return false;
		}
		return new FC_PDF_Participant($participants['events']);
	}

	/**
	 * Creates a new Excel instance
	 * @param  string $id WooCommerce Order Key
	 * @return FC_Excel_Tickets
	 */
	public static function create_excel($id) {
		$participants = static::create_participants($id);
		if(!$participants) {
			return false;
		}
		return new FC_Excel_Participant($participants['events']);
	}

	/**
	 * Find all tickets from the selected order
	 * @param  string $id WooCommerce Order Key
	 * @return array
	 */
	public static function create_participants($id) {
		if(!is_array($id)) {
			$id = array($id);
		}
		$events = array();
		$api = new FC_WooCommerce_API;
		foreach($id as $product_id) {
			if(has_term( 'Events', 'product_cat', $product_id )) {
				$product = wc_get_product($product_id);
				$event = array(
					'name' => $product->get_title(),
					'participants' => array()
				);

				$orders = $api->get_orders_by_product_id($product_id);
				if($orders) {
					foreach($orders as $post) {
						$order = wc_get_order($post);
						foreach($order->get_items() as $item) {
							if($item['product_id'] == $product_id) {
								for ($i=1; $i <= $item['qty']; $i++) {
									$name = trim($order->billing_first_name . ' ' . $order->billing_last_name) ? $order->billing_first_name . ' ' . $order->billing_last_name : 'Deltaker uten navn';
									$email = $order->billing_email ? $order->billing_email : ' ';
									$phone = $order->billing_phone ? $order->billing_phone : ' ';
									$comment = ' ';
									$event['participants'][] = array(
										'name'    => apply_filters('fjellcommerce_ticket_holder_name', $name, $order->id, $product_id, $item['variation_id'], $i),
										'email'   => apply_filters('fjellcommerce_ticket_holder_email', $email, $order->id, $product_id, $item['variation_id'], $i),
										'phone'   => apply_filters('fjellcommerce_ticket_holder_phone', $phone, $order->id, $product_id, $item['variation_id'], $i),
										'comment' => apply_filters('fjellcommerce_ticket_holder_comment', $comment, $order->id, $product_id, $item['variation_id'], $i),
									);
								}
							}
						}
					}
				}

				$events[] = $event;
			}
		}

		return array(
			'events' => $events
		);
	}

	/**
	 * Find all tickets from the selected order
	 * @param  string $id WooCommerce Order Key
	 * @return array
	 */
	public static function create_participants_from_ticket($id) {
		if(!is_array($id)) {
			$id = array($id);
		}
		$events = array();
		$api = new FC_WooCommerce_API;
		foreach($id as $product_id) {
			$product = wc_get_product($product_id);
			if(has_term( 'Events', 'product_cat', $product->parent->id )) {
				$event = array(
					'name' => $product->get_title(),
					'participants' => array()
				);

				$orders = $api->get_orders_by_product_id($product->parent->id);
				if($orders) {
					foreach($orders as $post) {
						$order = wc_get_order($post);
						foreach($order->get_items() as $item) {
							if(isset($item['variation_id']) && $item['variation_id'] && $item['variation_id'] == $product_id) {
								for ($i=1; $i <= $item['qty']; $i++) {
									$name    = trim($order->billing_first_name . ' ' . $order->billing_last_name) ? $order->billing_first_name . ' ' . $order->billing_last_name : 'Deltaker uten navn';
									$email   = $order->billing_email ? $order->billing_email : ' ';
									$phone   = $order->billing_phone ? $order->billing_phone : ' ';
									$comment = ' ';
									$event['participants'][] = array(
										'name'    => apply_filters('fjellcommerce_ticket_holder_name', $name, $order->id, $product_id, $item['variation_id'], $i),
										'email'   => apply_filters('fjellcommerce_ticket_holder_email', $email, $order->id, $product_id, $item['variation_id'], $i),
										'phone'   => apply_filters('fjellcommerce_ticket_holder_phone', $phone, $order->id, $product_id, $item['variation_id'], $i),
										'comment' => apply_filters('fjellcommerce_ticket_holder_comment', $comment, $order->id, $product_id, $item['variation_id'], $i),
									);
								}
							}
						}
					}
				}

				$events[] = $event;
			}
		}

		return array(
			'events' => $events
		);
	}

	/**
	 * Handle the ticket request
	 * @return void
	 */
	public static function participant_view() {
		global $wp;

		$request = explode( '/', $wp->request );

		if ( isset($request[0]) && $request[0] === 'participants' ) {
			if(isset($request[1]) && isset($request[2])) {
				$post_id = $request[1];
				$product_id = $request[2];
				$export_type =  isset($request[3]) ? $request[3] : 'pdf';

				if($product_id === 'all') {
					$product_id = get_post_meta( $post_id, 'event_products', true );
				} else if(stripos($product_id, 'ticket') !== FALSE) {
					$product_id = str_replace('ticket-', '', $product_id);
					$participants = static::create_participants_from_ticket($product_id);
					if(!$participants) {
						return false;
					}


					if($export_type === 'excel') {
						$excel = new FC_Excel_Participant($participants['events'], $product_id);
						return $excel->render();
					}
					elseif($export_type === 'pdf') {
						$pdf = new FC_PDF_Participant($participants['events'], $product_id);
						$pdf->render();
						die;
					}
				}

				if($export_type === 'pdf') {
					$pdf = static::create_pdf($product_id);
					$pdf->render();
					die;
				}
				elseif($export_type === 'excel') {
					$excel = static::create_excel($product_id);
					return $excel->render();
				}
				die;
			}
		}
	}

}

FC_Participants::init();
