<?php
/**
 * WooCommerce Api
 *
 * Registers post types and taxonomies.
 *
 * @class     FC_WooCommerce_API
 * @version   2.5.0
 * @package   WooCommerce/Classes/Products
 * @category  Class
 * @author    WooThemes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * FC_Post_types Class.
 */
class FC_WooCommerce_API {

	/**
	 * Gets the number of items sold via a specific payment method for a product
	 * @param  integer $product_id
	 * @param  string $payment_method
	 * @return integer
	 * @todo  Refactor to stay DRY
	 */
	public static function get_sold_payment_method_items($product_id, $payment_method) {
		global $wpdb;
		$query = "SELECT order_id FROM {$wpdb->prefix}woocommerce_order_itemmeta woim
		LEFT JOIN {$wpdb->prefix}woocommerce_order_items oi
		ON woim.order_item_id = oi.order_item_id
		WHERE meta_key = '_product_id' AND meta_value = %d

		GROUP BY order_id;";
		$order_ids = $wpdb->get_col( $wpdb->prepare( $query, $product_id ) );
		if( $order_ids ) {
			$sold = 0;
			$args = array(
				'post_type' =>'shop_order',
				'post__in' => $order_ids,
				'post_status' => 'wc-completed',
				'meta_key' => '_payment_method',
				'meta_value' => $payment_method,
				'posts_per_page' => 20,
				'order' => 'DESC'
			);
			$posts = get_posts( $args );
			foreach($posts as $post) {
				$order = wc_get_order($post);
				foreach($order->get_items() as $item) {
					$sold += $item['qty'];
				}
			}
			return $sold;
		}
		return 0;
	}

	/**
	 * Gets the number of items sold of a specific variation
	 * @param  integer $variation_id
	 * @return integer
	 * @todo  Refactor to stay DRY
	 */
	public static function get_sold_variation_items($variation_id) {
		$product = wc_get_product($variation_id)->parent;
		global $wpdb;
		$query = "SELECT order_id FROM {$wpdb->prefix}woocommerce_order_itemmeta woim
		LEFT JOIN {$wpdb->prefix}woocommerce_order_items oi
		ON woim.order_item_id = oi.order_item_id
		WHERE meta_key = '_product_id' AND meta_value = %d

		GROUP BY order_id;";
		$order_ids = $wpdb->get_col( $wpdb->prepare( $query, $product->id ) );
		if( $order_ids ) {
			$sold = 0;
			$args = array(
				'post_type' =>'shop_order',
				'post__in' => $order_ids,
				'post_status' => 'wc-completed',
				'posts_per_page' => -1,
				'order' => 'DESC'
			);
			$posts = get_posts( $args );
			foreach($posts as $post) {
				$order = wc_get_order($post);
				foreach($order->get_items() as $item) {
					if($item['variation_id'] == $variation_id) {
						$sold += $item['qty'];
					}
				}
			}
			return $sold;
		}
		return 0;
	}

	/**
	 * Gets the total amount sold (money) of a specific variation
	 * @param  integer $variation_id
	 * @return integer
	 * @todo  Refactor to stay DRY
	 */
	public static function get_sold_variation_total($variation_id) {
		$product = wc_get_product($variation_id)->parent;
		global $wpdb;
		$query = "SELECT order_id FROM {$wpdb->prefix}woocommerce_order_itemmeta woim
		LEFT JOIN {$wpdb->prefix}woocommerce_order_items oi
		ON woim.order_item_id = oi.order_item_id
		WHERE meta_key = '_product_id' AND meta_value = %d

		GROUP BY order_id;";
		$order_ids = $wpdb->get_col( $wpdb->prepare( $query, $product->id ) );
		if( $order_ids ) {
			$sold = array(
				'subtotal' => 0,
				'total' => 0,
				'discount' => 0,
				'fee' => 0
			);
			$args = array(
				'post_type' =>'shop_order',
				'post__in' => $order_ids,
				'post_status' => 'wc-completed',
				'posts_per_page' => -1,
				'order' => 'DESC'
			);
			$posts = get_posts( $args );
			foreach($posts as $post) {
				$order = wc_get_order($post);
				foreach($order->get_items() as $item) {
					if($item['variation_id'] == $variation_id) {
						$product = wc_get_product($item['variation_id']);
						$price = $product->get_price(); // get the product price
						$subtotal = ($price * $item['qty']); // the subtotal
						$discount = $price - ($item['line_total'] / $item['qty']); // the discount will be the product price minus the price the customer paid
						$percent  = 2.4;
						$partial  = $percent/100;
						$money    = 2;
						$price = (int) $price;
						$number = $price * ($percent / 100);
						$fee = $number + $money;
						$sold['subtotal'] += $subtotal;
						$sold['total'] += $subtotal - $discount - $fee;
						$sold['discount'] += $discount;
						$sold['fee'] += $fee;
					}
				}
			}
			return $sold;
		}
		return 0;
	}

	/**
	 * Gets orders by a specific product ID
	 * @param  integer $product_id
	 * @return array|boolean
	 * @todo  Refactor to stay DRY
	 */
	public static function get_orders_by_product_id($product_id, $include_processing = false)
	{
		global $wpdb;
		$query = "SELECT order_id FROM {$wpdb->prefix}woocommerce_order_itemmeta woim
		LEFT JOIN {$wpdb->prefix}woocommerce_order_items oi
		ON woim.order_item_id = oi.order_item_id
		WHERE meta_key = '_product_id' AND meta_value = %d

		GROUP BY order_id;";
		$order_ids = $wpdb->get_col( $wpdb->prepare( $query, $product_id ) );
		if( $order_ids ) {
			$args = array(
				'post_type' =>'shop_order',
				'post__in' => $order_ids,
				'post_status' => ['wc-completed', 'wc-processing'],
				'posts_per_page' => -1,
				'order' => 'DESC'
			);
			return get_posts( $args );
		}
		return false;
	}

	/**
	 * Gets orders by a specific variation ID
	 * @param  integer $product_id
	 * @return array|boolean
	 * @todo  Refactor to stay DRY
	 */
	public static function get_orders_by_variation_id($variation_id, $include_processing = false)
	{
		global $wpdb;
		$query = "SELECT order_id FROM {$wpdb->prefix}woocommerce_order_itemmeta woim
		LEFT JOIN {$wpdb->prefix}woocommerce_order_items oi
		ON woim.order_item_id = oi.order_item_id
		WHERE meta_key = '_variation_id' AND meta_value = %d

		GROUP BY order_id;";
		$order_ids = $wpdb->get_col( $wpdb->prepare( $query, $variation_id ) );
		if( $order_ids ) {
			$args = array(
				'post_type' =>'shop_order',
				'post__in' => $order_ids,
				'post_status' => ['wc-completed', 'wc-processing'],
				'posts_per_page' => -1,
				'order' => 'DESC'
			);
			return get_posts( $args );
		}
		return false;
	}

	/**
	 * Get all products that are synced
	 *
	 * @param integer $post_id
	 * @param integer $event_id
	 * @return void
	 */
	public function get_synced_query( $post_id, $event_id ) {
		$query = new WP_Query( array(
				'post_type' => 'product',
				'posts_per_page' => -1,
				'meta_query' => array(
					array(
						'key' => 'fjellcommerce_post',
						'value' => $post_id,
					),
					array(
						'key' => 'fjellcommerce_event',
						'value' => $event_id,
					)
				)
			) );

		return $query;
	}

	/**
	 * Checks if the event have been synced before
	 *
	 * @param integer $post_id
	 * @param integer $event_id
	 * @return boolean
	 */
	protected function has_synced( $post_id, $event_id ) {
		return $this->get_synced_query( $post_id, $event_id )->found_posts > 0;
	}

	/**
	 * Performs the save action
	 *
	 * @param array   $args
	 * @param array   $variations
	 * @return void
	 */
	public function save( $args, $variations = array() ) {
		if ( $this->has_synced( $args['post'], $args['event'] ) ) {
			$product = $this->get_synced_query( $args['post'], $args['event'] )->posts[0];
			$args['id'] = $product->ID;
		} else {
			$args['id'] = null;
		}
		$args['event_date'] = date_create_from_format('d.m.Y', $args['event_date']);
		$args['event_date'] = $args['event_date']->format('Ymd');
		$attributes = array(
			'id' => $args['id'],
			'title' => $args['title'],
			'quantity' => $args['quantity'],
			'virtual' => true,
			'categories' => array(
				'Events'
			),
			'taxonomies' => array(
				'nff_program_venue'    => $args['venue'],
				'nff_program_category' => $args['category'],
				'nff_program_activity' => $args['activity'],
				'nff_program_tags'     => $args['tags'],
				'nff_program_festival' => $args['festival'],
				'nff_program_year'     => $args['year'],
			),
			'meta' => array(
				'fjellcommerce_post'  => $args['post'],
				'fjellcommerce_event' => $args['event'],
				'sales_start'         => $args['sales_start'],
				'sales_stop'          => $args['sales_stop'],
				'event_date'          => $args['event_date'],
				'event_time'          => $args['event_time'],
				'age_from'            => $args['age_from'],
				'_user_information'   => $args['requires_information'] ? 'yes' : 'no',
				'_use_seatmaps'       => $args['ticket_type'] === 'seatmap' ? 'yes' : 'no',
				'age_to'              => $args['age_to']
			),
			'availiable_attributes' => array(
				'ticket_type'
			)
		);

		if($args['checkout_rule'] !== 'none') {
			if($args['checkout_rule'] === 'crs') {
				$attributes['meta']['_bulkdiscount_enabled'] = 'yes';
				$attributes['meta']['_bulkdiscount_text_info'] = 'Søskenrabatt';
				$attributes['meta']['_bulkdiscount_quantity_1'] = '2';
				$attributes['meta']['_bulkdiscount_discount_fixed_1'] = '100';
			}
		}

		$product = new FC_WooCommerce_Product( $attributes );

		$new_variations = array();
		foreach ( $variations as $variation ) {
			$new_variations[] = array(
				'price' => $variation['price'],
				'quantity' => $variation['quantity'],
				'attributes' => array(
					'ticket_type' => $variation['title']
				)
			);
		}
		$product->variation( $new_variations );

		return $product;
	}


}
