<?php
/**
 * WooCommerce Modifications
 *
 * Modifies woocommerce so it fits FjellCommerce
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
class FC_WooCommerce_Mods {


	/**
	 * Hook in methods.
	 */
	public static function init() {
		add_action( 'wp', array( __CLASS__, 'redirect_product_to_event' ) );
		add_action( 'init', array( __CLASS__, 'add_rewrite_endpoint' ) );
		add_filter( 'woocommerce_is_purchasable', array(__CLASS__, 'prevent_purchase'), 10, 2);
		add_action( 'pre_get_posts', array( __CLASS__, 'remove_events'));
		add_filter('woocommerce_credit_card_form_fields', array(__CLASS__, 'modify_form_fields'), 10, 2);
		add_action('wp_enqueue_scripts', array(__CLASS__, 'eager_load_cc_form_script'));
	}

	/**
	 * Makes sure the CC checkout is enabled
	 * @return [type] [description]
	 */
	public static function eager_load_cc_form_script() {
		if(is_checkout()) {
			wp_enqueue_script( 'wc-credit-card-form' );
		}
	}

	/**
	 * The WooCommerce CC gateway did not work on iOS/mobile devices. This is a fix!
	 * @param  array $fields
	 * @param  string $payment_gateway
	 * @return array
	 */
	public static function modify_form_fields($fields, $payment_gateway) {
		if($payment_gateway == 'stripe') {
			$fields['card-expiry-field'] = str_replace('inputmode="numeric"', 'inputmode="verbose"', $fields['card-expiry-field']);
			$fields['card-expiry-field'] = str_replace('type="tel"', 'type="text"', $fields['card-expiry-field']);
		}
		return $fields;
	}

	/**
	 * Hides events from the "Products" page in the backend.
	 *
	 * So that the client only updates events from the "Arrangementer" page
	 *
	 * @param  WP_Query $query
	 * @return void
	 */
	public static function remove_events($query) {
		if(!is_admin())
			return;

		if(!$query->is_main_query())
			return;

		if(is_post_type_archive( 'product' ) && !current_user_can( 'manage_options' )) {
			$query->set('tax_query', array(
				array(
			        'taxonomy' => 'product_cat',
			        'terms' => array('events'),
			        'field' => 'slug',
			        'operator' => 'NOT IN',
		        )
		    ));
		}
	}

	/**
	 * Prevents purchases
	 *
	 * A ticket can only be purchased for events of the current active
	 * festival and year.
	 *
	 * Every event has the option to add a custom sales start date. In
	 * addition every festival can set its sales start date. The default
	 * is June 1st the current year.
	 *
	 * @param  [type] $purchasable [description]
	 * @param  [type] $product     [description]
	 * @return [type]              [description]
	 */
	public static function prevent_purchase($purchasable, $product) {

		$active_festival = FjellCommerce()->get_active_festival();
		$active_year = FjellCommerce()->get_active_year();
		if(has_term( 'Events', 'product_cat', $product->id )) {
			if(has_term( $active_festival->slug, 'nff_program_festival', $product->id ) && has_term( $active_year, 'nff_program_year', $product->id )) {
				$purchasable = true;
				$sales_start = get_post_meta( $product->id, 'sales_start', true );
				if(FjellCommerce()->has_sale_not_started($product)) {
					$purchasable = false;
				}
				if(FjellCommerce()->has_sale_stopped($product)) {
					$purchasable = false;
				}
			} else {
				$purchasable = false;
			}
		}

		return $purchasable;
	}

	/**
	 * Adds a endpoint
	 */
	public static function add_rewrite_endpoint() {
		add_rewrite_endpoint( 'dato', EP_PERMALINK | EP_PAGES );
	}

	/**
	 * Redirects products to events when applicable
	 *
	 * @return void
	 */
	public static function redirect_product_to_event() {
		if ( is_product() ) {
			if ( has_term( 'Events', 'product_cat' ) ) {
				global $post;
				$post_id = $post->fjellcommerce_post;
				$event_id = $post->fjellcommerce_event;
				$event_date = $post->event_date;
				$date = date_create_from_format( 'Ymd', $event_date );
				$url = get_permalink( $post_id ) . 'dato/' . $date->format( 'd-m-Y' );
				wp_redirect( $url ); exit;
			}
		}
	}

}

FC_WooCommerce_Mods::init();
