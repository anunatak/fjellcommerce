<?php
/**
 * WooCommerce Product
 *
 * A MVC-ish implementation of the WooCommerce Product (before they released it)
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class FC_WooCommerce_Product {

	/**
	 * Holds all arguments for the product
	 *
	 * @var array
	 */
	protected $args = array();

	/**
	 * Construct a new product
	 *
	 * @param array   $args
	 */
	public function __construct( $args ) {
		$this->handle_args( $args );
		$this->save();
	}

	/**
	 * Merges the args with the defaults
	 *
	 * @param array   $args
	 * @return void
	 */
	protected function handle_args( $args ) {
		$args = wp_parse_args( $args, array(
				'id' => null,
				'title' => null,
				'quantity' => 0,
				'virtual' => false,
				'description' => '',
				'updating' => false,
				'price' => 0,
				'sku' => '',
				'meta' => array(),
				'taxonomies' => array(),
				'categories' => array(),
				'availiable_attributes' => array()
			) );

		$this->args = $args;
	}

	/**
	 * Gets the product ID
	 *
	 * @return integer
	 */
	public function get_product_id() {
		return $this->args['id'];
	}

	/**
	 * Saves the product
	 *
	 * @return void
	 */
	protected function save() {
		if ( $this->args['id'] ) {
			$this->update();
		} else {
			$this->create();
		}
	}

	/**
	 * Creates a product if its not saved
	 *
	 * @return void
	 */
	protected function create() {
		$post = array(
			'post_author' => get_current_user_id(),
			'post_content' => $this->args['description'],
			'post_status' => 'publish',
			'post_title' => $this->args['title'],
			'post_parent' => '',
			'post_type' => 'product',
			'meta_input' => $this->args['meta'],
			'tax_input' => $this->args['taxonomies']
		);

		$post_id = wp_insert_post( $post ); // Insert the post returning the new post id

		if ( !$post_id ) {
			return false;
		}

		$this->args['updating'] = false;

		$this->args['id'] = $post_id;

		update_post_meta( $post_id, '_sku', $this->args['sku'] ); // Set its SKU
		update_post_meta( $post_id, '_visibility', 'visible' ); // Set the product to visible, if not it won't show on the front end
		update_post_meta( $post_id, '_virtual', $this->args['virtual'] ? 'yes' : 'no' );
		update_post_meta( $post_id, '_manage_stock', 'yes' );
		update_post_meta( $post_id, '_stock', $this->args['quantity'] );

		wp_set_object_terms( $post_id, $this->args['categories'], 'product_cat' ); // Set up its categories
	}

	/**
	 * Updates an existing product
	 *
	 * @return void
	 */
	protected function update() {
		$this->args['updating'] = true;

		$post = array(
			'ID' => $this->args['id'],
			'post_author' => get_current_user_id(),
			'post_content' => $this->args['description'],
			'post_status' => 'publish',
			'post_title' => $this->args['title'],
			'post_parent' => '',
			'post_type' => 'product',
			'meta_input' => $this->args['meta'],
			'tax_input' => $this->args['taxonomies']
		);

		$post_id = wp_update_post( $post );

		if ( !$post_id ) {
			return false;
		}

		$this->args['id'] = $post_id;

		update_post_meta( $post_id, '_sku', $this->args['sku'] ); // Set its SKU
		update_post_meta( $post_id, '_visibility', 'visible' ); // Set the product to visible, if not it won't show on the front end
		update_post_meta( $post_id, '_manage_stock', 'yes' );
		update_post_meta( $post_id, '_virtual', $this->args['virtual'] ? 'yes' : 'no' );
		update_post_meta( $post_id, '_stock', $this->args['quantity'] );

		wp_set_object_terms( $post_id, $this->args['categories'], 'product_cat' ); // Set up its categories
	}

	/**
	 * Add variations to the product
	 *
	 * @param array   $variations
	 * @return void
	 */
	public function variation( $variations ) {
		if ( $this->args['updating'] ) {
			$this->update_variation( $variations );
		} else {
			$this->create_variation( $variations );
		}
	}

	/**
	 * Creates variations
	 *
	 * @param array   $variations
	 * @return void
	 */
	public function create_variation( $variations ) {
		wp_set_object_terms( $this->args['id'], 'variable', 'product_type' );
		$this->insert_product_attributes( $this->args['id'], $this->args['availiable_attributes'], $variations );
		$this->insert_product_variations( $this->args['id'], $variations );
	}

	/**
	 * Insert product attributes
	 *
	 * @param integer $post_id
	 * @param array   $availiable_attributes
	 * @param array   $variations
	 * @return void
	 */
	protected function insert_product_attributes( $post_id, $availiable_attributes, $variations ) {
		foreach ( $availiable_attributes as $attribute ) {
			$values = array(); // Set up an array to store the current attributes values.

			foreach ( $variations as $variation ) {
				$attribute_keys = array_keys( $variation['attributes'] ); // Get the keys for the current variations attributes

				foreach ( $attribute_keys as $key ) {
					if ( $key === $attribute ) {
						$values[] = $variation['attributes'][$key];
					}
				}
			}

			$values = array_unique( $values );
			wp_set_object_terms( $post_id, $values, 'pa_' . $attribute );
		}

		$product_attributes_data = array();

		foreach ( $availiable_attributes as $attribute ) {
			$product_attributes_data['pa_'.$attribute] = array(
				'name'         => 'pa_'.$attribute,
				'value'        => '',
				'is_visible'   => '1',
				'is_variation' => '1',
				'is_taxonomy'  => '1'
			);
		}

		update_post_meta( $post_id, '_product_attributes', $product_attributes_data );
	}

	/**
	 * Insert product variations
	 *
	 * @param integer $post_id
	 * @param array   $variations
	 * @return void
	 */
	protected function insert_product_variations( $post_id, $variations ) {
		foreach ( $variations as $index => $variation ) {
			$variation_post = array( // Setup the post data for the variation

				'post_title'  => 'Variation #'.$index.' of '.count( $variations ).' for product#'. $post_id,
				'post_name'   => 'product-'.$post_id.'-variation-'.$index,
				'post_status' => 'publish',
				'post_parent' => $post_id,
				'post_type'   => 'product_variation',
				'guid'        => home_url() . '/?product_variation=product-' . $post_id . '-variation-' . $index
			);

			$variation_post_id = wp_insert_post( $variation_post ); // Insert the variation

			foreach ( $variation['attributes'] as $attribute => $value ) {
				$attribute_term = get_term_by( 'name', $value, 'pa_'.$attribute ); // We need to insert the slug not the name into the variation post meta

				update_post_meta( $variation_post_id, 'attribute_pa_'.$attribute, $attribute_term->slug );
			}

			update_post_meta( $variation_post_id, '_price', $variation['price'] );
			update_post_meta( $variation_post_id, '_virtual', $this->args['virtual'] ? 'yes' : 'no' );
			update_post_meta( $variation_post_id, '_regular_price', $variation['price'] );
			if($variation['quantity'] !== false){
				update_post_meta( $variation_post_id, '_manage_stock', 'yes' );
				update_post_meta( $variation_post_id, '_stock', $variation['quantity'] );
				update_post_meta( $variation_post_id, '_stock_status', $variation['quantity'] !== '0' ? 'instock' : 'outofstock' );
			}
		}
	}

	/**
	 * Updates variations
	 *
	 * @param array   $variations
	 * @return void
	 */
	protected function update_variation( $variations ) {
		wp_set_object_terms( $this->args['id'], 'variable', 'product_type' );
		$this->insert_product_attributes( $this->args['id'], $this->args['availiable_attributes'], $variations );
		$this->update_product_variations( $this->args['id'], $variations );
	}

	/**
	 * Insert product variations
	 *
	 * @param integer $post_id
	 * @param array   $variations
	 * @return void
	 */
	protected function update_product_variations( $post_id, $variations ) {
		$exisiting_variations = get_posts( 'post_type=product_variation&post_parent='. $post_id );
		foreach ( $exisiting_variations as $index => $variation ) {
			$variationData = $variations[$index];

			$variation_post_id = $variation->ID; // Insert the variation

			foreach ( $variationData['attributes'] as $attribute => $value ) {
				$attribute_term = get_term_by( 'name', $value, 'pa_'.$attribute );
				update_post_meta( $variation_post_id, 'attribute_pa_'.$attribute, $attribute_term->slug );
			}
			update_post_meta( $variation_post_id, '_price', $variationData['price'] );
			update_post_meta( $variation_post_id, '_virtual', $this->args['virtual'] ? 'yes' : 'no' );
			update_post_meta( $variation_post_id, '_regular_price', $variationData['price'] );
			if($variationData['quantity'] !== false){
				update_post_meta( $variation_post_id, '_manage_stock', 'yes' );
				update_post_meta( $variation_post_id, '_stock', $variationData['quantity'] );
				update_post_meta( $variation_post_id, '_stock_status', $variationData['quantity'] !== '0' ? 'instock' : 'outofstock' );
			}

			unset( $variations[$index] );
		}
		if ( $variations ) {
			$this->insert_product_variations( $post_id, $variations );
		}
	}


}
