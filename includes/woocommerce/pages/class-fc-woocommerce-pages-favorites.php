<?php
/**
 * Adds favorites to the WC "My Account" page
 */
class FC_WooCommerce_Pages_Favorites {

	/**
	 * Custom endpoint name.
	 *
	 * @var string
	 */
	public static $endpoint = 'my-favorites';

	/**
	 * Plugin actions.
	 */
	public function __construct() {
		// Actions used to insert a new endpoint in the WordPress.
		add_action( 'init', array( $this, 'add_endpoints' ) );
		add_filter( 'query_vars', array( $this, 'add_query_vars' ), 0 );

		// Change the My Accout page title.
		add_filter( 'the_title', array( $this, 'endpoint_title' ) );

		// Insering your new tab/page into the My Account page.
		add_filter( 'woocommerce_account_menu_items', array( $this, 'new_menu_items' ) );
		add_action( 'woocommerce_account_' . self::$endpoint .  '_endpoint', array( $this, 'endpoint_content' ) );
	}

	/**
	 * Register new endpoint to use inside My Account page.
	 *
	 * @see https://developer.wordpress.org/reference/functions/add_rewrite_endpoint/
	 */
	public function add_endpoints() {
		add_rewrite_endpoint( self::$endpoint, EP_ROOT | EP_PAGES );
	}

	/**
	 * Add new query var.
	 *
	 * @param array   $vars
	 * @return array
	 */
	public function add_query_vars( $vars ) {
		$vars[] = self::$endpoint;

		return $vars;
	}

	/**
	 * Set endpoint title.
	 *
	 * @param string  $title
	 * @return string
	 */
	public function endpoint_title( $title ) {
		global $wp_query;

		$is_endpoint = isset( $wp_query->query_vars[ self::$endpoint ] );

		if ( $is_endpoint && ! is_admin() && is_main_query() && in_the_loop() && is_account_page() ) {
			// New page title.
			$title = __( 'I <span class="fa fa-heart"></span> disse arrangementene', 'woocommerce' );

			remove_filter( 'the_title', array( $this, 'endpoint_title' ) );
		}

		return $title;
	}

	/**
	 * Insert the new endpoint into the My Account menu.
	 *
	 * @param array   $items
	 * @return array
	 */
	public function new_menu_items( $items ) {
		// Remove the logout menu item.
		$logout = $items['customer-logout'];
		unset( $items['customer-logout'] );

		// Insert your custom endpoint.
		$items[ self::$endpoint ] = __( 'Favorittliste', 'woocommerce' );

		// Insert back the logout item.
		$items['customer-logout'] = $logout;

		return $items;
	}

	/**
	 * Endpoint HTML content.
	 */
	public function endpoint_content() {
		$favs = $this->get_favs();
		if($favs) { ?>
			<div class="fav-events">
			<?php foreach ( $favs as $model ) : ?>
				<div class="fav-item">
					<div class="fav-info">
						<h4><?php echo $model->name ?></h4>
						<small><?php echo $model->date ?></small>
					</div>
					<div class="fav-actions">
						<a href="#" class="cart" data-item='<?php echo json_encode( nff_get_tickets( $model->id, $model->event ) ) ?>'>
							<span class="simple icon-basket"></span>
						</a>
						<a href="#" class="remove-fav" data-id="<?php echo $model->id ?>" data-event="<?php echo $model->event ?>">
							<span class="simple icon-close"></span>
						</a>
					</div>
				</div>
				<hr>
			<?php endforeach ?>
			</div>

		<?php } else { ?>
			<div class="cart-empty">
				<div class="cart-empty-icon round">
					<span class="simple icon-close"></span>
				</div>
				<p>Ingenting lagt til i favoritter.</p>
			</div>
		<?php }
	}

	public function get_favs() {
		$dbfavs = get_user_meta( get_current_user_id(), '_account_favs', true );
		$favs = array();
		if($dbfavs) {
			foreach ( $dbfavs as $fav ) {
				$model = new stdClass;
				$model->id = $fav['id'];
				$model->name = get_the_title( $fav['id'] );
				$model->event = $fav['event'];
				$event = nff_get_event( $fav['id'], $fav['event'] );
				$model->date = $event['nff_program_date'];
				$favs[] = $model;
			}
		}
		return $favs;
	}
}

new FC_WooCommerce_Pages_Favorites();
