<?php
/**
 * Adds tickets to the WC "My Account" page
 */
class FC_WooCommerce_Pages_Ticket {

	/**
	 * Custom endpoint name.
	 *
	 * @var string
	 */
	public static $endpoint = 'my-tickets';

	/**
	 * Plugin actions.
	 */
	public function __construct() {
		// Actions used to insert a new endpoint in the WordPress.
		add_action( 'init', array( $this, 'add_endpoints' ) );
		add_filter( 'query_vars', array( $this, 'add_query_vars' ), 0 );

		// Change the My Accout page title.
		add_filter( 'the_title', array( $this, 'endpoint_title' ) );

		// Insering your new tab/page into the My Account page.
		add_filter( 'woocommerce_account_menu_items', array( $this, 'new_menu_items' ) );
		add_action( 'woocommerce_account_' . self::$endpoint .  '_endpoint', array( $this, 'endpoint_content' ) );
	}

	/**
	 * Register new endpoint to use inside My Account page.
	 *
	 * @see https://developer.wordpress.org/reference/functions/add_rewrite_endpoint/
	 */
	public function add_endpoints() {
		add_rewrite_endpoint( self::$endpoint, EP_ROOT | EP_PAGES );
	}

	/**
	 * Add new query var.
	 *
	 * @param array   $vars
	 * @return array
	 */
	public function add_query_vars( $vars ) {
		$vars[] = self::$endpoint;

		return $vars;
	}

	/**
	 * Set endpoint title.
	 *
	 * @param string  $title
	 * @return string
	 */
	public function endpoint_title( $title ) {
		global $wp_query;

		$is_endpoint = isset( $wp_query->query_vars[ self::$endpoint ] );

		if ( $is_endpoint && ! is_admin() && is_main_query() && in_the_loop() && is_account_page() ) {
			// New page title.
			$title = __( 'Mine billetter', 'woocommerce' );

			remove_filter( 'the_title', array( $this, 'endpoint_title' ) );
		}

		return $title;
	}

	/**
	 * Insert the new endpoint into the My Account menu.
	 *
	 * @param array   $items
	 * @return array
	 */
	public function new_menu_items( $items ) {
		// Remove the logout menu item.
		$logout = $items['customer-logout'];
		unset( $items['customer-logout'] );

		// Insert your custom endpoint.
		$items[ self::$endpoint ] = __( 'Billetter', 'woocommerce' );

		// Insert back the logout item.
		$items['customer-logout'] = $logout;

		return $items;
	}

	/**
	 * Endpoint HTML content.
	 */
	public function endpoint_content() {
		$orders = $this->get_orders();

		if($orders) { ?>
		<div class="fav-events">
		<?php foreach($orders as $order) : ?>
			<div class="fav-item">
				<div class="fav-info">
					<h4><?php echo count($order['tickets']['tickets']) ?> billett<?php echo count($order['tickets']['tickets']) > 1 ? 'er' : '' ?></h4>
					<small>Kjøpt <?php echo get_the_time( 'j. F Y H:i', $order['order']->id ); ?></small>
				</div>
				<div class="fav-actions">
					<a href="<?php echo home_url('/ticket/'. str_replace('wc_order_', '', $order['order']->order_key)) ?>" class="download">
						<span class="simple icon-arrow-down-circle"></span>
					</a>
				</div>
			</div>
			<hr>
		<?php endforeach ?>
		</div>
		<?php } else { ?>
			<div class="cart-empty">
				<div class="cart-empty-icon round">
					<span class="simple icon-close"></span>
				</div>
				<p>Ingen billetter funnet blant dine ordre.</p>
			</div>
		<?php }
	}

	public function get_orders() {
		$customer_orders = get_posts(
			array(
				'numberposts' => -1,
				'meta_key'    => '_customer_user',
				'meta_value'  => get_current_user_id(),
				'post_type'   => wc_get_order_types(),
				'post_status' => 'wc-completed',
			)
		);

		$orders = array();

		foreach ( $customer_orders as $order ) {
			$wc_order = wc_get_order( $order );
			$tickets = FC_Tickets::create_tickets( $wc_order->order_key );
			if ( $tickets ) {
				$orders[] = array(
					'order' => $wc_order,
					'tickets' => $tickets
				);
			}
		}

		return $orders;
	}
}

new FC_WooCommerce_Pages_Ticket();
