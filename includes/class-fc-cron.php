<?php
/**
 * Cron's to run that sends tickets some minutes after the order has been
 * placed. This is done so that in cases where the ticket generation fails,
 * the order is still placed and marked as being processed.
 */
class FC_Cron {

	/**
	 * Hook everything up
	 * @return [type] [description]
	 */
	public static function init() {
		// create custom cron schedules
		add_filter( 'cron_schedules', array(__CLASS__, 'schedules') );

		// add cron event schedule
		if ( ! wp_next_scheduled( 'fjellcommerce_send_tickets' ) ) {
			wp_schedule_event( time(), '15min', 'fjellcommerce_send_tickets' );
		}

		// hook in to the schedule
		add_action( 'fjellcommerce_send_tickets', array(__CLASS__, 'send_tickets'), 99999 );

		// this can also be run to execute the schedule
		add_action( 'wp_ajax_fjellcommerce_cron', array(__CLASS__, 'send_tickets_ajax'), 99999 );
	}

	/**
	 * Adds some schedules to wp-cron
	 * @param  array $schedules
	 * @return array
	 */
	public static function schedules($schedules) {
		if(!isset($schedules["15min"])){
			$schedules["15min"] = array(
				'interval' => 15*60,
				'display' => __('Once every 15 minutes')
			);
		}

		if(!isset($schedules["10min"])){
			$schedules["10min"] = array(
				'interval' => 10*60,
				'display' => __('Once every 10 minutes')
			);
		}

		if(!isset($schedules["5min"])){
			$schedules["5min"] = array(
				'interval' => 5*60,
				'display' => __('Once every 5 minutes')
			);
		}

		return $schedules;
	}

	/**
	 * Runs the job list via ajax
	 * @return void
	 */
	public static function send_tickets_ajax() {
		self::send_tickets();
		die;
	}

	/**
	 * Completes all processing orders and sends the tickets if there are some
	 * @return void
	 */
	public static function send_tickets() {
		$mailer = WC()->mailer();
		$orders = get_posts(array(
			'post_type' => 'shop_order',
			'post_status' => 'wc-processing',
			'posts_per_page' => -1
		));

		if($orders) {
			foreach($orders as $order) {
				$order = wc_get_order( $order );
				if ( isset($order->order_key) && $order->order_key && $pdf = FC_Tickets::create_pdf( $order->order_key ) ) { // Does the order contain any tickets?

					do_action( 'fjellcommerce_send_order_tickets', $order->id );
					$order->update_status( 'completed' );
				}
			}
		}
	}

}

FC_Cron::init();
