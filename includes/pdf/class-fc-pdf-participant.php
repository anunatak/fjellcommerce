<?php
/**
 * Ticket
 *
 * @class     FC_PDF_Participant
 * @version   2.5.0
 * @package   WooCommerce/Classes/Products
 * @category  Class
 * @author    WooThemes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * FC_Tickets Class.
 */
class FC_PDF_Participant {

	/**
	 * All tickets for the order
	 * @var array
	 */
	protected $events;

	/**
	 * All tickets for the order
	 * @var array
	 */
	protected $product;

	/**
	 * Order summary
	 * @var array
	 */
	protected $summary;

	/**
	 * TCPDF Instance
	 * @var TCPDF
	 */
	protected $pdf;

	/**
	 * Hooks up everything
	 * @param array $tickets
	 * @param array $summary
	 */
	public function __construct( $events, $product_id = null ) {

		// if tcpdf does not exist, lets include it
		if ( !class_exists( 'TCPDF' ) ) {
			include_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/vendor/tcpdf/tcpdf_autoconfig.php';
			include_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/vendor/tcpdf/tcpdf.php';
			include_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/vendor/tcpdf/tcpdf_barcodes_2d.php';
		}
		if($product_id)
			$this->product = $this->get_product($product_id);

		// add properties
		$this->events = $events;
		$this->pdf    = new TCPDF( 'P', 'mm', 'A4', true, 'UTF-8', false );
	}

	protected function get_product($product_id) {
		$product = wc_get_product($product_id);
		$attributes = $product->get_formatted_variation_attributes(true);
		return $attributes;
	}

	/**
	 * Render the PDF (output to browser)
	 * @return void
	 */
	public function render() {
		$this->build();
		$this->pdf->Output('event_particpants.pdf', 'I');
	}

	/**
	 * Build the document
	 * @return void
	 */
	protected function build() {
		$this->fonts();
		$this->document();
		$this->pages();
	}

	/**
	 * Set up the correct fonts
	 * @return void [description]
	 */
	protected function fonts() {
		TCPDF_FONTS::addTTFfont( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/fonts/Unisans regular.ttf', 'TrueTypeUnicode', '', 32 );
		TCPDF_FONTS::addTTFfont( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/fonts/Unisans regular italic.ttf', 'TrueTypeUnicode', '', 32 );
		TCPDF_FONTS::addTTFfont( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/fonts/Unisans bold.ttf', 'TrueTypeUnicode', '', 32 );
		TCPDF_FONTS::addTTFfont( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/fonts/Unisans bold italic.ttf', 'TrueTypeUnicode', '', 32 );
	}

	/**
	 * Set document meta data
	 * @return void
	 */
	protected function document() {
		$this->pdf->SetCreator( 'Norsk Fjellfestival' );
		$this->pdf->SetAuthor( 'Anunatak AS' );
		$this->pdf->SetTitle( 'Deltakerliste' );
		$this->pdf->SetSubject( 'Deltakerliste' );
		$this->pdf->SetKeywords( 'particpants' );
		$this->pdf->SetMargins( 15, 15, 15, 15 );
		$this->pdf->SetAutoPageBreak( TRUE, 15 );
		$this->pdf->setFontSubsetting( true );
		$this->pdf->SetFont( 'unisans', '', 11, '', true );
		$this->pdf->setPrintHeader( false );
		$this->pdf->setPrintFooter( false );
	}

	/**
	 * Create the pages
	 * @return void
	 */
	protected function pages() {
		$this->participantsPage();
	}

	/**
	 * Create the order summary page
	 * @return void
	 */
	protected function participantsPage() {
		$svg = file_get_contents( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/images/logo.svg' );
		$this->pdf->addPage();
		$this->pdf->ImageSVG( '@'. $svg, 15, 0, 100, 40, home_url() );
		$this->pdf->Ln( 30 );
		$this->pdf->WriteHTML( $this->participantsPageHtml() );
	}

	/**
	 * Create the order summary page HTML
	 * @return string
	 */
	protected function participantsPageHtml() {
		$html = '';

		foreach($this->events as $event) {

			$html .= '<h3 style="font-weight:normal">'. $event['name'] .'</h3>'.($this->product ? '<h4>'.$this->product.'</h4>' : '').'<h3></h3>';
			$html .= '<table>';
			$html .= '<tr>';
			$html .= '<th><strong>Navn</strong><br></th>';
			$html .= '<th><strong>Telefon</strong><br></th>';
			$html .= '<th><strong>E-post</strong><br></th>';
			$html .= '<th><strong>Kommentar</strong><br></th>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td colspan="4"><hr></td>';
			$html .= '</tr>';

			foreach($event['participants'] as $user) {
				$html .= '<tr>';
				$html .= '<td>'.$user['name'].'<br></td>';
				$html .= '<td>'.$user['phone'].'<br></td>';
				$html .= '<td>'.$user['email'].'<br></td>';
				$html .= '<td>'.nl2br($user['comment']).'<br></td>';
				$html .= '</tr>';
				$html .= '<tr>';
				$html .= '<td colspan="4"><hr></td>';
				$html .= '</tr>';
			}
			$html .= '</table>';

		}

		return $html;
	}


}
