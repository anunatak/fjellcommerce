<?php
/**
 * Ticket
 *
 * @class     FC_PDF_Tickets
 * @version   2.5.0
 * @package   WooCommerce/Classes/Products
 * @category  Class
 * @author    WooThemes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * FC_Tickets Class.
 */
class FC_PDF_Revenue {

	/**
	 * All revenue for the order
	 * @var array
	 */
	protected $report;

	/**
	 * TCPDF Instance
	 * @var TCPDF
	 */
	protected $pdf;

	/**
	 * Hooks up everything
	 * @param array $report
	 * @param array $summary
	 */
	public function __construct( $report) {

		// if tcpdf does not exist, lets include it
		if ( !class_exists( 'TCPDF' ) ) {
			include_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/vendor/tcpdf/tcpdf_autoconfig.php';
			include_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/vendor/tcpdf/tcpdf.php';
			include_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/vendor/tcpdf/tcpdf_barcodes_2d.php';
		}

		if(!class_exists('QRencode')) {
			include_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/vendor/phpqrcode/qrlib.php';
		}

		// add properties
		$this->report = $report;
		$this->pdf    = new TCPDF( 'P', 'mm', 'A4', true, 'UTF-8', false );
	}

	/**
	 * Render the PDF (output to browser)
	 * @return void
	 */
	public function render() {
		$this->build();
		$this->pdf->Output('report_'. date('Ymd', $this->report->get_transfer()->date) .'.pdf', 'I');
	}

	/**
	 * Save the PDF to a file
	 * @return string The file path
	 */
	public function save()  {
		$this->build();
		$filename = $this->getSavePath();
		$this->pdf->Output($filename, 'F');
		return $filename;
	}

	/**
	 * Get the path to the proposed file
	 * @return string
	 */
	protected function getSavePath() {
		$dirs = wp_upload_dir();

		if(!is_dir($dirs['basedir'] . '/revenue'))
			$dir = wp_mkdir_p( $dirs['basedir'] . '/revenue' );

		return $dirs['basedir'] . '/revenue/report_' . date('Ymd', $this->report->get_transfer()->date) .'.pdf';
	}

	/**
	 * Build the document
	 * @return void
	 */
	protected function build() {
		$this->fonts();
		$this->document();
		$this->pages();
	}

	/**
	 * Set up the correct fonts
	 * @return void [description]
	 */
	protected function fonts() {
		TCPDF_FONTS::addTTFfont( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/fonts/Unisans regular.ttf', 'TrueTypeUnicode', '', 32 );
		TCPDF_FONTS::addTTFfont( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/fonts/Unisans regular italic.ttf', 'TrueTypeUnicode', '', 32 );
		TCPDF_FONTS::addTTFfont( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/fonts/Unisans bold.ttf', 'TrueTypeUnicode', '', 32 );
		TCPDF_FONTS::addTTFfont( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/fonts/Unisans bold italic.ttf', 'TrueTypeUnicode', '', 32 );
	}

	/**
	 * Set document meta data
	 * @return void
	 */
	protected function document() {
		$this->pdf->SetCreator( 'Norsk Fjellfestival' );
		$this->pdf->SetAuthor( 'Anunatak AS' );
		$this->pdf->SetTitle( 'Rapport for overføring kjørt '. date_i18n('j. F Y', $this->report->get_transfer()->date) );
		$this->pdf->SetSubject( 'Billetter' );
		$this->pdf->SetKeywords( 'billett, ticket, ordre' );
		$this->pdf->SetMargins( 15, 15, 15, 15 );
		$this->pdf->SetAutoPageBreak( TRUE, 15 );
		$this->pdf->setFontSubsetting( true );
		$this->pdf->SetFont( 'unisans', '', 11, '', true );
		$this->pdf->setPrintHeader( false );
		$this->pdf->setPrintFooter( false );
	}

	/**
	 * Create the pages
	 * @return void
	 */
	protected function pages() {
		$this->reportItemsPage();
		// $this->reportSummaryPage();
	}

	/**
	 * Create the order summary page
	 * @return void
	 */
	protected function reportItemsPage() {
		$svg = file_get_contents( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/images/logo.svg' );
		$this->pdf->addPage();
		$this->pdf->ImageSVG( '@'. $svg, 15, 0, 100, 40, home_url() );
		$this->pdf->Ln( 30 );
		$this->pdf->WriteHTML( $this->reportItemsPageHtml() );
	}

	/**
	 * Create the order summary page HTML
	 * @return string
	 */
	protected function reportItemsPageHtml() {
		$html = '<p>Overført til konto <b>'.date_i18n('l j. F Y', $this->report->get_transfer()->date).'</b></p>';

		$html .= '<table>';
			$html .= '<thead>';
			$html .= '<tr>';
			$html .= '<td width="32%"><b>Arrangement</b></td>';
			$html .= '<td width="8%"><b>Antall</b></td>';
			$html .= '<td width="15%"><b>Sum</b></td>';
			$html .= '<td width="14%"><b>Rabatt</b></td>';
			$html .= '<td width="15%"><b>Avgifter</b></td>';
			$html .= '<td width="15%"><b>Netto</b></td>';
			$html .= '</tr>';
			$html .= '</thead>';
		$html .= '<tbody>';
		foreach($this->report->get_items() as $item) {
			$html .= '<tr style="border-bottom: 1px solid #f0f0f0;">';
			$html .= '<td width="32%">'. $item->name .'</td>';
			$html .= '<td width="8%">'. $item->qty .'</td>';
			$html .= '<td width="15%">'. number_format($item->subtotal, 2, ',', '.') .'</td>';
			$html .= '<td width="14%">'. number_format($item->discount, 2, ',', '.') .'</td>';
			$html .= '<td width="15%">'. number_format($item->fee, 2, ',', '.') .'</td>';
			$html .= '<td width="15%">'. number_format($item->total, 2, ',', '.') .'</td>';
			$html .= '</tr>';
		}
		$html .= '</tbody>';
			$html .= '<tfoot>';
			$html .= '<tr>';
			$html .= '<td colspan="6"> </td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td colspan="5"><b>Sum billettsalg</b></td>';
			$html .= '<td>'. number_format($this->report->get_summary()->subtotal, 2, ',', '.') .'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td colspan="5"><b>Rabatter</b></td>';
			$html .= '<td>-'. number_format($this->report->get_summary()->discount, 2, ',', '.') .'</td>';
			$html .= '<td> </td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td colspan="5"><b>Avgifter (2.4% + 2kr)</b></td>';
			$html .= '<td>-'. number_format($this->report->get_summary()->fee, 2, ',', '.') .'</td>';
			$html .= '<td> </td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td colspan="5"><b>Refundert (inkl. avgifter)</b></td>';
			$html .= '<td>'. number_format($this->report->get_summary()->refund, 2, ',', '.') .'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td colspan="5"><b>Overført til konto</b></td>';
			$html .= '<td>'. number_format($this->report->get_summary()->total, 2, ',', '.') .'</td>';
			$html .= '</tr>';
			$html .= '</tfoot>';
		$html .= '</table>';
		return $html;
	}

	/**
	 * Create the order summary page
	 * @return void
	 */
	protected function revenuePage() {
		$this->pdf->addPage();
		foreach($this->revenue as $ticket) {
			if($ticket['is_active']) {
				$this->pdf->WriteHTML($this->createTicketHtml($ticket), false);
				$this->pdf->Ln(5);
			}
		}
	}

	protected function createTicketHTML($ticket) {
		$enc = QRencode::factory(QR_ECLEVEL_L, 3, 0);
        $file = $enc->encodePNG($ticket['check_link'], WP_CONTENT_DIR . '/uploads/qr/'. $ticket['order_id'] .''. $ticket['order_ticket'] . '.png', false);

		$src = WP_CONTENT_DIR . '/uploads/qr/'. $ticket['order_id'] .''. $ticket['order_ticket'] . '.png';
		$ticket['qr_code'] = $src;

		$html = '<table border="1" cellspacing="0" cellpadding="10" style="width:100%">';
			$html .= '<tr>';
				$html .= '<td width="20%" align="center">';
					$html .= '<img src="'. plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/images/logo-rotate.png'.'" style="height:150px">';
				$html .= '</td>';
				$html .= '<td width="80%">';
					$html .= '<table border="0" cellspacing="0" cellpadding="0" style="width:100%">';
						$html .= '<tr>';
							$html .= '<td width="80%">';
								$html .= '<br><br><span style="font-size:15px">'.implode(',', $ticket['venue']).'</span>';
								$html .= '<div style="font-size:22px">'.$ticket['title'].'</div>';
								$html .= ''.( isset($ticket['name']) ? '<div style="font-size:14px">'.$ticket['name'].'</div>' : '' ).'';
								$html .= '<table border="0" cellspacing="0" cellpadding="0" style="width:100%;">';
									$html .= '<tr>';
										$html .= '<td width="50%">';
											$html .= '<br><br><br>'.( isset($ticket['seatmap']) ? '<span style="font-size:14px">Sete '. $ticket['seatmap']['seat'] .' Rad '. $ticket['seatmap']['row'] .'</span><br>' : '' ).'';
											$html .= '<span style="font-size:12px;line-height:1.5">'.ucfirst(date_i18n( 'l j. F Y', $ticket['date']->format('U') )).'</span><br>';
											$html .= '<span style="font-size:12px;line-height:1.5">Kl '.date_i18n( 'H.i', $ticket['date']->format('U') ).'</span><br>';
										$html .= '</td>';
										$html .= '<td width="50%">';
											$html .= '<br><br><br><span style="font-size:14px;line-height:1.5">'.$ticket['ticket_type'].'</span><br>';
											$html .= '<span style="font-size:12px;line-height:1.5">Kr '.$ticket['item_price'].'</span><br>';
											$html .= ''.( isset($ticket['seatmap']) ? '<span style="font-size:12px">Sete '. $ticket['seatmap']['seat'] .' Rad '. $ticket['seatmap']['row'] .'</span>' : '' ).'';
										$html .= '</td>';
									$html .= '</tr>';
								$html .= '</table>';
							$html .= '</td>';
							$html .= '<td width="20%">';
								$html .= '<img src="'.$ticket['qr_code'].'">';
							$html .= '</td>';
						$html .= '</tr>';
					$html .= '</table>';
				$html .= '</td>';
			$html .= '</tr>';
		$html .= '</table>';

		return $html;
	}

}
