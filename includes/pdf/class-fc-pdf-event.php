<?php
/**
 * Ticket
 *
 * @class     FC_PDF_Tickets
 * @version   2.5.0
 * @package   WooCommerce/Classes/Products
 * @category  Class
 * @author    WooThemes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * FC_Tickets Class.
 */
class FC_PDF_Event {

	/**
	 * All revenue for the order
	 * @var array
	 */
	protected $report;

	/**
	 * TCPDF Instance
	 * @var TCPDF
	 */
	protected $pdf;

	/**
	 * Hooks up everything
	 * @param array $report
	 * @param array $summary
	 */
	public function __construct( $report) {

		// if tcpdf does not exist, lets include it
		if ( !class_exists( 'TCPDF' ) ) {
			include_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/vendor/tcpdf/tcpdf_autoconfig.php';
			include_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/vendor/tcpdf/tcpdf.php';
			include_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/vendor/tcpdf/tcpdf_barcodes_2d.php';
		}

		if(!class_exists('QRencode')) {
			include_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/vendor/phpqrcode/qrlib.php';
		}

		// add properties
		$this->report = $report;
		$this->pdf    = new TCPDF( 'P', 'mm', 'A4', true, 'UTF-8', false );
	}

	/**
	 * Render the PDF (output to browser)
	 * @return void
	 */
	public function render() {
		$this->build();
		$this->pdf->Output('report.pdf', 'I');
	}

	/**
	 * Save the PDF to a file
	 * @return string The file path
	 */
	public function save()  {
		$this->build();
		$filename = $this->getSavePath();
		$this->pdf->Output($filename, 'F');
		return $filename;
	}

	/**
	 * Get the path to the proposed file
	 * @return string
	 */
	protected function getSavePath() {
		$dirs = wp_upload_dir();

		if(!is_dir($dirs['basedir'] . '/revenue'))
			$dir = wp_mkdir_p( $dirs['basedir'] . '/revenue' );

		return $dirs['basedir'] . '/revenue/report.pdf';
	}

	/**
	 * Build the document
	 * @return void
	 */
	protected function build() {
		$this->fonts();
		$this->document();
		$this->pages();
	}

	/**
	 * Set up the correct fonts
	 * @return void [description]
	 */
	protected function fonts() {
		TCPDF_FONTS::addTTFfont( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/fonts/Unisans regular.ttf', 'TrueTypeUnicode', '', 32 );
		TCPDF_FONTS::addTTFfont( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/fonts/Unisans regular italic.ttf', 'TrueTypeUnicode', '', 32 );
		TCPDF_FONTS::addTTFfont( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/fonts/Unisans bold.ttf', 'TrueTypeUnicode', '', 32 );
		TCPDF_FONTS::addTTFfont( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/fonts/Unisans bold italic.ttf', 'TrueTypeUnicode', '', 32 );
	}

	/**
	 * Set document meta data
	 * @return void
	 */
	protected function document() {
		$this->pdf->SetCreator( 'Norsk Fjellfestival' );
		$this->pdf->SetAuthor( 'Anunatak AS' );
		$this->pdf->SetTitle( 'Rapport' );
		$this->pdf->SetSubject( 'Billetter' );
		$this->pdf->SetKeywords( 'billett, ticket, ordre' );
		$this->pdf->SetMargins( 15, 15, 15, 15 );
		$this->pdf->SetAutoPageBreak( TRUE, 15 );
		$this->pdf->setFontSubsetting( true );
		$this->pdf->SetFont( 'unisans', '', 11, '', true );
		$this->pdf->setPrintHeader( false );
		$this->pdf->setPrintFooter( false );
	}

	/**
	 * Create the pages
	 * @return void
	 */
	protected function pages() {
		$this->reportItemsPage();
		// $this->reportSummaryPage();
	}

	/**
	 * Create the order summary page
	 * @return void
	 */
	protected function reportItemsPage() {
		$svg = file_get_contents( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/images/logo.svg' );
		$this->pdf->addPage();
		$this->pdf->ImageSVG( '@'. $svg, 15, 0, 100, 40, home_url() );
		$this->pdf->Ln( 30 );
		$this->pdf->WriteHTML( $this->reportItemsPageHtml() );
	}

	/**
	 * Create the order summary page HTML
	 * @return string
	 */
	protected function reportItemsPageHtml() {
		$html = '';
		foreach($this->report as $report) {
			$html .= '<h1>Antall billetter solgt for '.$report['product']->get_title().'</h1>';
			$html .= '<p>'. date_i18n( 'l j. F Y', date_create_from_format('Ymd', $report['date'])->format('U') ) .'</p>';

			$html .= '<h3>Betalingsmetoder</h3>';
			$html .= '<br><br>';
			$html .= '<table>';
			$html .= '<thead>';
			$html .= '<tr>';
			$html .= '<td width="80%"><strong>Betalingsmetode</strong></td>';
			$html .= '<td width="20%"><strong>Antall</strong></td>';
			$html .= '</tr>';
			$html .= '</thead>';

			$html .= '<tbody>';
			foreach($report['payment_methods'] as $method) {
				$html .= '<tr>';
				$html .= '<td width="80%">'. $method['name'] .'</td>';
				$html .= '<td width="20%">'. $method['sold'] .'</td>';
				$html .= '</tr>';
			}
			$html .= '</tbody>';

			$html .= '<tfoot>';
			$html .= '<tr>';
			$html .= '<td width="80%"><strong>Totalt antall solgte billetter</strong></td>';
			$html .= '<td width="20%"><strong>'.$report['sold'].'</strong></td>';
			$html .= '</tr>';
			$html .= '</tfoot>';

			$html .= '</table>';

			$html .= '<h3>Billett typer</h3>';
			$html .= '<br><br>';
			$html .= '<table>';
			$html .= '<thead>';
			$html .= '<tr>';
			$html .= '<td width="80%"><strong>Billett type</strong></td>';
			$html .= '<td width="20%"><strong>Antall</strong></td>';
			$html .= '</tr>';
			$html .= '</thead>';

			$html .= '<tbody>';
			foreach($report['ticket_types'] as $method) {
				$html .= '<tr>';
				$html .= '<td width="80%">'. $method['name'] .'</td>';
				$html .= '<td width="20%">'. $method['sold'] .'</td>';
				$html .= '</tr>';
			}
			$html .= '</tbody>';

			$html .= '<tfoot>';
			$html .= '<tr>';
			$html .= '<td width="80%"><strong>Totalt antall solgte billetter</strong></td>';
			$html .= '<td width="20%"><strong>'.$report['sold'].'</strong></td>';
			$html .= '</tr>';
			$html .= '</tfoot>';

			$html .= '</table>';

			$html .= '<h3>Billettsalg</h3>';
			$html .= '<br><br>';
			$html .= '<table>';
			$html .= '<thead>';
			$html .= '<tr>';
			$html .= '<td width="80%"><strong>Beskrivelse</strong></td>';
			$html .= '<td width="20%"><strong>Total</strong></td>';
			$html .= '</tr>';
			$html .= '</thead>';

			$html .= '<tbody>';
			foreach($report['sales'] as $method) {
				$html .= '<tr>';
				$html .= '<td width="80%">'. $method['name'] .'</td>';
				$html .= '<td width="20%">'. number_format($method['sold'], 2, ',', '.') .'</td>';
				$html .= '</tr>';
			}
			$html .= '<tr>';
			$html .= '<td width="80%"><em>Rabatter <sup>1</sup></em></td>';
			$html .= '<td width="20%">–'. number_format($report['discounts'], 2, ',', '.') .'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td width="80%"><em>Avgifter (Kredittkort) <sup>2</sup></em></td>';
			$html .= '<td width="20%">–'. number_format($report['fees'], 2, ',', '.') .'</td>';
			$html .= '</tr>';
			$html .= '</tbody>';

			$html .= '<tfoot>';
			$html .= '<tr>';
			$html .= '<td width="80%"><strong>Totalt antall solgte billetter</strong></td>';
			$html .= '<td width="20%"><strong>'. number_format($report['total'], 2, ',', '.') .'</strong></td>';
			$html .= '</tr>';
			$html .= '</tfoot>';

			$html .= '</table>';
		}
		return $html;
	}

	/**
	 * Create the order summary page
	 * @return void
	 */
	protected function revenuePage() {
		$this->pdf->addPage();
		foreach($this->revenue as $ticket) {
			if($ticket['is_active']) {
				$this->pdf->WriteHTML($this->createTicketHtml($ticket), false);
				$this->pdf->Ln(5);
			}
		}
	}

	protected function createTicketHTML($ticket) {
		$enc = QRencode::factory(QR_ECLEVEL_L, 3, 0);
        $file = $enc->encodePNG($ticket['check_link'], WP_CONTENT_DIR . '/uploads/qr/'. $ticket['order_id'] .''. $ticket['order_ticket'] . '.png', false);

		$src = WP_CONTENT_DIR . '/uploads/qr/'. $ticket['order_id'] .''. $ticket['order_ticket'] . '.png';
		$ticket['qr_code'] = $src;

		$html = '<table border="1" cellspacing="0" cellpadding="10" style="width:100%">';
			$html .= '<tr>';
				$html .= '<td width="20%" align="center">';
					$html .= '<img src="'. plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/images/logo-rotate.png'.'" style="height:150px">';
				$html .= '</td>';
				$html .= '<td width="80%">';
					$html .= '<table border="0" cellspacing="0" cellpadding="0" style="width:100%">';
						$html .= '<tr>';
							$html .= '<td width="80%">';
								$html .= '<br><br><span style="font-size:15px">'.implode(',', $ticket['venue']).'</span>';
								$html .= '<div style="font-size:22px">'.$ticket['title'].'</div>';
								$html .= ''.( isset($ticket['name']) ? '<div style="font-size:14px">'.$ticket['name'].'</div>' : '' ).'';
								$html .= '<table border="0" cellspacing="0" cellpadding="0" style="width:100%;">';
									$html .= '<tr>';
										$html .= '<td width="50%">';
											$html .= '<br><br><br>'.( isset($ticket['seatmap']) ? '<span style="font-size:14px">Sete '. $ticket['seatmap']['seat'] .' Rad '. $ticket['seatmap']['row'] .'</span><br>' : '' ).'';
											$html .= '<span style="font-size:12px;line-height:1.5">'.ucfirst(date_i18n( 'l j. F Y', $ticket['date']->format('U') )).'</span><br>';
											$html .= '<span style="font-size:12px;line-height:1.5">Kl '.date_i18n( 'H.i', $ticket['date']->format('U') ).'</span><br>';
										$html .= '</td>';
										$html .= '<td width="50%">';
											$html .= '<br><br><br><span style="font-size:14px;line-height:1.5">'.$ticket['ticket_type'].'</span><br>';
											$html .= '<span style="font-size:12px;line-height:1.5">Kr '.$ticket['item_price'].'</span><br>';
											$html .= ''.( isset($ticket['seatmap']) ? '<span style="font-size:12px">Sete '. $ticket['seatmap']['seat'] .' Rad '. $ticket['seatmap']['row'] .'</span>' : '' ).'';
										$html .= '</td>';
									$html .= '</tr>';
								$html .= '</table>';
							$html .= '</td>';
							$html .= '<td width="20%">';
								$html .= '<img src="'.$ticket['qr_code'].'">';
							$html .= '</td>';
						$html .= '</tr>';
					$html .= '</table>';
				$html .= '</td>';
			$html .= '</tr>';
		$html .= '</table>';

		return $html;
	}

}
