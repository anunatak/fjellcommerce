<?php
/**
 * Ticket
 *
 * @class     FC_PDF_Tickets
 * @version   2.5.0
 * @package   WooCommerce/Classes/Products
 * @category  Class
 * @author    WooThemes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * FC_Tickets Class.
 */
class FC_PDF_Tickets {

	/**
	 * All tickets for the order
	 * @var array
	 */
	protected $tickets;

	/**
	 * Order summary
	 * @var array
	 */
	protected $summary;

	/**
	 * TCPDF Instance
	 * @var TCPDF
	 */
	protected $pdf;

	/**
	 * Hooks up everything
	 * @param array $tickets
	 * @param array $summary
	 */
	public function __construct( $tickets, $summary ) {

		// if tcpdf does not exist, lets include it
		if ( !class_exists( 'TCPDF' ) ) {
			include_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/vendor/tcpdf/tcpdf_autoconfig.php';
			include_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/vendor/tcpdf/tcpdf.php';
			include_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/vendor/tcpdf/tcpdf_barcodes_2d.php';
		}

		if(!class_exists('QRencode')) {
			include_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/vendor/phpqrcode/qrlib.php';
		}

		// add properties
		$this->tickets = $tickets;
		$this->summary = $summary;
		$this->pdf     = new TCPDF( 'P', 'mm', 'A4', true, 'UTF-8', false );
	}

	/**
	 * Render the PDF (output to browser)
	 * @return void
	 */
	public function render() {
		$this->build();
		$this->pdf->Output('order_'. $this->summary['order_id'] .'_tickets.pdf', 'I');
	}

	/**
	 * Save the PDF to a file
	 * @return string The file path
	 */
	public function save()  {
		$this->build();
		$filename = $this->getSavePath();
		$this->pdf->Output($filename, 'F');
		return $filename;
	}

	/**
	 * Get the path to the proposed file
	 * @return string
	 */
	protected function getSavePath() {
		$dirs = wp_upload_dir();

		if(!is_dir($dirs['basedir'] . '/tickets'))
			$dir = wp_mkdir_p( $dirs['basedir'] . '/tickets' );

		return $dirs['basedir'] . '/tickets/' . $this->summary['order_id'] .'.pdf';
	}

	/**
	 * Build the document
	 * @return void
	 */
	protected function build() {
		$this->fonts();
		$this->document();
		$this->pages();
	}

	/**
	 * Set up the correct fonts
	 * @return void [description]
	 */
	protected function fonts() {
		TCPDF_FONTS::addTTFfont( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/fonts/Unisans regular.ttf', 'TrueTypeUnicode', '', 32 );
		TCPDF_FONTS::addTTFfont( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/fonts/Unisans regular italic.ttf', 'TrueTypeUnicode', '', 32 );
		TCPDF_FONTS::addTTFfont( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/fonts/Unisans bold.ttf', 'TrueTypeUnicode', '', 32 );
		TCPDF_FONTS::addTTFfont( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/fonts/Unisans bold italic.ttf', 'TrueTypeUnicode', '', 32 );
	}

	/**
	 * Set document meta data
	 * @return void
	 */
	protected function document() {
		$this->pdf->SetCreator( 'Norsk Fjellfestival' );
		$this->pdf->SetAuthor( 'Anunatak AS' );
		$this->pdf->SetTitle( 'Billetter for ordre '. $this->summary['order_id'] );
		$this->pdf->SetSubject( 'Billetter' );
		$this->pdf->SetKeywords( 'billett, ticket, ordre' );
		$this->pdf->SetMargins( 15, 15, 15, 15 );
		$this->pdf->SetAutoPageBreak( TRUE, 15 );
		$this->pdf->setFontSubsetting( true );
		$this->pdf->SetFont( 'unisans', '', 11, '', true );
		$this->pdf->setPrintHeader( false );
		$this->pdf->setPrintFooter( false );
	}

	/**
	 * Create the pages
	 * @return void
	 */
	protected function pages() {
		$this->orderSummaryPage();
		$this->ticketsPage();
	}

	/**
	 * Create the order summary page
	 * @return void
	 */
	protected function orderSummaryPage() {
		$svg = file_get_contents( plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/images/logo.svg' );
		$this->pdf->addPage();
		$this->pdf->ImageSVG( '@'. $svg, 15, 0, 100, 40, home_url() );
		$this->pdf->Ln( 30 );
		$this->pdf->WriteHTML( $this->orderSummaryPageHtml() );
	}

	/**
	 * Create the order summary page HTML
	 * @return string
	 */
	protected function orderSummaryPageHtml() {
		$html = '<h1 style="font-weight:normal">Takk for at du handlet!</h1>';
		$html .= '<h3 style="font-weight:normal">Har du laget profil på vår nettside, har du også tilgang til alle dine billetter digitalt – og du kan vise de på mobil på arrangementet.</h3>';
		$html .= '<p>Billettene kan medbringes på arrangementet, digitalt eller printet. Billetten vil bli kontrollert ved inngang, og kopierte eller falske billetter vil bli avvist. Billettene er å anse som verdipapir og skal behandles deretter. Andrehåndskjøp av billetter står for kjøpers egen risiko.</p>';

		foreach($this->summary['items'] as $item) {
			$html .= '<table>';
				$html .= '<tr style="border-bottom:2px solid #000">';
					$html .= '<td width="80%">';
						$html .= '<h3>'.$item['name'].'</h3>';
						$html .= '<span>'. $item['qty'] .'x '. $item['ticket_type'] . '</span><br>';
					$html .= '</td>';
						$html .= '<td width="20%" align="bottom">';
						$html .= '<br><br><br>Totalt kr '.$item['total'].' ,–';
					$html .= '</td>';
				$html .= '</tr>';
			$html .= '</table>';
			$html .= '<hr>';
		}

		$html = apply_filters( 'nff_ticket_after_items', $html, $this );

		$html .= '<table>';
		$html .= '<tr>';
		$html .= '<td width="80%"><br><h1 style="color: rgb(255, 80, 0)">Totalt</h1></td>';
		$html .= '<td width="20%"><br><h1 style="color: rgb(255, 80, 0);text-align:center">Kr '. $this->summary['total'] .' ,–</h1></td>';
		$html .= '</tr>';
		$html .= '</table>';

		$html = apply_filters( 'nff_ticket_after_total', $html, $this );

		return $html;
	}

	/**
	 * Create the order summary page
	 * @return void
	 */
	protected function ticketsPage() {
		$this->pdf->addPage();
		$i = 0;
		foreach($this->tickets as $ticket) {
			if($ticket['is_active']) {
				$this->pdf->WriteHTML($this->createTicketHtml($ticket), false);
				$this->pdf->Ln(5);
			}
			$i++;
			if($i % 3 === 0 ) {
				$this->pdf->addPage();
			}
		}
	}

	protected function createTicketHTML($ticket) {
		$enc = QRencode::factory(QR_ECLEVEL_L, 3, 0);
        $file = $enc->encodePNG($ticket['check_link'], WP_CONTENT_DIR . '/uploads/qr/'. $ticket['order_id'] .''. $ticket['order_ticket'] . '.png', false);

		$src = WP_CONTENT_DIR . '/uploads/qr/'. $ticket['order_id'] .''. $ticket['order_ticket'] . '.png';
		$ticket['qr_code'] = $src;

		$html = '<table border="1" cellspacing="0" cellpadding="10" style="width:100%">';
			$html .= '<tr>';
				$html .= '<td width="20%" align="center">';
					$html .= '<img src="'. plugin_dir_path( FC_PLUGIN_FILE ) . 'assets/images/logo-rotate.png'.'" style="height:150px">';
				$html .= '</td>';
				$html .= '<td width="80%">';
					$html .= '<table border="0" cellspacing="0" cellpadding="0" style="width:100%">';
						$html .= '<tr>';
							$html .= '<td width="80%">';
								$html .= '<br><br><span style="font-size:15px">'.implode(',', $ticket['venue']).'</span>';
								$html .= '<div style="font-size:22px">'.$ticket['title'].'</div>';
								$html .= ''.( isset($ticket['name']) ? '<div style="font-size:14px">'.$ticket['name'].'</div>' : '' ).'';
								$html .= '<table border="0" cellspacing="0" cellpadding="0" style="width:100%;">';
									$html .= '<tr>';
										$html .= '<td width="50%">';
											$html .= '<br><br><br>'.( isset($ticket['seatmap']) ? '<span style="font-size:14px">Sete '. $ticket['seatmap']['seat'] .' Rad '. $ticket['seatmap']['row'] .'</span><br>' : '' ).'';
											$html .= '<span style="font-size:12px;line-height:1.5">'.ucfirst(date_i18n( 'l j. F Y', $ticket['date']->format('U') )).'</span><br>';
											$html .= '<span style="font-size:12px;line-height:1.5">Kl '.date_i18n( 'H.i', $ticket['date']->format('U') ).'</span><br>';
										$html .= '</td>';
										$html .= '<td width="50%">';
											$html .= '<br><br><br><span style="font-size:14px;line-height:1.5">'.$ticket['ticket_type'].'</span><br>';
											$html .= '<span style="font-size:12px;line-height:1.5">Kr '.$ticket['item_price'].'</span><br>';
											$html .= ''.( isset($ticket['seatmap']) ? '<span style="font-size:12px">Sete '. $ticket['seatmap']['seat'] .' Rad '. $ticket['seatmap']['row'] .'</span>' : '' ).'';
										$html .= '</td>';
									$html .= '</tr>';
								$html .= '</table>';
							$html .= '</td>';
							$html .= '<td width="20%">';
								$html .= '<img src="'.$ticket['qr_code'].'">';
							$html .= '</td>';
						$html .= '</tr>';
					$html .= '</table>';
				$html .= '</td>';
			$html .= '</tr>';
		$html .= '</table>';

		return $html;
	}

}
