<?php
/**
 * Ajax
 *
 * @class     FC_Ajax
 * @version   2.5.0
 * @package   WooCommerce/Classes/Products
 * @category  Class
 * @author    WooThemes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * FC_Ajax Class.
 */
class FC_Ajax {

	/**
	 * Hook in methods.
	 *
	 * @return void
	 */
	public static function init() {

		// Get stock
		add_action( 'wp_ajax_nff_cart_get_availiable', array( __CLASS__, 'get_availiable' ) );
		add_action( 'wp_ajax_nopriv_nff_cart_get_availiable', array( __CLASS__, 'get_availiable' ) );

		// Add item
		add_action( 'wp_ajax_nff_cart_add_item', array( __CLASS__, 'add_item' ) );
		add_action( 'wp_ajax_nopriv_nff_cart_add_item', array( __CLASS__, 'add_item' ) );

		// Get items
		add_action( 'wp_ajax_nff_cart_get_items', array( __CLASS__, 'get_items' ) );
		add_action( 'wp_ajax_nopriv_nff_cart_get_items', array( __CLASS__, 'get_items' ) );

		// Get items
		add_action( 'wp_ajax_nff_account_fav_add', array( __CLASS__, 'add_fav' ) );
		add_action( 'wp_ajax_nopriv_nff_account_fav_add', array( __CLASS__, 'add_fav' ) );

		// Get items
		add_action( 'wp_ajax_nff_account_fav_remove', array( __CLASS__, 'remove_fav' ) );
		add_action( 'wp_ajax_nopriv_nff_account_fav_remove', array( __CLASS__, 'remove_fav' ) );

		// Get filters
		add_action( 'wp_ajax_nff_program_get_filters', array( __CLASS__, 'get_program_filters' ) );
		add_action( 'wp_ajax_nopriv_nff_program_get_filters', array( __CLASS__, 'get_program_filters' ) );

		// Get items
		add_action( 'wp_ajax_nff_program_get_data', array( __CLASS__, 'get_program_data' ) );
		add_action( 'wp_ajax_nopriv_nff_program_get_data', array( __CLASS__, 'get_program_data' ) );

	}

	/**
	 * Gets availiable tickets for a product
	 *
	 * @return void
	 */
	public static function get_availiable() {
		$id = $_REQUEST['id'];
		$product = get_product( $id );
		$stock = array();
		$variations = $product->get_available_variations();
		foreach ( $variations as $variation ) {
			$stock[$variation['variation_id']] = $variation['max_qty'];
		}
		wp_send_json( ['tickets' => $stock] );
		die;
	}

	/**
	 * Put items in the cart
	 *
	 * @return void
	 */
	public static function add_item() {
		$error = 0;
		if ( isset( $_REQUEST['items'] ) ) {
			foreach ( $_REQUEST['items'] as $item ) {
				$product_id = $item['id'];
				$variation_id = $item['ticket'];
				$variation = get_product( $variation_id );
				$product = get_product( $product_id );
				if ( $variation->is_in_stock() ) {
					$myvar = null;
					foreach ( $product->get_available_variations() as $var ) {
						if ( $var['variation_id'] == $variation_id )
							$myvar = $var;
					}
					if ( $myvar ) {
						WC()->cart->add_to_cart( $product_id, $item['quantity'], $variation_id, $myvar['attributes'] );
					} else {
						$error++;
					}
				}
				else {
					$error++;
				}
			}
		}
		if ( !$error ) {
			$cart = WC()->cart;
			wp_send_json( ['success' => true, 'data' => $cart->get_cart(), 'total' => $cart->get_cart_total(), 'items' => $cart->get_cart_contents_count()] );
		} else {
			wp_send_json_error( ['message' => 'Det er ikke nok tilgjengelige billetter til dette arrangementet.', 'errors' => $error] );
		}
		die;
	}

	/**
	 * Gets all cart items
	 *
	 * @return void
	 */
	public static function get_items() {
		$cart = WC()->cart;
		wp_send_json( ['success' => true, 'data' => $cart->get_cart(), 'total' => $cart->get_cart_total(), 'items' => $cart->get_cart_contents_count()] );
		die;
	}

	/**
	 * Adds an item to the favs
	 */
	public static function add_fav() {
		if ( is_user_logged_in() ) {
			$id = $_REQUEST['id'];
			$eventId = $_REQUEST['event'];
			if ( $id ) {
				$event = get_post( $id );
				$events = get_field( 'nff_program_events', $event->ID );
				$selectedEvent = isset( $events[$eventId] ) ? $events[$eventId] : false;
				if ( $selectedEvent ) {
					$dbEvents = get_user_meta( get_current_user_id(), '_account_favs', true );
					$dbEvents = $dbEvents ? $dbEvents : [];
					$exists = false;
					foreach ( $dbEvents as $dbEvent ) {
						if ( $dbEvent['id'] === $id && $dbEvent['event'] == $eventId ) {
							$exists = true;
						}
					}
					if ( !$exists ) {
						$dbEvents[] = array(
							'id'       => $id,
							'event'    => $eventId,
						);
						update_user_meta( get_current_user_id(), '_account_favs', $dbEvents );
					}
					wp_send_json_success( array( 'data' => $dbEvents ) );
				}
			}
		}
		wp_send_json_error();
		die;
	}

	/**
	 * Removes an item from the favs
	 */
	public static function remove_fav() {
		if ( is_user_logged_in() ) {
			$id = $_REQUEST['id'];
			$eventId = $_REQUEST['event'];
			if ( $id ) {
				$event = get_post( $id );
				$events = get_field( 'nff_program_events', $event->ID );
				$selectedEvent = isset( $events[$eventId] ) ? $events[$eventId] : false;
				if ( $selectedEvent ) {
					$dbEvents = get_user_meta( get_current_user_id(), '_account_favs', true );
					$dbEvents = $dbEvents ? $dbEvents : [];
					$exists = false;
					$newEvents = [];
					foreach ( $dbEvents as $index => $dbEvent ) {
						if ( $dbEvent['id'] === $id && $dbEvent['event'] == $eventId ) {
							unset( $dbEvents[$index] );
						}
					}
					update_user_meta( get_current_user_id(), '_account_favs', $dbEvents );

					wp_send_json_success( array( 'data' => $dbEvents ) );
				}
			}
		}
		wp_send_json_error();
	}

	/**
	 * Gets the filters for the program
	 * @return void
	 */
	public static function get_program_filters()
	{
		$events = new FC_Events;
		wp_send_json(array('data' => $events->get_filters()));
	}

	/**
	 * Gets data for the program
	 * @return void
	 */
	public static function get_program_data()
	{
		$events = new FC_Events;
		$data = $events->get_events();
		wp_send_json(['data' => $data, 'pages' => ceil($data['found']/20)]);
	}

}

FC_Ajax::init();
