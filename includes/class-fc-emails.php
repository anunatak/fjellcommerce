<?php
/**
 * Modifies the emails from WC
 */
class FC_Emails {

	/**
	 * Hooks everything up
	 * @return void
	 */
	public static function init() {
		add_action( 'woocommerce_email_order_meta', array( __CLASS__, 'add_tickets_to_email' ), 20, 1 );
		add_filter('woocommerce_email_classes', array(__CLASS__, 'add_emails'));
		add_filter('woocommerce_email_actions', array(__CLASS__, 'add_actions'));
		add_action('phpmailer_init', array(__CLASS__, 'adjust_smtp'));
	}

	/**
	 * Adjusts the phpmailer options a bit
	 * @param  PHPMailer $phpmailer
	 * @return void
	 */
	public static function adjust_smtp($phpmailer) {
		$phpmailer->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);
	}

	/**
	 * Adds a custom email for tickets to WC
	 * @param array $email_classes
	 */
	public static function add_emails($email_classes) {
		require_once plugin_dir_path( FC_PLUGIN_FILE ) . 'includes/emails/class-fc-emails-ticket.php'; // ticket email
		$email_classes['FC_Emails_Ticket'] = new FC_Emails_Ticket();
		return $email_classes;
	}

	/**
	 * Adds a new email action to WC
	 * @param array $actions
	 */
	public static function add_actions($actions) {
		$actions[] = 'fjellcommerce_send_order_tickets';
		return $actions;
	}

	/**
	 * Adds a text to the order email if a ticket has been detected in the order
	 * @param WC_Order $order
	 */
	public static function add_tickets_to_email( $order ) {
		$key = $order->order_key;
		if ( $tickets = FC_Tickets::create_tickets( $key ) ) {?>
			<h2>Billetter</h2>
			<p>Din ordre inneholder billetter, og disse sendes til epostadressen din innen 15 minutter. Billetten fremvises på arrangementet – printet eller digitalt. Har du konto på norskfjellfestival.no, finner du billettene dine der også – til en hver tid.</p>

			<p>Har du ikke konto? <a href="<?php echo home_url('/min-konto') ?>">Lag deg en her</a>, og få alle fremtidige billettkjøp samlet på et og samme sted.</p>

			<p>Følg oss gjerne på <a href="<?php echo home_url('/') ?>">nett</a>, <a href="http://facebook.com/norskfjellfestival">facebook</a> og <a href="http://instagram.com/norskfjellfestival">instagram</a> for oppdateringer og inspirasjon.</p>

		<?php }
	}
}

FC_Emails::init();
